/// <reference types="cypress" />
import { defineConfig } from 'cypress'

export default defineConfig({
    projectId: '3a93hs',
    viewportWidth: 1920,
    viewportHeight: 1080,
    component: {
        devServer: {
            framework: 'next',
            bundler: 'webpack',
        },
        specPattern: 'src/tests/__tests__/**/*.{spec,test,cy}.{js,ts,jsx,tsx}',
        video: false,
    },

    e2e: {
        setupNodeEvents(on, config) {
            // implement node event listeners here
        },
        specPattern: 'src/tests/e2e/**/*.{spec,test,cy}.{js,ts,jsx,tsx}',
        excludeSpecPattern: [
            '**/1-getting-started/*',
            '**/2-advanced-examples/*',
        ],
        testIsolation: false,
        video: false,
    },
})
