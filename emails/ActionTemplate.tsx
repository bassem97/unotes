import React from 'react'

interface ActionTemplateProps {
    username: string
    body: string
    link: string | null
}

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL

const ActionTemplate = ({ username, body, link }: ActionTemplateProps) => {
    return (
        <div style={{ background: '#69B3FC', padding: '20px' }}>
            <div
                style={{
                    background: '#ffffff',
                    padding: '40px',
                    borderRadius: '10px',
                    boxShadow: '0px 0px 10px rgba(0,0,0,0.1)',
                }}
            >
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '40px',
                    }}
                >
                    Dear {username}
                </p>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '40px',
                    }}
                >
                    We want to inform you that:
                </p>
                {link ? (
                    <a href={BASE_URL + link}>
                        <p
                            style={{
                                color: '#666666',
                                fontSize: '18px',
                                lineHeight: '28px',
                                marginBottom: '40px',
                            }}
                        >
                            {body}
                        </p>
                    </a>
                ) : (
                    <p
                        style={{
                            color: '#666666',
                            fontSize: '18px',
                            lineHeight: '28px',
                            marginBottom: '40px',
                        }}
                    >
                        {body}
                    </p>
                )}
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '20px',
                    }}
                >
                    Best regards,
                </p>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '40px',
                    }}
                >
                    The uNotes Team
                </p>
                <div
                    style={{
                        background: '#69B3FC',
                        borderRadius: '10px',
                        color: '#666666',
                        fontSize: '14px',
                        padding: '20px',
                        textAlign: 'center',
                    }}
                >
                    <p style={{ margin: '0' }}>
                        If you wish to unsubscribe, please click{' '}
                        <a href="#">here</a>.
                    </p>
                </div>
            </div>
        </div>
    )
}

export default ActionTemplate
