import React from 'react'

interface PasswordChangedTemplateProps {
    username: string
}

const PasswordChangedTemplate = ({
    username,
}: PasswordChangedTemplateProps) => {
    return (
        <div style={{ background: '#69B3FC', padding: '20px' }}>
            <div
                style={{
                    background: '#ffffff',
                    padding: '40px',
                    borderRadius: '10px',
                    boxShadow: '0px 0px 10px rgba(0,0,0,0.1)',
                }}
            >
                <h1
                    style={{
                        color: '#333333',
                        fontSize: '36px',
                        fontWeight: 'bold',
                        marginBottom: '40px',
                        textAlign: 'center',
                    }}
                >
                    Password reset !
                </h1>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '40px',
                    }}
                >
                    Dear {username}, we want to inform you that your uNotes
                    account password has been changed
                </p>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '40px',
                    }}
                >
                    If you have any questions or concerns, please feel free to
                    reach out to our support team.
                </p>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '20px',
                    }}
                >
                    Best regards,
                </p>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '40px',
                    }}
                >
                    The uNotes Team
                </p>
                <div
                    style={{
                        background: '#69B3FC',
                        borderRadius: '10px',
                        color: '#666666',
                        fontSize: '14px',
                        padding: '20px',
                        textAlign: 'center',
                    }}
                >
                    <p style={{ margin: '0' }}>
                        You received this email because your password for uNotes
                        has been changed
                    </p>
                    <p style={{ margin: '0' }}>
                        If you wish to unsubscribe, please click{' '}
                        <a href="#">here</a>.
                    </p>
                </div>
            </div>
        </div>
    )
}

export default PasswordChangedTemplate
