interface ResetPasswordProps {
    token: string
}

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL

export default function VerificationRequestTemplate({
    token,
}: ResetPasswordProps) {
    return (
        <div style={{ background: '#69B3FC', padding: '20px' }}>
            <div
                style={{
                    background: '#ffffff',
                    padding: '40px',
                    borderRadius: '10px',
                    boxShadow: '0px 0px 10px rgba(0,0,0,0.1)',
                }}
            >
                <h1
                    style={{
                        color: '#333333',
                        fontSize: '36px',
                        fontWeight: 'bold',
                        marginBottom: '40px',
                        textAlign: 'center',
                    }}
                >
                    Reset your uNotes account password
                </h1>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '40px',
                    }}
                >
                    {' '}
                    To reset your account password, please click the button
                    below.
                </p>
                <a
                    href={BASE_URL + `/reset-password/${token}`}
                    style={{
                        background: '#69B3FC',
                        borderRadius: '5px',
                        color: '#ffffff',
                        display: 'inline-block',
                        fontSize: '18px',
                        lineHeight: '28px',
                        padding: '10px 20px',
                        textDecoration: 'none',
                        textAlign: 'center',
                    }}
                >
                    Reset Password
                </a>
            </div>
        </div>
    )
}
