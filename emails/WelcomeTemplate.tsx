import React from 'react'

interface WelcomeTemplateProps {
    username: string
}

const WelcomeTemplate = ({ username }: WelcomeTemplateProps) => {
    return (
        <div style={{ background: '#69B3FC', padding: '20px' }}>
            <div
                style={{
                    background: '#ffffff',
                    padding: '40px',
                    borderRadius: '10px',
                    boxShadow: '0px 0px 10px rgba(0,0,0,0.1)',
                }}
            >
                <h1
                    style={{
                        color: '#333333',
                        fontSize: '36px',
                        fontWeight: 'bold',
                        marginBottom: '40px',
                        textAlign: 'center',
                    }}
                >
                    Welcome to uNotes {username} !
                </h1>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '40px',
                    }}
                >
                    We're excited to have you join our e-learning platform. With
                    uNotes, you'll be able to access high-quality course
                    content, connect with instructors and peers, and track your
                    progress.
                </p>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '20px',
                    }}
                >
                    To get started, here are a few things you can do:
                </p>
                <ul
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '40px',
                    }}
                >
                    <li>
                        Explore courses and discover all the features we have to
                        offer.
                    </li>
                    <li>
                        browse university courses and connect with other
                        students.
                    </li>
                    <li>
                        Get involved in our community events and activities.
                    </li>
                </ul>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '40px',
                    }}
                >
                    If you have any questions or concerns, please feel free to
                    reach out to our support team.
                </p>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '20px',
                    }}
                >
                    Best regards,
                </p>
                <p
                    style={{
                        color: '#666666',
                        fontSize: '18px',
                        lineHeight: '28px',
                        marginBottom: '40px',
                    }}
                >
                    The uNotes Team
                </p>
                <div
                    style={{
                        background: '#69B3FC',
                        borderRadius: '10px',
                        color: '#666666',
                        fontSize: '14px',
                        padding: '20px',
                        textAlign: 'center',
                    }}
                >
                    <p style={{ margin: '0' }}>
                        You received this email because you signed up for uNotes
                    </p>
                    <p style={{ margin: '0' }}>
                        If you wish to unsubscribe, please click{' '}
                        <a href="#">here</a>.
                    </p>
                </div>
            </div>
        </div>
    )
}

export default WelcomeTemplate
