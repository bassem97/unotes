/** @type {import('next').NextConfig} */

/** next bundle analyzer */
const withBundleAnalyzer = require('@next/bundle-analyzer')({
    enabled: process.env.ANALYZE === 'true',
    openAnalyzer: false,
})
/** next configurations */
const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    i18n: {
        locales: ['en'],
        defaultLocale: 'en',
    },
    images: {
        remotePatterns: [
            {
                protocol: 'https',
                hostname: 'lh3.googleusercontent.com',
            },
            {
                protocol: 'https',
                hostname: 'unotes-images.nyc3.cdn.digitaloceanspaces.com',
            },
        ],
    },
    async redirects() {
        return [
            {
                source: '/documents/edit/:id',
                destination: '/_maintenance',
                permanent: false,
            },
        ]
    },
    async rewrites() {
        return {
            beforeFiles: [
                {
                    source: '/documents/edit/:id',
                    destination: '/_maintenance',
                },
            ],
        }
    },
    async headers() {
        return [
            {
                // matching all API routes
                source: '/api/:path*',
                headers: [
                    { key: 'Access-Control-Allow-Credentials', value: 'true' },
                    { key: 'Access-Control-Allow-Origin', value: '*' }, // replace this your actual origin
                    {
                        key: 'Access-Control-Allow-Methods',
                        value: 'GET,DELETE,PATCH,POST,PUT',
                    },
                    {
                        key: 'Access-Control-Allow-Headers',
                        value: 'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version',
                    },
                ],
            },
        ]
    },
}

module.exports = withBundleAnalyzer(nextConfig)
