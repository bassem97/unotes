import prisma from '../lib/prisma'

export async function coursesSeeder() {
    /**
     * Default User
     */
    const defaultUser = await prisma.user.findUnique({
        where: { username: 'unotes' },
    })

    /**
     * University Of Ottawa
     */
    const ottawa = await prisma.school.findFirst({
        where: { name: 'University Of Ottawa' },
    })

    /**
     * Carleton University
     */
    const carleton = await prisma.school.findFirst({
        where: { name: 'Carleton University' },
    })

    /**
     * ESPRIT University
     */
    const esprit = await prisma.school.findFirst({
        where: { name: 'ESPRIT University' },
    })

    /**
     * Linear Algebra
     */
    let linearAlgebra = await prisma.course.findFirst({
        where: { name: 'Linear Algebra' },
    })
    if (!linearAlgebra && defaultUser && ottawa) {
        linearAlgebra = await prisma.course.create({
            data: {
                name: 'Linear Algebra',
                code: 'MAT1341',
                schoolId: ottawa.id,
                moderators: {
                    connect: {
                        id: defaultUser.id,
                    },
                },
            },
        })
        console.log({ linearAlgebra })
    } else console.warn('   - LINEAR ALGEBRA COURSE ALREADY EXISTS')

    /**
     * Data Structures and Algorithms
     */
    let dataStructuresAndAlgorithms = await prisma.course.findFirst({
        where: { name: 'Data Structures and Algorithms' },
    })
    if (!dataStructuresAndAlgorithms && defaultUser && ottawa) {
        dataStructuresAndAlgorithms = await prisma.course.create({
            data: {
                name: 'Data Structures and Algorithms',
                code: 'CSI2110',
                schoolId: ottawa.id,
                moderators: {
                    connect: {
                        id: defaultUser.id,
                    },
                },
            },
        })
        console.log({ dataStructuresAndAlgorithms })
    } else
        console.warn(
            '   - DATA STRUCTURES AND ALGORITHMS COURSE ALREADY EXISTS',
        )

    /**
     * Discrete Structures
     */
    let discreteStructures = await prisma.course.findFirst({
        where: { name: 'Discrete Structures' },
    })
    if (!discreteStructures && defaultUser && carleton) {
        discreteStructures = await prisma.course.create({
            data: {
                name: 'Discrete Structures',
                code: 'COMP1805',
                schoolId: carleton.id,
                moderators: {
                    connect: {
                        id: defaultUser.id,
                    },
                },
            },
        })
        console.log({ discreteStructures })
    } else console.warn('   - DISCRETE STRUCTURES COURSE ALREADY EXISTS')

    /**
     * Introduction to Computer Science II
     */
    let introductionToComputerScienceII = await prisma.course.findFirst({
        where: { name: 'Introduction to Computer Science II' },
    })
    if (!introductionToComputerScienceII && defaultUser && carleton) {
        introductionToComputerScienceII = await prisma.course.create({
            data: {
                name: 'Introduction to Computer Science II',
                code: 'COMP1406',
                schoolId: carleton.id,
                moderators: {
                    connect: {
                        id: defaultUser.id,
                    },
                },
            },
        })
        console.log({ introductionToComputerScienceII })
    } else
        console.warn(
            '   - INTRODUCTION TO COMPUTER SCIENCE II COURSE ALREADY EXISTS',
        )

    /**
     * Server-Side Web Programming
     */
    let serverSideWebProgramming = await prisma.course.findFirst({
        where: { name: 'Server-Side Web Programming' },
    })
    if (!serverSideWebProgramming && defaultUser && esprit) {
        serverSideWebProgramming = await prisma.course.create({
            data: {
                name: 'Server-Side Web Programming',
                code: 'WEB01',
                schoolId: esprit.id,
                moderators: {
                    connect: {
                        id: defaultUser.id,
                    },
                },
            },
        })
        console.log({ serverSideWebProgramming })
    } else
        console.warn('   - SERVER-SIDE WEB PROGRAMMING COURSE ALREADY EXISTS')

    /**
     * Web Programming
     */
    let webProgramming = await prisma.course.findFirst({
        where: { name: 'Web Programming' },
    })
    if (!webProgramming && defaultUser && esprit) {
        webProgramming = await prisma.course.create({
            data: {
                name: 'Web Programming',
                code: 'WEB02',
                schoolId: esprit.id,
                moderators: {
                    connect: {
                        id: defaultUser.id,
                    },
                },
            },
        })
        console.log({ webProgramming })
    } else console.warn('   - WEB PROGRAMMING COURSE ALREADY EXISTS')
}
