// create an async function that take a number as parameter and loop through it to create a schools with a name like this: school 1, school 2, school 3, etc...
// and a username defaultUser
import prisma from '../lib/prisma'

export async function dynamicSchoolSeeder(numberOfSchools: number) {
    /**
     * Default User
     */
    const defaultUser = await prisma.user.findUnique({
        where: { username: 'unotes 2' },
    })

    if (defaultUser)
        for (let i = 0; i < numberOfSchools; i++) {
            let school = await prisma.school.findFirst({
                where: { name: `school ${i}` },
            })
            if (!school) {
                school = await prisma.school.create({
                    data: {
                        name: `school ${i}`,
                        userId: defaultUser.id,
                    },
                })
                console.log({ school })
            } else console.warn(`   - school ${i} ALREADY EXISTS`)
        }
    else console.warn(`   - defaultUser DOES NOT EXISTS`)
}
