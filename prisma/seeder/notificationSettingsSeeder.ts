import prisma from '../lib/prisma'

export async function notificationSettingsSeeder() {
    let notificationSetting1 = await prisma.notificationSettings.findUnique({
        where: { type: 'FAVORITE_SCHOOL' },
    })
    let notificationSetting2 = await prisma.notificationSettings.findUnique({
        where: { type: 'FAVORITE_SCHOOL' },
    })
    if (!notificationSetting1 && !notificationSetting2) {
        notificationSetting1 = await prisma.notificationSettings.upsert({
            where: { type: 'FAVORITE_SCHOOL' },
            update: {},
            create: {
                type: 'FAVORITE_SCHOOL',
                label: 'notify me when my favorite school has a new event',
            },
        })
        notificationSetting2 = await prisma.notificationSettings.upsert({
            where: { type: 'COMMENT_ON_DOCUMENT' },
            update: {},
            create: {
                type: 'COMMENT_ON_DOCUMENT',
                label: 'notify me when someone comments on my document',
            },
        })
        console.log({ notificationSetting1, notificationSetting2 })
    } else console.warn('   - NOTIFICATION SETTINGS ALREADY EXISTS')
}
