import prisma from '../lib/prisma'

export async function schoolsSeeder() {
    /**
     * Default User
     */
    const defaultUser = await prisma.user.findUnique({
        where: { username: 'unotes' },
    })

    /**
     * University Of Ottawa
     */
    let ottawa = await prisma.school.findFirst({
        where: { name: 'University Of Ottawa' },
    })
    if (!ottawa && defaultUser) {
        ottawa = await prisma.school.create({
            data: {
                name: 'University Of Ottawa',
                userId: defaultUser.id,
            },
        })
        console.log({ ottawa })
    } else console.warn('   - OTTAWA SCHOOL ALREADY EXISTS')

    /**
     * Carleton University
     */
    let carleton = await prisma.school.findFirst({
        where: { name: 'Carleton University' },
    })
    if (!carleton && defaultUser) {
        carleton = await prisma.school.create({
            data: {
                name: 'Carleton University',
                userId: defaultUser.id,
            },
        })
        console.log({ carleton })
    } else console.warn('   - CARLETON SCHOOL ALREADY EXISTS')

    /**
     *  ESPRIT University
     */
    let esprit = await prisma.school.findFirst({
        where: { name: 'ESPRIT University' },
    })
    if (!esprit && defaultUser) {
        esprit = await prisma.school.create({
            data: {
                name: 'ESPRIT University',
                userId: defaultUser.id,
            },
        })
        console.log({ esprit })
    }
}
