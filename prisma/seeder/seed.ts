import { notificationSettingsSeeder } from './notificationSettingsSeeder'
import prisma from '../lib/prisma'
import { usersSeeder } from './userSeeder'
import { schoolsSeeder } from './schoolSeeder'
import { coursesSeeder } from './coursesSeeder'
import { tagsSeeder } from './tagsSeeder'

async function main() {
    await notificationSettingsSeeder()
    await usersSeeder()
    await schoolsSeeder()
    await coursesSeeder()
    await tagsSeeder()
    // await dynamicSchoolSeeder(100)
}
main()
    .then(async () => {
        await prisma.$disconnect()
    })
    .catch(async e => {
        console.error(e)
        await prisma.$disconnect()
        process.exit(1)
    })
