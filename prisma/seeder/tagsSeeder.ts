import prisma from '../lib/prisma'

export async function tagsSeeder() {
    const tags = await prisma.tag.findMany()
    if (tags.length > 0) console.warn('   - TAGS ALREADY EXISTS')
    else {
        await prisma.tag.create({
            data: {
                label: 'New',
            },
        })
        await prisma.tag.create({
            data: {
                label: 'Important',
            },
        })
        await prisma.tag.create({
            data: {
                label: 'Urgent',
            },
        })
        await prisma.tag.create({
            data: {
                label: 'Helpful',
            },
        })
    }
}
