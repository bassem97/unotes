import prisma from '../lib/prisma'

export async function usersSeeder() {
    let defaultUser = await prisma.user.findUnique({
        where: { email: 'unotes.g@gmail.com' },
    })
    if (!defaultUser) {
        defaultUser = await prisma.user.upsert({
            where: { email: 'unotes.g@gmail.com' },
            update: {},
            create: {
                email: 'unotes.g@gmail.com',
                name: 'unotes',
                firstName: 'unotes',
                lastName: 'unotes',
                username: 'unotes',
                status: 'ACTIVE_USER',
                password:
                    '$2a$10$Dqho6kQU0.WOHzIY8FBivuGC.fWO7wafLeRP3nPNxvZhuZtBrM8YW', // 123456
            },
        })
        console.log({ defaultUser })
    } else console.warn('   - DEFAULT USER ALREADY EXISTS')
}
