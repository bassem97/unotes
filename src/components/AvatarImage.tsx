import React, { FC } from 'react'
import Image from 'next/image'

type Props = {
    imageUrl?: string
}

const AvatarImage: FC<Props> = ({ imageUrl }) => {
    return (
        <div className="relative rounded-full h-12 aspect-square flex object-contain border dark:border-primary border-primary">
            <Image
                fill
                style={{ objectFit: 'contain' }}
                sizes="(max-width: 100px) 40px, 80px"
                src={imageUrl || '/avatar.png'}
                alt="avatar"
                className="rounded-full"
            />
        </div>
    )
}

export default AvatarImage
