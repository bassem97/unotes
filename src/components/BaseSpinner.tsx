import Image from 'next/image'

type Props = {
    img?: string
}
const baseSpinner = ({ img }: Props) => {
    return (
        <div className="relative w-full h-full">
            <Image
                className="object-cover"
                src={img ?? '/spinner.gif'}
                sizes={'100%'}
                priority
                fill
                alt=""
            />
        </div>
    )
}

export default baseSpinner
