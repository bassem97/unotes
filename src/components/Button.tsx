import { ReactNode } from 'react'

type Props = {
    label: string
    icon?: JSX.Element
    children?: ReactNode
    disabled?: boolean
    classname?: string
    variant?: string
    onClick?: (e?: any) => void
}

const Button = ({
    onClick,
    label,
    children,
    icon,
    disabled,
    classname = 'py-2 px-4',
    variant = 'primary',
}: Props) => {
    return (
        <div
            onClick={onClick}
            role="button"
            className={
                'flex items-center text-bg gap-2 whitespace-nowrap h-fit w-fit rounded-2xl capitalize font-semibold font-poppins  ' +
                (disabled
                    ? 'cursor-not-allowed'
                    : 'cursor-pointer hover:bg-opacity-90 dark:hover:bg-opacity-80') +
                ' ' +
                (classname ?? ' text-sm xs:text-xs') +
                (variant === 'primary'
                    ? ' border-primary bg-primary dark:bg-primarysec dark:border-primarysec '
                    : ' border-white  bg-transparent')
            }
            style={{ pointerEvents: disabled ? 'none' : 'auto' }}
        >
            {icon && icon}
            {label}
            {children}
        </div>
    )
}

export default Button
