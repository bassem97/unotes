import { Course, Document, School } from '@prisma/client'
import Image from 'next/image'
import Link from 'next/link'
import { Button } from '@/components/index'
import { useSession } from 'next-auth/react'
import { Session } from 'next-auth'
import { gql, useMutation } from '@apollo/client'
import { likeDocument, unDislikeDocument } from '@/graphql/typeDefs'

interface Props {
    item: Course | School | Document
}

const likeDocumentMutation = gql`
    mutation likeDocument($id: String!, $userId: String!) {
        likeDocument(id: $id, userId: $userId) {
            id
        }
    }
`

const unlikeDocumentMutation = gql`
    mutation unLikeDocument($id: String!, $userId: String!) {
        unLikeDocument(id: $id, userId: $userId) {
            id
        }
    }
`

const dislikeDocumentMutation = gql`
    mutation dislikeDocument($id: String!, $userId: String!) {
        dislikeDocument(id: $id, userId: $userId) {
            id
        }
    }
`

const unDislikeDocumentMutation = gql`
    mutation unDislikeDocument($id: String!, $userId: String!) {
        unDislikeDocument(id: $id, userId: $userId) {
            id
        }
    }
`

const Card = ({ item }: Props) => {
    /**
     * destructuring the session object to get the user id
     */
    const { data } = useSession() as unknown as { data: Session }

    const [likeDocument] = useMutation(likeDocumentMutation, {
        variables: {
            id: item.id,
            userId: data?.user?.id,
        },
        onCompleted: () => {
            console.log('liked')
        },
    })

    const [unlikeDocument] = useMutation(unlikeDocumentMutation, {
        variables: {
            id: item.id,
            userId: data?.user?.id,
        },
        onCompleted: () => {
            console.log('unLiked')
        },
    })

    const [dislikeDocument] = useMutation(dislikeDocumentMutation, {
        variables: {
            id: item.id,
            userId: data?.user?.id,
        },
        onCompleted: () => {
            console.log('disliked')
        },
    })

    const [unDislikeDocument] = useMutation(unDislikeDocumentMutation, {
        variables: {
            id: item.id,
            userId: data?.user?.id,
        },
        onCompleted: () => {
            console.log('undisliked')
        },
    })

    const handleLike = async () => {
        // await likeDocument();
        // await unlikeDocument();
        // await dislikeDocument();
        // await unDislikeDocument();
    }

    return (
        <>
            <Link
                href={'/documents/' + item.id}
                className={
                    'bg-bg relative rounded-3xl flex transition-all w-full aspect-video max-h-fit ' +
                    'hover:scale-105 cursor-pointer shadow-[0_0_5px_1px_#00000050] '
                }
            >
                <Image
                    src={'/photo-1497091071254-cc9b2ba7c48a.avif'}
                    alt="placeholder"
                    fill
                    className="w-full rounded-3xl object-cover"
                />
                <div className="flex rounded-3xl flex-col items-center justify-center gap-1 w-full h-full absolute top-0 left-0 bg-black bg-opacity-40">
                    <h1
                        className={
                            'text-2xl leading-5 font-thin text-bg uppercase text-center whitespace-nowrap' +
                            '  2xl:text-xl xl:text-base md:text-2xl xs:text-xl'
                        }
                    >
                        {item.name}
                    </h1>
                </div>
            </Link>
        </>
    )
}

export default Card
