import { gql, useMutation } from '@apollo/client'
import { useState } from 'react'
import { User } from '@prisma/client'
import { AiTwotoneDislike, AiTwotoneLike } from 'react-icons/ai'
import { BsFillTrashFill } from 'react-icons/bs'
import AvatarImage from '@/components/AvatarImage'
import CircularProgress from '@mui/material/CircularProgress'

const likeCommentMutation = gql`
    mutation likeComment($id: String!, $userId: String!) {
        likeComment(id: $id, userId: $userId) {
            id
        }
    }
`

const unlikeCommentMutation = gql`
    mutation unLikeComment($id: String!, $userId: String!) {
        unLikeComment(id: $id, userId: $userId) {
            id
        }
    }
`

const dislikeCommentMutation = gql`
    mutation dislikeComment($id: String!, $userId: String!) {
        dislikeComment(id: $id, userId: $userId) {
            id
        }
    }
`

const unDislikeCommentMutation = gql`
    mutation unDislikeComment($id: String!, $userId: String!) {
        unDislikeComment(id: $id, userId: $userId) {
            id
        }
    }
`

const deleteCommentMutation = gql`
    mutation deleteComment($id: String!) {
        deleteComment(id: $id) {
            id
        }
    }
`

interface Props {
    comment: any
    userId?: string
    documentUserId?: string
    removeComment?: (id: string) => void
}

const CommentCard = ({
    comment: commentProp,
    userId,
    documentUserId,
    removeComment,
}: Props) => {
    const [comment, setComment] = useState(commentProp)
    const canDeleteComment =
        comment.user.id === userId || documentUserId === userId

    const [deleteComment, { loading: loadingDelete }] = useMutation(
        deleteCommentMutation,
        {
            variables: {
                id: comment.id,
            },
            onCompleted: () => {
                if (removeComment) {
                    removeComment(comment.id)
                }
            },
        },
    )

    const [likeComment] = useMutation(likeCommentMutation, {
        variables: {
            id: comment.id,
            userId,
        },
    })

    const [unlikeComment] = useMutation(unlikeCommentMutation, {
        variables: {
            id: comment.id,
            userId,
        },
        onCompleted: () => {
            setComment({
                ...comment,
                likedBy: comment.likedBy.filter(
                    (user: User) => user.id !== userId,
                ),
            })
        },
    })

    const [dislikeComment] = useMutation(dislikeCommentMutation, {
        variables: {
            id: comment.id,
            userId,
        },
    })

    const [unDislikeComment] = useMutation(unDislikeCommentMutation, {
        variables: {
            id: comment.id,
            userId,
        },
        onCompleted: () => {
            setComment({
                ...comment,
                dislikedBy: comment.dislikedBy.filter(
                    (user: User) => user.id !== userId,
                ),
            })
        },
    })

    const handleLikeClick = async () => {
        const likedByUser = comment.likedBy.find(
            (user: User) => user.id === userId,
        )
        if (likedByUser) await unlikeComment()
        else await handleLike(likedByUser)
    }

    const handleDislikeClick = async () => {
        const dislikedByUser = comment.dislikedBy.find(
            (user: User) => user.id === userId,
        )
        if (dislikedByUser) await unDislikeComment()
        else await handleDislike(dislikedByUser)
    }

    const handleLike = async (likedByUser: User) => {
        setComment({
            ...comment,
            likedBy: likedByUser
                ? comment.likedBy
                : [...comment.likedBy, { id: userId }],
            dislikedBy: comment.dislikedBy.filter(
                (user: User) => user.id !== userId,
            ),
        })
        await likeComment()
    }

    const handleDislike = async (dislikedByUser: User) => {
        setComment({
            ...comment,
            likedBy: comment.likedBy.filter((user: User) => user.id !== userId),
            // push the userId to the dislikedBy array if it doesn't exist
            dislikedBy: dislikedByUser
                ? comment.dislikedBy
                : [...comment.dislikedBy, { id: userId }],
        })
        await dislikeComment()
    }

    const isUserConnected = !!userId // Check if user is connected

    return (
        <div className="bg-white p-4 rounded-md shadow-md mb-4 flex w-full border dark:bg-darkbg">
            <AvatarImage imageUrl={comment.user?.images} />
            <div className="ml-4 flex-grow dark:text-bg">
                <div className="flex justify-between comments-center ">
                    <p className="text-gray-800 font-bold dark:text-bg">
                        {comment.user.name || comment.user.username}
                    </p>
                    {userId && (
                        <div className="flex justify-between comments-center">
                            {canDeleteComment &&
                                (loadingDelete ? (
                                    <CircularProgress size={20} />
                                ) : (
                                    <BsFillTrashFill
                                        className="text-red-500 hover:text-red-600 text-xl cursor-pointer"
                                        onClick={() => deleteComment()}
                                    />
                                ))}
                        </div>
                    )}
                </div>
                <p className="text-gray-800 dark:text-bg">{comment.body}</p>
                <div className="flex space-x-3 justify-end mt-4">
                    {isUserConnected ? (
                        <>
                            <div className="flex gap-1 items-center">
                                <span className="text-xs">
                                    {comment.likedBy?.length || 0}
                                </span>
                                <AiTwotoneLike
                                    onClick={handleLikeClick}
                                    className={`cursor-pointer ${
                                        comment.likedBy.find(
                                            (user: User) => user.id === userId,
                                        )
                                            ? 'text-primary dark:text-primarysec'
                                            : 'text-gray-500 dark:text-gray-400'
                                    } hover:opacity-90`}
                                />
                            </div>
                            <div className="flex gap-1 items-center">
                                <span className="text-xs">
                                    {comment.dislikedBy?.length || 0}
                                </span>
                                <AiTwotoneDislike
                                    onClick={handleDislikeClick}
                                    className={`cursor-pointer ${
                                        comment.dislikedBy.find(
                                            (user: User) => user.id === userId,
                                        )
                                            ? 'text-red-500 hover:text-red-600'
                                            : 'text-gray-500 dark:text-gray-400'
                                    }`}
                                />
                            </div>
                        </>
                    ) : (
                        <>
                            <div className="flex gap-1 items-center">
                                <span className="text-xs">
                                    {comment.likedBy?.length || 0}
                                </span>
                                <AiTwotoneLike className="text-gray-500 dark:text-gray-400" />
                            </div>
                            <div className="flex gap-1 items-center">
                                <span className="text-xs">
                                    {comment.dislikedBy?.length || 0}
                                </span>
                                <AiTwotoneDislike className="text-gray-500 dark:text-gray-400" />
                            </div>
                        </>
                    )}
                </div>
            </div>
        </div>
    )
}

export default CommentCard
