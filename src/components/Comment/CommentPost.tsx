import { Button, Input } from '@/components'
import Box from '@mui/material/Box'
import CircularProgress from '@mui/material/CircularProgress'
import * as React from 'react'
import { FormikErrors, FormikTouched } from 'formik/dist/types'
import { FC } from 'react'

type Values = {
    body: string
}

type Props = {
    handleSubmit: (e?: React.FormEvent<HTMLFormElement>) => void
    setFieldValue: (
        field: string,
        value: any,
        shouldValidate?: boolean,
    ) => Promise<FormikErrors<Values>> | Promise<void>
    values: Values
    errors: FormikErrors<Values>
    touched: FormikTouched<Values>
    isSubmitting: boolean
}

const CommentPost: FC<Props> = (props: Props) => {
    const {
        handleSubmit,
        setFieldValue,
        values,
        errors,
        touched,
        isSubmitting,
    } = props
    return (
        <form className="my-2 w-full " onSubmit={handleSubmit}>
            <Input
                label="Comment"
                handleChange={e => setFieldValue('body', e.target.value)}
                value={values.body}
            />
            {errors.body && touched.body && (
                <div className="text-red-500 text-xs italic">
                    {errors.body?.toString()}
                </div>
            )}
            {isSubmitting ? (
                <div className="flex w-full justify-center h-4">
                    <Box
                        sx={{
                            display: 'flex',
                            height: 4,
                        }}
                    >
                        <CircularProgress size={25} />
                    </Box>
                </div>
            ) : (
                <Button
                    classname="my-2 bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 ml-2 rounded"
                    label=""
                >
                    <button type="submit">Post</button>
                </Button>
            )}
        </form>
    )
}

export default CommentPost
