import { FC, Fragment } from 'react'
import { Comment, Document } from '@prisma/client'
import CommentCard from '@/components/Comment/CommentCard'
import { TemporaryFileDocumentType } from '@/types/TemporaryFileDocumentType'
import { Session, User } from 'next-auth'

type Props = {
    comments: Comment[]
    userData: Session | null
    documentModel: Document & { files: TemporaryFileDocumentType[] }
    removeComment: (id: string) => void
}

const CommentsList: FC<Props> = (props: Props) => {
    const { comments, userData, documentModel, removeComment } = props
    return (
        <>
            {comments.map((comment: Comment) => (
                <Fragment key={comment.id}>
                    {userData ? (
                        <CommentCard
                            comment={comment}
                            userId={userData.user.id}
                            documentUserId={documentModel.userId}
                            removeComment={removeComment}
                        />
                    ) : (
                        <CommentCard comment={comment} />
                    )}
                </Fragment>
            ))}
        </>
    )
}

export default CommentsList
