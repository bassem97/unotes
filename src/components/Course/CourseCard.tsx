import { Course } from '@prisma/client'
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'
import { gql, useMutation } from '@apollo/client'
import { AiFillStar, AiOutlineStar } from 'react-icons/ai'
import CircularProgress from '@mui/material/CircularProgress'

interface Props {
    course: Course
    userId?: string
    isFavorite?: boolean
}

const addCourseToFavoritesMutation = gql`
    mutation addCourseToFavoritesMutation(
        $userId: String!
        $courseId: String!
    ) {
        addCourseToFavorites(userId: $userId, courseId: $courseId) {
            id
            name
        }
    }
`

const removeCourseToFavoritesMutation = gql`
    mutation removeCourseToFavoritesMutation(
        $userId: String!
        $courseId: String!
    ) {
        removeCourseFromFavorites(userId: $userId, courseId: $courseId) {
            id
            name
        }
    }
`

const CourseCard = ({ course, userId, isFavorite }: Props) => {
    const [isFavoriteCourse, setIsFavoriteCourse] = useState(isFavorite)

    const [addCourseToFavorites, { loading: addCourseLoading }] = useMutation(
        addCourseToFavoritesMutation,
        {
            onCompleted: () => {
                setIsFavoriteCourse(true)
            },
        },
    )

    const [removeCourseToFavorites, { loading: removeCourseLoading }] =
        useMutation(removeCourseToFavoritesMutation, {
            onCompleted: () => {
                setIsFavoriteCourse(false)
            },
        })

    const handleAddCourseToFavorites = async (add: boolean) => {
        try {
            if (add) {
                await addCourseToFavorites({
                    variables: { userId, courseId: course.id },
                })
            } else {
                await removeCourseToFavorites({
                    variables: { userId, courseId: course.id },
                })
            }
        } catch (error) {
            console.error('Error adding/removing from favorites:', error)
        }
    }
    return (
        <div className="relative hover:scale-105 h-fit">
            {userId && (
                <>
                    {isFavoriteCourse ? (
                        <div
                            onClick={() => handleAddCourseToFavorites(false)}
                            className="absolute top-0 right-0 z-20 py-2 px-4 text-yellow-300"
                        >
                            {removeCourseLoading ? (
                                <CircularProgress size={25} />
                            ) : (
                                <AiFillStar
                                    className="cursor-pointer text-2xl"
                                    title="Remove from favorites"
                                />
                            )}
                        </div>
                    ) : (
                        <div
                            onClick={() => handleAddCourseToFavorites(true)}
                            className="absolute top-0 right-0 z-20 py-2 px-4 text-yellow-300"
                        >
                            {addCourseLoading ? (
                                <CircularProgress size={25} />
                            ) : (
                                <AiOutlineStar
                                    title="Add to favorites"
                                    className="cursor-pointer text-2xl"
                                />
                            )}
                        </div>
                    )}
                </>
            )}
            <Link
                href={'/courses/' + course.id.toString()}
                className={
                    'bg-bg school-card relative rounded-3xl flex transition-all w-full aspect-square max-h-fit ' +
                    'cursor-pointer shadow-[0_0_5px_1px_#00000050] '
                }
            >
                <Image
                    src={
                        course.image || '/photo-1497091071254-cc9b2ba7c48a.avif'
                    }
                    alt="placeholder"
                    fill
                    className="w-full rounded-3xl object-cover"
                    sizes={
                        '(min-width: 1024px) 33vw, (min-width: 768px) 50vw, 100vw'
                    }
                    priority={true}
                />
                <div className="flex rounded-3xl flex-col items-center justify-end gap-1 w-full h-full absolute top-0 left-0 bg-black bg-opacity-40">
                    <h1
                        className={
                            'text-lg pb-8 w-full overflow-hidden text-ellipsis leading-5 font-thin text-bg uppercase text-center whitespace-nowrap' +
                            '  2xl:text-sm xl:text-xs md:text-xl sm:text-base xs:text-xl'
                        }
                    >
                        {course.name}
                    </h1>
                </div>
            </Link>
        </div>
    )
}

export default CourseCard
