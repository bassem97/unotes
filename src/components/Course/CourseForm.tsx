import { useFormik } from 'formik'
import { Button, Input, SelectForm, SwitchInput } from '@/components'
import BaseSpinner from '@/components/BaseSpinner'
import { School, User } from '@prisma/client'
import * as Yup from 'yup'
import { gql, useMutation } from '@apollo/client'
import { useSocket } from '@/hooks/useSocket'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/react'
import { Session } from 'next-auth'
import { Option } from '@/types/OptionType'
import { TemporaryCourseType } from '@/types/TemporaryCourseType'
import useUpup from '@/hooks/useUpup'
import { UploadAdapter, UpupUploader } from '@bassem97/upup'

type Props = {
    schools: Array<School>
    users: Array<User>
    course?: TemporaryCourseType
    setResult: (result: string) => void
    setRedirectTo: (path: string) => void
}

const validationSchema = Yup.object().shape({
    name: Yup.string().min(2).required('Required'),
    code: Yup.string().min(2).required('Required'),
})

const createCourseMutation = gql`
    mutation createCourseMutation($input: CreateCourseInput!) {
        createCourse(input: $input) {
            id
            name
        }
    }
`
const updateCourseMutation = gql`
    mutation updateCourseMutation($input: UpdateCourseInput!) {
        updateCourse(input: $input) {
            id
            name
        }
    }
`

const CourseForm = ({
    schools,
    users,
    course,
    setResult,
    setRedirectTo,
}: Props) => {
    const { socket } = useSocket()
    const { query: { school_id } = {} } = useRouter()
    const { data: userData } = useSession() as { data: Session }
    const [imageURL, setImageURL] = useState<String | null | undefined>(
        course?.image,
    )

    /**
     * UpupUploader Configs
     */
    const { baseConfigs, cloudStorageConfigs, setCanUpload, keys } = useUpup()

    useEffect(() => {
        if (keys.length == 1) {
            setImageURL(process.env.NEXT_PUBLIC_IMAGE_ENDPOINT + '/' + keys[0])
            handleSubmit()
        }
    }, [keys])

    // remove connected user from users array
    users = users.filter((user: any) => user.value !== userData?.user?.id)

    const [createCourse] = useMutation(createCourseMutation, {
        onCompleted: async data => {
            setResult('Course Created')
            setRedirectTo('/courses/' + data.createCourse.id)
        },
        onError: error => {
            setResult(error.message)
        },
    })

    const [updateCourse] = useMutation(updateCourseMutation, {
        onCompleted: async () => {
            setResult('Course Updated')
            await socket.emit('createdMessage', {
                action: 'notification',
            })
            setRedirectTo('/courses/' + course?.id)
        },
        onError: error => {
            setResult(error.message)
        },
    })

    const {
        handleSubmit,
        errors,
        touched,
        handleBlur,
        setFieldValue,
        setErrors,
        isSubmitting,
        values,
    } = useFormik({
        initialValues: {
            id: course?.id ?? '',
            name: course?.name ?? '',
            code: course?.code ?? '',
            deleted: course?.deleted ?? false,
            verified: course?.verified ?? false,
            image: imageURL ?? '',
            schoolId: school_id ?? course?.schoolId ?? '',
            moderators: course?.moderators ?? [],
        },
        validationSchema,
        onSubmit: async values => {
            setCanUpload(true)
            const input = {
                code: values.code,
                image: imageURL,
                moderators: values.moderators.map(
                    (moderator: User) => moderator.id,
                ),
                name: values.name,
                schoolId: values.schoolId,
                verified: values.verified,
            }

            if (course)
                await updateCourse({
                    variables: {
                        input: {
                            ...input,
                            id: course.id,
                        },
                    },
                })
            else
                await createCourse({
                    variables: {
                        input: {
                            ...input,
                        },
                    },
                })
        },
    })

    const school = schools.find((s: School) => s.id === values.schoolId)

    return (
        <form
            onSubmit={handleSubmit}
            className="w-full flex gap-4 sm:flex-wrap"
        >
            <div className="w-1/2 px-6 flex flex-col gap-6 sm:w-full">
                <div className="flex flex-col gap-1 bg-transparent w-full">
                    <div
                        className={`rounded-3xl flex items-center
                      ${errors.name! && touched.name && 'border-red-500'} `}
                    >
                        <Input
                            label="name"
                            handleChange={e =>
                                setFieldValue('name', e.target.value)
                            }
                            value={values.name}
                        />
                    </div>
                    {errors.name && touched.name && (
                        <div className="text-red-500 text-xs italic">
                            {errors.name?.toString()}
                        </div>
                    )}
                </div>

                <div className="flex flex-col gap-1 bg-transparent w-full">
                    <div
                        className={`rounded-3xl flex items-center
                      ${errors.code! && touched.code && 'border-red-500'} `}
                    >
                        <Input
                            label="code"
                            handleChange={e =>
                                setFieldValue('code', e.target.value)
                            }
                            value={values.code}
                        />
                    </div>
                    {errors.code && touched.code && (
                        <div className="text-red-500 text-xs italic">
                            {errors.code?.toString()}
                        </div>
                    )}
                </div>

                <SelectForm
                    label="School"
                    options={schools.map((s: School) => ({
                        label: s.name,
                        value: s.id,
                    }))}
                    onChange={items => {
                        if (Array.isArray(items)) return
                        const item = items as Option
                        const school = schools.find(
                            (s: School) => s.id === item.value,
                        )
                        setFieldValue('schoolId', school?.id)
                    }}
                    selected={
                        school
                            ? { label: school?.name, value: school?.id }
                            : null
                    }
                />

                <SelectForm
                    label="Moderators"
                    options={users.map((moderator: User) => ({
                        label: moderator.name || moderator.username || '',
                        value: moderator.id,
                    }))}
                    onChange={items => {
                        setFieldValue(
                            'moderators',
                            Array.isArray(items)
                                ? items.map((item: Option) => {
                                      const mod = users.find(
                                          (u: User) => u.id === item.value,
                                      )
                                      return mod
                                  })
                                : [],
                        )
                    }}
                    selected={values.moderators.map((moderator: User) => ({
                        value: moderator.id,
                        label: moderator.name || moderator.username || '',
                    }))}
                    isMulti
                />
                <SwitchInput
                    label="Is Verified ?"
                    checked={values.verified}
                    onChange={checked => setFieldValue('verified', checked)}
                />
            </div>
            <div className="w-1/2 px-6 flex flex-col gap-2 sm:w-full">
                <div className="w-full">
                    <UpupUploader
                        baseConfigs={baseConfigs}
                        cloudStorageConfigs={cloudStorageConfigs}
                        uploadAdapters={[UploadAdapter.INTERNAL]}
                    />
                </div>

                {errors.image && touched.image && (
                    <div className="text-red-500 text-xs italic">
                        {errors.image?.toString()}
                    </div>
                )}

                {isSubmitting ? (
                    <BaseSpinner />
                ) : (
                    <div className="flex justify-center w-full">
                        <Button
                            classname="mt-4 py-3 px-5 text-lg mx-auto"
                            disabled={isSubmitting}
                            label=""
                        >
                            <button type="submit" className="w-full h-fit">
                                {course ? 'Update' : 'Create'} course
                            </button>
                        </Button>
                    </div>
                )}
            </div>
        </form>
    )
}

export default CourseForm
