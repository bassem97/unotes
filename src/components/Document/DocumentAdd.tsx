import { Button, SelectForm, SwitchInput } from '@/components'
import { Course, DocumentType, School, Season, Tag } from '@prisma/client'
import { useState } from 'react'
import { Carousel } from 'react-responsive-carousel'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import { UploadAdapter, UpupUploader } from '@bassem97/upup'
import { Option, toOption, toOptions } from '@/types/OptionType'
import { IoIosArrowBack, IoIosArrowForward } from 'react-icons/io'
import { TemporaryDocumentType } from '@/types/TemporaryDocumentType'
import { MultiValue, SingleValue } from 'react-select'
import DocumentForm from './DocumentForm'
import useUpup from '@/hooks/useUpup'
import { useFormik } from 'formik'
import { useSession } from 'next-auth/react'
import { Session } from 'next-auth'
import { array, bool, number, object, string } from 'yup'
import usePrevious from '@/hooks/usePrevious'
import { gql, useMutation } from '@apollo/client'
import { useSocket } from '@/hooks/useSocket'
import CircularProgress from '@mui/material/CircularProgress'
import Link from 'next/link'

interface Props {
    document?: TemporaryDocumentType
    course?: Course
    schools: School[]
    selectedCourse: Course
    selectedSchool: School
    tags: Tag[]
    setResult: (result: string) => void
    dragging: boolean
    setDragging: (dragging: boolean) => void
    setRedirectTo: (path: string) => void
}

/**
 *
 * @param documentToUpdate
 * @param schools
 * @param selectedCourse
 * @param selectedSchool
 * @param tags
 * @param setResult
 * @param setRedirectTo
 * @constructor
 */
const DocumentAdd = ({
    document: documentToUpdate,
    schools,
    selectedCourse,
    selectedSchool,
    tags,
    setResult,
    setRedirectTo,
}: Props) => {
    const [school, setSchool] = useState<School | null>(selectedSchool || null)
    const [files, setFiles] = useState<File[]>([])
    const [course, setCourse] = useState<Course | null>(selectedCourse || null)
    const [isMultipleDocs, setMultipleDocs] = useState(false)
    const [selectedTags, setSelectedTags] = useState<Tag[]>(
        documentToUpdate?.tags || [],
    )

    const { data: userData } = useSession() as { data: Session }
    const { socket } = useSocket()

    const addDocument = () => {
        setValues(prev => [
            ...prev,
            {
                name: documentToUpdate?.name || '',
                season: documentToUpdate?.season || Season.WINTER,
                type: documentToUpdate?.type || DocumentType.OTHERS,
                year: documentToUpdate?.year || new Date().getFullYear(),
                courseId: documentToUpdate?.courseId || course?.id || '',
                files: documentToUpdate?.files || [],
                userId: userData?.user?.id,
                isSolutionsIncluded:
                    documentToUpdate?.isSolutionsIncluded || false,
                isCompleted: documentToUpdate?.isCompleted || false,
                deleted: documentToUpdate?.deleted || false,
                verified: documentToUpdate?.verified || false,
                tags: documentToUpdate?.tags || [],
            } as TemporaryDocumentType,
        ])
    }
    const courses = school
        ? (
              schools.find(
                  (sch: School) => sch?.id === school?.id!,
              ) as School as any
          ).courses!
        : []

    const handleOnChangeCourses = (
        items: MultiValue<Option> | SingleValue<Option>,
    ) => {
        const foundCourse = courses.find(
            (s: Course) => s.id === (items as Option).value,
        )
        setCourse(foundCourse ?? null)
    }

    const handleOnChangeSchool = (
        items: MultiValue<Option> | SingleValue<Option>,
    ) => {
        const school = schools.find(
            (s: School) => s.id === (items as Option).value,
        )
        setSchool(school ?? null)
        setCourse(null)
    }
    const validationSchema = array().of(
        object().shape({
            name: string().required('Required'),
            season: string().required('Required'),
            type: string().required('Required'),
            year: number()
                .max(new Date().getFullYear() + 10)
                .min(2000)
                .required('Required'),
            isSolutionsIncluded: bool().optional(),
            isCompleted: bool().optional(),
        }),
    )

    const {
        baseConfigs,
        cloudStorageConfigs,
        setCanUpload,
        keys,
        googleConfigs,
        oneDriveConfigs,
    } = useUpup({ setFiles, isDocument: true, multiple: true })
    const document_endpoint = process.env.NEXT_PUBLIC_DOCUMENT_ENDPOINT + '/'
    const createDocumentMutation = gql`
        mutation createDocumentMutation($input: CreateDocumentInput!) {
            createDocument(input: $input) {
                id
            }
        }
    `
    const [createDocument, { loading: loadingCreateDocument }] = useMutation(
        createDocumentMutation,
        {
            onCompleted: async data => {
                setResult('Document Created')
                await socket.emit('createdMessage', {
                    action: 'notification',
                })
                setRedirectTo('/documents/' + data.createDocument?.id)
            },
            onError: error => {
                setResult(error.message)
            },
        },
    )
    const formik = useFormik({
        initialValues: [
            {
                name: documentToUpdate?.name || '',
                season: documentToUpdate?.season || Season.WINTER,
                type: documentToUpdate?.type || DocumentType.OTHERS,
                year: documentToUpdate?.year || new Date().getFullYear(),
                courseId: documentToUpdate?.courseId || course?.id || '',
                files: documentToUpdate?.files || [],
                userId: userData?.user?.id,
                isSolutionsIncluded:
                    documentToUpdate?.isSolutionsIncluded || false,
                isCompleted: documentToUpdate?.isCompleted || false,
                deleted: documentToUpdate?.deleted || false,
                verified: documentToUpdate?.verified || false,
                tags: documentToUpdate?.tags || [],
            } as TemporaryDocumentType,
        ],
        validationSchema,
        onSubmit: async values => {
            setCanUpload(true)

            if (course?.id) {
                values[0].year = parseInt(values[0].year.toString())
                if (documentToUpdate) {
                    // await updateDocument({
                    //     variables: {
                    //         input: {
                    //             ...values,
                    //             courseId: course!.id,
                    //             tags,
                    //         },
                    //         id: documentToUpdate.id,
                    //     },
                    // })
                } else {
                    if (keys.length !== 0) {
                        values[0].files = [
                            {
                                url: document_endpoint + keys,
                            },
                        ]
                        await createDocument({
                            variables: {
                                input: {
                                    ...values[0],
                                    tags: values[0].tags?.map(t => t.id) || [],
                                    files: values[0].files.map(f => f.url),
                                    courseId: course!.id,
                                },
                            },
                        })
                    } else throw new Error('No file uploaded')
                }
            } else throw new Error('Course is required')
        },
    })
    const { values, isSubmitting, handleSubmit, setValues } = formik

    const previousDocuments = usePrevious(values)

    return (
        <div
            className={
                'w-full flex flex-wrap content-start gap-4 h-full px-8 ' +
                (!documentToUpdate ? '' : 'flex justify-center')
            }
        >
            <div className="flex flex-col w-full gap-4">
                <h1 className="w-full h-fit text-2xl pointer-events-none dark:text-bg font-bold text-center">
                    New Document
                </h1>
                <div className="w-full z-[90] mx-auto mb-2 flex gap-4 h-fit">
                    <SelectForm
                        label="School"
                        options={toOptions(schools)}
                        onChange={handleOnChangeSchool}
                        disabled={!!documentToUpdate}
                        selected={school && toOption(school)}
                        noOptions={() => (
                            <Link
                                href={{
                                    pathname: '/schools/new',
                                    query: { callBackUrl: '/documents/new' },
                                }}
                                className="hover:text-primary dark:hover:text-bg"
                            >
                                <p> Could Not Find A School?</p>
                                <p className="font-bold py-1">Add it here.</p>
                            </Link>
                        )}
                    />
                    <SelectForm
                        label="Course"
                        options={toOptions(courses)}
                        onChange={handleOnChangeCourses}
                        disabled={!school || !!documentToUpdate}
                        selected={course && toOption(course)}
                        noOptions={() => (
                            <Link
                                className="hover:text-primary dark:hover:text-bg"
                                href={{
                                    pathname: '/courses/new',
                                    query: {
                                        school_id: school?.id,
                                        callBackUrl:
                                            '/documents/new?school_id=' +
                                            school?.id,
                                    },
                                }}
                            >
                                <p>A Course is Missing?</p>
                                <p className="font-bold py-1">Add it here.</p>
                            </Link>
                        )}
                    />
                </div>
            </div>

            <form
                className="w-full flex gap-4 sm:flex-wrap"
                onSubmit={handleSubmit}
            >
                <div className="w-1/2 px-6 flex flex-col gap-2 sm:w-full">
                    <div className="w-full">
                        <UpupUploader
                            baseConfigs={baseConfigs}
                            cloudStorageConfigs={cloudStorageConfigs}
                            googleConfigs={googleConfigs}
                            oneDriveConfigs={oneDriveConfigs}
                            uploadAdapters={[
                                UploadAdapter.INTERNAL,
                                UploadAdapter.GOOGLE_DRIVE,
                                UploadAdapter.ONE_DRIVE,
                            ]}
                        />
                        {/*<SwitchInput*/}
                        {/*    label="Multiple Documents?"*/}
                        {/*    checked={isMultipleDocs}*/}
                        {/*    onChange={checked => setMultipleDocs(checked)}*/}
                        {/*/>*/}
                    </div>

                    <div className="flex justify-center w-full h-fit">
                        {loadingCreateDocument ? (
                            <div className="mt-4 mx-auto">
                                <CircularProgress size={40} />
                            </div>
                        ) : (
                            <Button
                                classname="mt-4 py-3 px-5 text-lg mx-auto"
                                label=""
                            >
                                <button type="submit" className="w-full h-fit">
                                    Create document
                                </button>
                            </Button>
                        )}
                    </div>
                </div>
                <div className="w-full h-full sm:h-fit">
                    {!isMultipleDocs ? (
                        <div
                            style={{
                                borderColor:
                                    files && files.length > 0
                                        ? ''
                                        : 'transparent',
                            }}
                            className="w-full h-full border-2 sm:mb-4 rounded-lg border-primary"
                        >
                            <Carousel
                                className="h-full"
                                statusFormatter={(current, total) =>
                                    `${current} / ${total}`
                                }
                                showThumbs={false}
                                axis="horizontal"
                                renderArrowNext={(clickHandler, hasNext) =>
                                    hasNext && (
                                        <div className="h-full absolute top-0 right-0 flex items-center z-20">
                                            <IoIosArrowForward
                                                onClick={clickHandler}
                                                className="text-primary text-4xl cursor-pointer"
                                            />
                                        </div>
                                    )
                                }
                                renderArrowPrev={(clickHandler, hasNext) =>
                                    hasNext && (
                                        <div className="h-full absolute top-0 left-0 flex items-center z-20">
                                            <IoIosArrowBack
                                                onClick={clickHandler}
                                                className="text-primary text-4xl cursor-pointer"
                                            />
                                        </div>
                                    )
                                }
                            >
                                {files.map((file, index) => (
                                    <DocumentForm
                                        setValues={setValues}
                                        fileName={file.name}
                                        key={index}
                                        course={course}
                                        school={school}
                                        tags={tags}
                                        setResult={setResult}
                                        dragging={false}
                                        setDragging={() => {}}
                                        setRedirectTo={setRedirectTo}
                                        document={values[0]}
                                        index={index}
                                    />
                                ))}
                            </Carousel>
                        </div>
                    ) : (
                        <></>
                    )}
                </div>
            </form>
        </div>
    )
}
export default DocumentAdd
