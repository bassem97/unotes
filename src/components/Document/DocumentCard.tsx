import FileTypeIcon from '@/types/FileTypeIcon'
import { TemporaryDocumentType } from '@/types/TemporaryDocumentType'
import { Tag } from '@prisma/client'
import Link from 'next/link'
import { AiOutlineCheck } from 'react-icons/ai'
import { ImCross } from 'react-icons/im'

interface Props {
    document: TemporaryDocumentType
}

const DocumentCard = ({ document }: Props) => {
    return (
        <Link
            href={'/documents/' + document.id?.toString()}
            className={
                'bg-bg dark:bg-transparent flex rounded-lg transition-all w-full px-4 py-3 shadow-[0_0_5px_1px_#00000050] dark:shadow-[0_0_5px_1px_#ffffff50]'
            }
        >
            {document.files && document.files.length > 0 && (
                <div className="flex w-1/12 justify-center">
                    <FileTypeIcon
                        className={'text-[70px]'}
                        type={document.files[0]?.url}
                    />
                </div>
            )}
            <div
                className={
                    'flex w-5/12 h-full items-center ' +
                    'xl:flex-wrap xl:content-center xl:gap-1 xl:pl-2 xl:w-3/12 lg:w-5/12'
                }
            >
                <span
                    title={document.name}
                    className="w-full dark:text-bg whitespace-nowrap overflow-hidden text-ellipsis text-xl capitalize text-center xl:text-left"
                >
                    {document.name}
                </span>
                <span className="w-full dark:text-bg capitalize text-center xl:text-left">
                    {document.season.toLocaleLowerCase()} {document.year}
                </span>
            </div>
            <div className="flex w-6/12 xl:w-8/12 lg:flex-wrap lg:w-6/12 lg:content-center lg:gap-1">
                <div className="flex gap-2 items-center w-1/3 justify-center lg:w-full lg:justify-start lg:h-fit sm:justify-center">
                    {document.tags?.map((tag: Tag, index: number) => (
                        <span
                            className="text-sm text-gray-600 dark:text-gray-300"
                            key={index}
                        >
                            #{tag.label}
                        </span>
                    ))}
                </div>
                <div
                    className={
                        'flex items-center gap-4 w-2/3 justify-center lg:w-full lg:grid lg:grid-cols-2 lg:gap-1 ' +
                        'sm:flex sm:flex-wrap sm:justify-center'
                    }
                >
                    <div className="flex w-fit gap-1">
                        <span className="dark:text-bg">Solutions?</span>
                        {document.isSolutionsIncluded ? (
                            <AiOutlineCheck className="text-green-500 lg:text-sm" />
                        ) : (
                            <ImCross className="text-red-500 lg:text-sm" />
                        )}
                    </div>
                    <div className="flex w-fit gap-1">
                        <span className="dark:text-bg">Complete?</span>
                        {document.isCompleted ? (
                            <AiOutlineCheck className="text-green-500 lg:text-sm" />
                        ) : (
                            <ImCross className="text-red-500 lg:text-sm" />
                        )}
                    </div>
                    <div className="flex w-fit gap-1">
                        <span className="dark:text-bg">Verified?</span>
                        {document.verified ? (
                            <AiOutlineCheck className="text-green-500 lg:text-sm" />
                        ) : (
                            <ImCross className="text-red-500 lg:text-sm" />
                        )}
                    </div>
                </div>
            </div>
        </Link>
    )
}

export default DocumentCard
