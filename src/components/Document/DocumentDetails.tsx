import React, { FC } from 'react'
import { Button } from '@/components'
import { AiOutlineCloudDownload } from 'react-icons/ai'
import { BsFillTrashFill } from 'react-icons/bs'
import SeasonIcon from '@/types/SeasonIcon'
import { TemporaryFileDocumentType } from '@/types/TemporaryFileDocumentType'
import { Course, Document } from '@prisma/client'
import CircularProgress from '@mui/material/CircularProgress'

type Props = {
    isDeleteButtonVisible: boolean
    downloadFiles: () => void
    handleDeleteDocument: () => void
    loadingDeleteDocument: boolean
    documentModel: Document & { files: TemporaryFileDocumentType[] }
    course: Course
}

const DocumentDetails: FC<Props> = props => {
    const {
        isDeleteButtonVisible,
        downloadFiles,
        handleDeleteDocument,
        loadingDeleteDocument,
        documentModel,
        course,
    } = props
    return (
        <div className="grid grid-cols-2 justify-evenly gap-4">
            {isDeleteButtonVisible && (
                <Button
                    label="Download"
                    icon={
                        <AiOutlineCloudDownload className="text-xl lg:text-lg md:text-xl" />
                    }
                    classname={
                        '  bg-yellow-500 mx-auto w-fit hover:bg-yellow-600 text-white font-bold py-2 px-4' +
                        ' xl:text-sm xl:px-2 xl:py-1 lg:text-xs md:text-base'
                    }
                    onClick={downloadFiles}
                />
            )}
            <div className="flex flex-col items-center gap-1">
                {loadingDeleteDocument ? (
                    <CircularProgress size={25} />
                ) : (
                    <Button
                        label="Delete"
                        icon={
                            <BsFillTrashFill className="text-sm lg:text-xs md:text-xl" />
                        }
                        classname={
                            (!isDeleteButtonVisible && 'hidden ') +
                            '  py-2 px-4 mx-auto border-red-500 hover:bg-red-600 bg-red-500 dark:bg-red-500 dark:border-red-500' +
                            ' xl:text-sm xl:px-2 xl:py-1 lg:text-xs md:text-base'
                        }
                        onClick={handleDeleteDocument}
                    />
                )}
            </div>

            <div className="flex flex-col items-center gap-1">
                <span className="font-bold text-xl xl:text-lg md:text-xl dark:text-bg">
                    Season
                </span>
                <div className="flex items-center gap-1 dark:text-bg">
                    <SeasonIcon
                        className="text-primary "
                        season={documentModel.season}
                    />
                    <span className="capitalize ">
                        {documentModel.season.toLowerCase()}
                    </span>
                    <SeasonIcon
                        className="text-primary "
                        season={documentModel.season}
                    />
                </div>
            </div>
            <div className="flex flex-col items-center gap-1 dark:text-bg">
                <span className="font-bold text-xl xl:text-lg md:text-xl">
                    Year
                </span>
                <span className="capitalize ">{documentModel.year}</span>
            </div>
            <div className="flex flex-col items-center gap-1 dark:text-bg">
                <span className="font-bold text-xl xl:text-lg md:text-xl">
                    Type
                </span>
                <span className="capitalize ">
                    {documentModel.type?.toLowerCase()}
                </span>
            </div>
            <div className="flex flex-col items-center gap-1 dark:text-bg">
                <span className="font-bold text-xl xl:text-lg md:text-xl">
                    Course
                </span>
                <span className="capitalize text-center leading-tight ">
                    {course.name}
                </span>
            </div>
        </div>
    )
}

export default DocumentDetails
