import { Input, SwitchInput, SelectForm } from '@/components'
import { Course, DocumentType, School, Season, Tag } from '@prisma/client'
import { FormikErrors } from 'formik'
import { ChangeEvent, useState } from 'react'
import { bool, boolean, number, object, string } from 'yup'
import { gql, useMutation } from '@apollo/client'
import { useSocket } from '@/hooks/useSocket'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import {
    Option,
    enumToOption,
    enumsToOptions,
    toModels,
    toOptions,
} from '@/types/OptionType'
import { TemporaryDocumentType } from '@/types/TemporaryDocumentType'

interface Props {
    document: TemporaryDocumentType
    course: Course | null
    school: School | null
    tags: Tag[]
    index: number
    setResult: (result: string) => void
    dragging: boolean
    setDragging: (dragging: boolean) => void
    setRedirectTo: (path: string) => void
    fileName: string
    setValues: (
        values: React.SetStateAction<TemporaryDocumentType[]>,
    ) => Promise<FormikErrors<TemporaryDocumentType[]>> | Promise<void>
}

const validationSchema = object().shape({
    name: string().required('Required'),
    season: string().required('Required'),
    type: string().required('Required'),
    year: number()
        .max(new Date().getFullYear() + 10)
        .min(2000)
        .required('Required'),
    isSolutionsIncluded: bool().optional(),
    isCompleted: bool().optional(),
})

const createDocumentMutation = gql`
    mutation createDocumentMutation($input: CreateDocumentInput!) {
        createDocument(input: $input) {
            id
        }
    }
`

const updateDocumentMutation = gql`
    mutation updateDocumentMutation(
        $id: String!
        $input: UpdateDocumentInput!
    ) {
        updateDocument(id: $id, input: $input) {
            id
        }
    }
`

/**
 *
 * @param documentToUpdate
 * @param selectedCourse
 * @param selectedSchool
 * @param tags
 * @param setResult
 * @param setRedirectTo
 * @constructor
 */
const DocumentForm = ({
    document: documentToUpdate,
    tags,
    setResult,
    setRedirectTo,
    course,
    school,
    fileName,
    setValues,
    index,
}: Props) => {
    const { socket } = useSocket()

    const [key, setKey] = useState<string[]>([])
    const [canUpload, setCanUpload] = useState(false)

    const [createDocument] = useMutation(createDocumentMutation, {
        onCompleted: async data => {
            setResult('Document Created')
            await socket.emit('createdMessage', {
                action: 'notification',
            })
            setRedirectTo('/documents/' + data.createdocumentToUpdate?.id)
        },
        onError: error => {
            setResult(error.message)
        },
    })

    const [updateDocument] = useMutation(updateDocumentMutation, {
        onCompleted: async data => {
            setResult('Document Updated')
            // await socket.emit('createdMessage', {
            //     action: 'notification',
            // })
            setRedirectTo('/documents/' + data.updatedocumentToUpdate?.id)
        },
        onError: error => {
            setResult(error.message)
        },
    })
    const onHandleChange = (
        value: string | boolean | Tag[],
        attribute: string,
    ) => {
        setValues(prev =>
            prev.map((doc, i) => {
                if (i === index) {
                    return {
                        ...doc,
                        [attribute]: value,
                    }
                }
                return doc
            }),
        )
    }
    return (
        <div className="flex flex-col gap-4 px-6 sm:w-full h-full py-10">
            <Input
                label="name"
                handleChange={e => onHandleChange(e.target.value, 'name')}
                value={documentToUpdate.name}
                disabled={!school || !course}
            />
            <Input
                label="Year"
                handleChange={e =>
                    onHandleChange(e.target.value.replaceAll(/\D/g, ''), 'year')
                }
                value={documentToUpdate.year.toString()}
                disabled={!school || !course}
            />
            <SelectForm
                disabled={!school || !course}
                label="Tags"
                options={toOptions(tags, 'id', 'label')}
                onChange={items =>
                    onHandleChange(
                        toModels(items as Option[], tags) as Tag[],
                        'tags',
                    )
                }
                selected={toOptions(documentToUpdate?.tags, 'id', 'label')}
                isMulti
            />
            <SelectForm
                disabled={!school || !course}
                label="Seasons"
                options={enumsToOptions(Object(Season))}
                selected={enumToOption(Object(Season), documentToUpdate.season)}
                onChange={items =>
                    onHandleChange((items as Option).label, 'season')
                }
            />
            <SelectForm
                disabled={!school || !course}
                label="Documents Type"
                options={enumsToOptions(Object(DocumentType))}
                selected={enumToOption(
                    Object(DocumentType),
                    documentToUpdate.type,
                )}
                onChange={items =>
                    onHandleChange((items as Option).label, 'type')
                }
            />
            <SwitchInput
                disabled={!school || !course}
                label="Is Solutions Included ?"
                checked={documentToUpdate.isSolutionsIncluded}
                onChange={isSolutionsIncluded =>
                    onHandleChange(isSolutionsIncluded, 'isSolutionsIncluded')
                }
            />
            <SwitchInput
                disabled={!school || !course}
                label="Is Document completed ?"
                checked={documentToUpdate.isCompleted}
                onChange={isCompleted =>
                    onHandleChange(isCompleted, 'isCompleted')
                }
            />
        </div>
    )
}

export default DocumentForm
