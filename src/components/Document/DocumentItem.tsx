import { Document } from '@prisma/client'
import { AiOutlineCheck } from 'react-icons/ai'
import { ImCross } from 'react-icons/im'

type Props = {
    document: Document
}

const DocumentItem = ({ document }: Props) => {
    return (
        <div className="py-6 w-full gap-4 flex items-center">
            <span className="w-2/12 capitalize flex justify-center max-w-[calc(100%/6] overflow-hidden px-2">
                {document.name}
            </span>
            <span className="w-2/12 capitalize flex justify-center max-w-[calc(100%/6] overflow-hidden px-2">
                {document.year}
            </span>
            <span className="w-2/12 capitalize flex justify-center max-w-[calc(100%/6] overflow-hidden px-2">
                {document.season.toLocaleLowerCase()}
            </span>
            <span className="w-2/12 capitalize flex justify-center max-w-[calc(100%/6] overflow-hidden px-2">
                {document.isCompleted ? <AiOutlineCheck /> : <ImCross />}
            </span>
            <span className="w-2/12 capitalize flex justify-center max-w-[calc(100%/6] overflow-hidden px-2">
                {document.isSolutionsIncluded ? (
                    <AiOutlineCheck />
                ) : (
                    <ImCross />
                )}
            </span>
            <span className="w-2/12 capitalize flex justify-center max-w-[calc(100%/6] overflow-hidden px-2">
                {document.verified ? <AiOutlineCheck /> : <ImCross />}
            </span>
        </div>
    )
}

export default DocumentItem
