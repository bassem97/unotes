import DocViewer, {
    DocViewerRenderers,
    IDocument,
} from '@cyntler/react-doc-viewer'

type params = {
    files: IDocument[]
}

/**
 *
 * @param files
 * @constructor
 */
const PDFViewer = ({ files }: params) => {
    return (
        <DocViewer
            documents={files}
            pluginRenderers={DocViewerRenderers}
            className="dark:react-doc-viewer"
        />
    )
}

export default PDFViewer
