import { ReactNode, useRef, useState } from 'react'
import { useClickOutside } from '@/hooks/useClickOutside'

type Props = {
    head: ReactNode
    children: ReactNode
    position: string
}

const DropdownContainer = ({ children, head, position }: Props) => {
    const [isOpen, setIsOpen] = useState<boolean>(false)
    const wrapperRef = useRef(null)
    useClickOutside(wrapperRef, () => setIsOpen(() => false))
    return (
        <div className="relative select-none" ref={wrapperRef}>
            <span
                className="cursor-pointer"
                onClick={() => setIsOpen(prev => !prev)}
            >
                {head}
            </span>
            {isOpen && (
                <div
                    className={
                        'absolute z-40 rounded-3xl bg-bg dark:bg-darkfont bg-opacity-100 custom-shadow ' +
                        position
                    }
                >
                    {children}
                </div>
            )}
        </div>
    )
}

export default DropdownContainer
