import { Button, Search } from '@/components/index'
import { router } from 'next/client'

interface FilterProps {
    buttonLabel: string
    itemsFilter: string
    setItemsFilter: (value: string) => void
    handleClick: () => void
}

const Filter = ({
    buttonLabel,
    itemsFilter,
    setItemsFilter,
    handleClick,
}: FilterProps) => {
    return (
        <div className="w-full flex gap-4 items-center mt-4 justify-center md:px-2">
            <div className="w-2/3">
                <Search
                    filter={itemsFilter}
                    onChange={value => setItemsFilter(value)}
                    placeholder="Filter..."
                />
            </div>
            <div>
                <Button label={`Add ${buttonLabel}`} onClick={handleClick} />
            </div>
        </div>
    )
}

export default Filter
