import { FC } from 'react'
import { capitalizeFirst } from '@/lib/capitalizeFirst'
import Head from 'next/head'

type Props = {
    description: string
}

const Header: FC<Props> = ({ description }) => {
    const title = `uNotes | ${capitalizeFirst(description)}`
    return (
        <Head>
            <title>{title}</title>
            <meta
                property="og:title"
                name="description"
                content={title}
                key="title"
            />
        </Head>
    )
}

export default Header
