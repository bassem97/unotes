import { useClickOutside } from '@/hooks/useClickOutside'
import { ChangeEvent } from 'react'

type Props = {
    label: string
    value?: string
    handleChange?: (e: ChangeEvent<HTMLInputElement>) => void
    disabled?: boolean
}

const Input = ({ label, value, handleChange, disabled = false }: Props) => {
    return (
        <div className="flex flex-wrap gap-2 w-full h-fit relative">
            <input
                className="w-full h-fit placeholder:capitalize dark:bg-transparent dark:text-bg px-[8px] border-solid py-[2px] border min-h-[38px] border-[hsl(0,0%,80%)] rounded-[4px]"
                name={label}
                id={label}
                type="text"
                value={value}
                onChange={handleChange}
                placeholder={label + '...'}
                disabled={disabled}
            />
            <label
                htmlFor={label}
                className="px-1 capitalize text-xs dark:text-bg font-semibold text-gray-500 absolute -top-2 left-2 bg-bg dark:bg-darkbg"
            >
                {label}
            </label>
        </div>
    )
}

export default Input
