import { Dispatch, SetStateAction } from 'react'
import { gql, useMutation } from '@apollo/client'
import Link from 'next/link'
import { IoNotificationsOutline } from 'react-icons/io5'
import { Notification } from '@prisma/client'
import { ImCross } from 'react-icons/im'

type Props = {
    notification: Notification
    setNotifications: Dispatch<SetStateAction<Notification[]>>
    setOpen: Dispatch<SetStateAction<boolean>>
}

const setSeenNotification = gql`
    mutation setSeenNotification($id: String!) {
        setSeenNotification(id: $id) {
            id
            seen
        }
    }
`
const DELETE_NOTIFICATION = gql`
    mutation DeleteNotification($id: String!) {
        deleteNotification(id: $id) {
            id
        }
    }
`
const NotificationItem = ({
    notification,
    setNotifications,
    setOpen,
}: Props) => {
    const [setSeen] = useMutation(setSeenNotification, {
        onCompleted: data => {
            setNotifications(prev => {
                return prev.map(notif => {
                    if (notif.id === notification.id) {
                        return {
                            ...notif,
                            seen: true,
                        }
                    }
                    return notif
                })
            })
        },
        onError: error => {
            console.log('Error:', error)
        },
    })
    const [deleteNotif] = useMutation(DELETE_NOTIFICATION, {
        onCompleted: () => {
            const deletedNotificationId = notification.id
            setNotifications(prev =>
                prev.filter(notif => notif.id !== deletedNotificationId),
            )
        },
        onError: error => {
            console.log('Error deleting notification:', error)
        },
    })
    const handleDelete = async (notification: Notification) => {
        try {
            await deleteNotif({
                variables: {
                    id: notification.id,
                },
            })
        } catch (error) {
            console.log('Error deleting notification:', error)
        }
    }
    const handleSeen = (notification: Notification) => {
        if (!notification.seen) {
            setSeen({
                variables: {
                    id: notification.id,
                },
            })
        }
    }

    return (
        <Link href={notification.link || ''}>
            <div
                className={
                    'mx-2 border-2 rounded-3xl p-2 md:p-4 bg-sec bg-opacity-[0.01] dark:bg-darkbg ' +
                    (notification.seen
                        ? ' '
                        : 'border-primary notification-item-shadow ') +
                    (notification.link ? 'cursor-pointer' : 'cursor-default')
                }
                onClick={() => setOpen(false)}
            >
                <div className="border-2 rounded-2xl p-4 md:p-4 flex gap-4 items-center dark:bg-darkbg">
                    <div
                        className={
                            'aspect-square rounded-lg bg-opacity-10 p-1 h-fit dark:text-white' +
                            (notification.seen
                                ? 'bg-sec'
                                : 'notification-item-shadow bg-primary')
                        }
                    >
                        <IoNotificationsOutline
                            className={
                                'text-4xl ' +
                                (notification.seen ? '' : 'text-primary')
                            }
                        />
                    </div>
                    <div className="flex flex-wrap gap-2 w-full dark:bg-darkbg">
                        <span className="text-darkfont dark:text-white w-full text-xs leading-none font-bold h-fit opacity-80 rounded-3xl">
                            {notification.link
                                ? 'Click here to navigate'
                                : 'Announcement'}
                        </span>
                        <p className="text-darkfont dark:text-white text-base md:text-lg leading-none h-fit rounded-3xl">
                            {notification.body}
                        </p>
                    </div>
                    <ImCross
                        onClick={() => handleDelete(notification)}
                        className={
                            'cursor-pointer text-xl text-primary dark:text-primary '
                        }
                    />
                </div>
            </div>
        </Link>
    )
}

export default NotificationItem
