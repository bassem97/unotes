import { IoNotificationsOutline, IoNotifications } from 'react-icons/io5'
import { BsFillTrashFill } from 'react-icons/bs'
import { useEffect, useState } from 'react'
import { useSession } from 'next-auth/react'
import { gql, NetworkStatus, useMutation } from '@apollo/client'
import { Session } from 'next-auth'
import { useSocket } from '@/hooks/useSocket'
import useSound from 'use-sound'
import { DropdownContainer, NotificationItem } from '@/components'
import { useWindowSize } from '@/hooks/useWindowSize'
import { ImCross } from 'react-icons/im'
import { useInfiniteScroll } from '@/hooks/useInfiniteScroll'
import { Notification } from '@prisma/client'
import BaseSpinner from '@/components/BaseSpinner'

const GET_NOTIFICATIONS = gql`
    query notificationsByUser($userId: String!, $limit: Int, $skip: Int) {
        notificationsByUser(userId: $userId, limit: $limit, skip: $skip) {
            id
            body
            seen
            link
            createdAt
        }
    }
`

const DELETE_NOTIFICATION = gql`
    mutation deleteNotificationsByUser($userId: String!) {
        deleteNotificationsByUser(userId: $userId)
    }
`

const Notifications = () => {
    /**
     * destructuring the session object to get the user id
     */
    const { data: { user: { id } = {} } = {} } = useSession() as unknown as {
        data: Session
    }
    const [open, setOpen] = useState<boolean>(false)
    const { message } = useSocket()
    const [play] = useSound('/sounds/notification-sound.mp3')

    /**
     * TODO: refactor initial state of the notifications
     */
    const [notifications, setNotifications] = useState<Array<Notification>>([])

    /**
     * Trigered when a new notification is received
     */
    useEffect(() => {
        if (message.action === 'notification') {
            fetchMore({
                variables: {
                    userId: id,
                },
            }).then(data => {
                if (
                    data.data.notificationsByUser.length > notifications.length
                ) {
                    play()
                }
            })
        }
    }, [message])

    const { ref, networkStatus, error, fetchMore } =
        useInfiniteScroll<Notification>({
            query: GET_NOTIFICATIONS,
            items: notifications,
            setItems: setNotifications,
            queryVariables: {
                userId: id,
            },
            skip: 0,
            limit: 6,
        })

    /**
     * Delete all users notifications
     */
    const [deleteNotifications] = useMutation(DELETE_NOTIFICATION, {
        variables: {
            userId: id,
        },
        onCompleted: () => {
            setNotifications([])
        },
    })
    const width = useWindowSize()
    const isMobile = width && width < 768

    if (networkStatus === NetworkStatus.loading) {
        return <BaseSpinner />
    }
    if (error) {
        console.log(error)
    }

    return isMobile ? (
        <div>
            <span
                className="cursor-pointer"
                onClick={() => setOpen(prev => !prev)}
            >
                <div className="relative cursor-pointer">
                    {notifications.filter(notif => !notif.seen).length > 0 && (
                        <div className="absolute -top-1 -right-1 text-xs text-bg rounded-full dark:bg-primary bg-sec aspect-square h-4 text-center">
                            {notifications.filter(notif => !notif.seen).length}
                        </div>
                    )}
                    {notifications.filter(notif => !notif.seen).length > 0 ? (
                        <IoNotifications className="text-3xl dark:text-bg2" />
                    ) : (
                        <IoNotificationsOutline className="text-3xl dark:text-bg2" />
                    )}
                </div>
            </span>
            {open && (
                <div className="pt-4 pb-4 fixed z-50 left-0 top-0 w-full bg-bg h-full">
                    <div className="flex items-center justify-between mb-4 mx-4">
                        <h1 className="text-lg font-bold dark:text-bg2 md:text-3xl">
                            Notifications
                        </h1>
                        <div className="flex items-center gap-4">
                            <BsFillTrashFill
                                onClick={() => deleteNotifications()}
                                className={
                                    'cursor-pointer text-xl text-sec dark:text-white ' +
                                    (notifications.length === 0 && 'hidden')
                                }
                            />
                            <ImCross
                                onClick={prev => setOpen(!prev)}
                                className="text-xl text-red-500 cursor-pointer"
                            />
                        </div>
                    </div>
                    <div className="flex flex-col w-full gap-4">
                        {notifications.length > 0 ? (
                            <div>
                                {notifications
                                    .slice(0, 2)
                                    .map((notification, key) => (
                                        <NotificationItem
                                            key={key}
                                            setNotifications={setNotifications}
                                            notification={notification}
                                            setOpen={setOpen}
                                        />
                                    ))}
                            </div>
                        ) : (
                            <div className="flex justify-center items-center h-24 ">
                                <p className="text-sec dark:text-white text-lg font-bold">
                                    No notifications
                                </p>
                            </div>
                        )}
                        <div ref={ref} />
                    </div>
                </div>
            )}
        </div>
    ) : (
        <DropdownContainer
            position="top-[calc(100%+1rem)] lg:right-[-10rem] right-0 w-[25rem] z-50  overflow-y-auto scroller "
            head={
                <div className="relative cursor-pointer ">
                    {notifications.filter(notif => !notif.seen).length > 0 && (
                        <div className="absolute -top-1 -right-1 text-xs  text-bg rounded-full dark:bg-primary bg-primary aspect-square h-4 text-center">
                            {notifications.filter(notif => !notif.seen).length}
                        </div>
                    )}
                    {notifications.filter(notif => !notif.seen).length > 0 ? (
                        <IoNotifications className="text-3xl dark:text-bg2" />
                    ) : (
                        <IoNotificationsOutline className="text-3xl dark:text-bg2" />
                    )}
                </div>
            }
        >
            <div className="pt-4  rounded-3xl bg-bg dark:bg-darkbg ">
                <div className="flex items-center justify-between mb-4 mx-4 ">
                    <h1 className="text-lg font-bold dark:text-bg2">
                        Notifications
                    </h1>
                    <BsFillTrashFill
                        onClick={() => deleteNotifications()}
                        className={
                            'cursor-pointer text-xl text-primary dark:text-primary ' +
                            (notifications.length === 0 && 'hidden')
                        }
                    />
                </div>
                <div className="flex flex-col w-full gap-4 max-h-[calc(100vh-4rem)] ">
                    <div
                        style={{ maxHeight: '20rem', overflowY: 'auto' }}
                        className="overflow-y-auto scroller"
                    >
                        {notifications.length > 0 ? (
                            <div>
                                {notifications.map((notification, key) => (
                                    <NotificationItem
                                        key={key}
                                        setNotifications={setNotifications}
                                        notification={notification}
                                        setOpen={setOpen}
                                    />
                                ))}
                            </div>
                        ) : (
                            <div className="flex justify-center items-center h-24">
                                <p className="text-sec dark:text-primary text-lg font-bold">
                                    No notifications
                                </p>
                            </div>
                        )}
                    </div>
                    <div ref={ref} />
                </div>
            </div>
        </DropdownContainer>
    )
}

export default Notifications
