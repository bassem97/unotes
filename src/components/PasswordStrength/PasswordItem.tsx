import { ItemChecker } from './PasswordChecker'
interface PasswordItemProps {
    itemChecker: ItemChecker
    key?: number
}

const PasswordItem = ({ itemChecker }: PasswordItemProps) => {
    return (
        <>
            <div
                className={`rounded-full p-1 fill-current ${
                    itemChecker.term
                        ? 'bg-green-200 text-green-700'
                        : 'bg-red-200 text-red-700'
                } `}
            >
                <svg
                    className="w-4 h-4"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                >
                    <path
                        visibility={itemChecker.term ? 'visible' : 'hidden'}
                        d="M5 13l4 4L19 7"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                    />
                    <path
                        visibility={!itemChecker.term ? 'visible' : 'hidden'}
                        d="M6 18L18 6M6 6l12 12"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="2"
                    />
                </svg>
            </div>
            <span
                className={`font-medium text-sm ml-3 ${
                    itemChecker.term ? 'text-green-700' : 'text-red-700'
                } `}
            >
                {itemChecker.term
                    ? itemChecker.success_message
                    : itemChecker.failure_message}
            </span>
        </>
    )
}
export default PasswordItem
