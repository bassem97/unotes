import { ChangeEvent } from 'react'

type Props = {
    list: string[]
    onChange: (e: ChangeEvent<HTMLInputElement>) => void
    checked: string
    label: string
}

const RadioInput = ({ list, onChange, checked, label }: Props) => {
    return (
        <div className="flex gap-2 mt-4 flex-wrap">
            <label
                className={
                    'text-xl min-h-[1.125rem] w-full text-font2 dark:text-bg xs:text-sm  leading-none '
                }
            >
                {label}
            </label>
            {list.map((item, key) => (
                <div key={key} className="flex gap-1 flex-col">
                    <input
                        type="checkbox"
                        id={`${label} ${item}`}
                        aria-label={label}
                        name={item}
                        value={item}
                        checked={checked === item}
                        onChange={e => onChange(e)}
                    />
                    <label
                        className="text-sm font-thin capitalize"
                        htmlFor={item}
                    >
                        {item.toLowerCase()}
                    </label>
                </div>
            ))}
        </div>
    )
}

export default RadioInput
