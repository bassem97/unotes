import { School } from '@prisma/client'
import Image from 'next/image'
import Link from 'next/link'
import { useState } from 'react'
import { gql, useMutation } from '@apollo/client'
import { AiOutlineStar, AiFillStar } from 'react-icons/ai'
import CircularProgress from '@mui/material/CircularProgress'

const addSchoolToFavoritesMutation = gql`
    mutation addSchoolToFavoritesMutation(
        $userId: String!
        $schoolId: String!
    ) {
        addSchoolToFavorites(userId: $userId, schoolId: $schoolId) {
            id
            name
        }
    }
`

const removeSchoolToFavoritesMutation = gql`
    mutation removeSchoolToFavoritesMutation(
        $userId: String!
        $schoolId: String!
    ) {
        removeSchoolFromFavorites(userId: $userId, schoolId: $schoolId) {
            id
            name
        }
    }
`

interface Props {
    school: School
    userId?: string
    isFavorite?: boolean
}

const SchoolCard = ({ school, userId, isFavorite }: Props) => {
    const [isFavoriteSchool, setIsFavoriteSchool] = useState(isFavorite)
    const [addSchoolToFavorites, { loading: addSchoolLoading }] = useMutation(
        addSchoolToFavoritesMutation,
        {
            onCompleted: () => {
                setIsFavoriteSchool(true)
            },
        },
    )

    const [removeSchoolToFavorites, { loading: removeSchoolLoading }] =
        useMutation(removeSchoolToFavoritesMutation, {
            onCompleted: () => {
                setIsFavoriteSchool(false)
            },
        })

    const handleAddSchoolToFavorites = async (add: boolean) => {
        try {
            if (add) {
                await addSchoolToFavorites({
                    variables: { userId, schoolId: school.id },
                })
            } else {
                await removeSchoolToFavorites({
                    variables: { userId, schoolId: school.id },
                })
            }
        } catch (error) {
            console.error('Error adding/removing from favorites:', error)
        }
    }

    return (
        <div className="relative hover:scale-105 h-fit">
            {userId && (
                <>
                    {isFavoriteSchool ? (
                        <div
                            onClick={() => handleAddSchoolToFavorites(false)}
                            className="absolute top-0 right-0 z-20 py-2 px-4 text-yellow-300"
                        >
                            {removeSchoolLoading ? (
                                <CircularProgress size={25} />
                            ) : (
                                <AiFillStar
                                    className="cursor-pointer text-2xl"
                                    title="Remove from favorites"
                                />
                            )}
                        </div>
                    ) : (
                        <div
                            onClick={() => handleAddSchoolToFavorites(true)}
                            className="absolute top-0 right-0 z-20 py-2 px-4 text-yellow-300"
                        >
                            {addSchoolLoading ? (
                                <CircularProgress size={25} />
                            ) : (
                                <AiOutlineStar
                                    title="Add to favorites"
                                    className="cursor-pointer text-2xl"
                                />
                            )}
                        </div>
                    )}
                </>
            )}
            <Link
                href={'/schools/' + school.id.toString()}
                className={
                    'bg-bg school-card relative rounded-3xl flex transition-all w-full aspect-square max-h-fit ' +
                    'cursor-pointer shadow-[0_0_5px_1px_#00000050] '
                }
            >
                <Image
                    src={
                        school.image || '/photo-1497091071254-cc9b2ba7c48a.avif'
                    }
                    alt="placeholder"
                    fill
                    className="w-full rounded-3xl object-cover"
                    sizes={
                        '(min-width: 1024px) 33vw, (min-width: 768px) 50vw, 100vw'
                    }
                    priority={true}
                />
                <div className="flex rounded-3xl flex-col items-center justify-end gap-1 w-full h-full absolute top-0 left-0 bg-black bg-opacity-40">
                    <h1
                        className={
                            'text-lg pb-8 w-full overflow-hidden text-ellipsis leading-5 font-thin text-bg uppercase text-center whitespace-nowrap' +
                            '  2xl:text-sm xl:text-xs md:text-xl sm:text-base xs:text-xl'
                        }
                    >
                        {school.name}
                    </h1>
                </div>
            </Link>
        </div>
    )
}

export default SchoolCard
