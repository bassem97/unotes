import { useFormik } from 'formik'
import { Button, Input } from '@/components'
import BaseSpinner from '@/components/BaseSpinner'
import * as Yup from 'yup'
import { gql, useMutation } from '@apollo/client'
import { useSocket } from '@/hooks/useSocket'
import { School } from '@prisma/client'
import { useSession } from 'next-auth/react'
import { Session } from 'next-auth'
import { useEffect, useState } from 'react'
import { UploadAdapter, UpupUploader } from '@bassem97/upup'
import useUpup from '@/hooks/useUpup'
import CircularProgress from '@mui/material/CircularProgress'

const validationSchema = Yup.object().shape({
    name: Yup.string().min(4).required('Required'),
})

const createSchoolMutation = gql`
    mutation createSchoolMutation($input: CreateSchoolInput!) {
        createSchool(input: $input) {
            id
            name
        }
    }
`

const updateSchoolMutation = gql`
    mutation updateSchoolMutation($input: UpdateSchoolInput!) {
        updateSchool(input: $input) {
            id
            name
        }
    }
`

const images_endpoint = process.env.NEXT_PUBLIC_IMAGE_ENDPOINT + '/'

type Props = {
    setResult: (result: string) => void
    school?: School
    setRedirectTo: (path: string) => void
}

const SchoolForm = ({ setResult, school, setRedirectTo }: Props) => {
    const [imageURL, setImageURL] = useState<String | null | undefined>(
        school?.image,
    )
    const { data: userData } = useSession() as { data: Session }
    const { socket } = useSocket()

    /**
     * UpupUploader Configs
     */
    const { baseConfigs, cloudStorageConfigs, setCanUpload, keys } = useUpup()

    useEffect(() => {
        if (keys.length == 1) {
            setImageURL(images_endpoint + keys[0])
            handleSubmit()
        }
    }, [keys])

    const [createSchool] = useMutation(createSchoolMutation, {
        onCompleted: async data => {
            setResult('School Created')
            setRedirectTo('/schools/' + data.createSchool.id)
        },
        onError: error => {
            setResult(error.message)
        },
    })

    const [updateSchool] = useMutation(updateSchoolMutation, {
        onCompleted: async () => {
            setResult('School Updated')
            await socket.emit('createdMessage', {
                action: 'notification',
            })
            setRedirectTo('/schools/' + school!.id)
        },
        onError: error => {
            setResult(error.message)
        },
    })

    const {
        handleSubmit,
        errors,
        touched,
        handleChange,
        handleBlur,
        setFieldValue,
        setErrors,
        isSubmitting,
        values,
    } = useFormik({
        initialValues: {
            name: school?.name || '',
            image: school?.image || null,
            verified: school?.verified || (true as Boolean),
        },
        validationSchema,
        onSubmit: async ({ name, verified }) => {
            // trigger the upload process
            setCanUpload(true)
            if (school)
                await updateSchool({
                    variables: {
                        input: {
                            id: school!.id,
                            verified,
                            name,
                            image: imageURL,
                        },
                    },
                })
            else
                await createSchool({
                    variables: {
                        input: {
                            name,
                            verified,
                            userId: userData?.user?.id,
                            image: imageURL,
                        },
                    },
                })
        },
    })

    return (
        <form onSubmit={handleSubmit}>
            <div className="flex flex-col gap-1 bg-transparent sm:w-screen scroller overflow-hidden overflow-x-auto overflow-y-hidden">
                <div
                    className={`rounded-3xl flex px-4 py-2 items-center
                      ${errors.name! && touched.name && 'border-red-500'} `}
                >
                    <Input
                        label="name"
                        handleChange={e =>
                            setFieldValue('name', e.target.value)
                        }
                        value={values.name}
                    />
                </div>
                {errors.name && touched.name && (
                    <div className="text-red-500 text-xs italic">
                        {errors.name?.toString()}
                    </div>
                )}

                <div
                    className={`rounded-3xl flex px-4 py-2 items-center
                      ${errors.name! && touched.name && 'border-red-500'} `}
                >
                    <div className="w-full sm:w-screen">
                        <UpupUploader
                            baseConfigs={baseConfigs}
                            cloudStorageConfigs={cloudStorageConfigs}
                            uploadAdapters={[UploadAdapter.INTERNAL]}
                        />
                    </div>
                </div>
            </div>

            {isSubmitting ? (
                <div className="flex justify-center">
                    <CircularProgress size={40} />
                </div>
            ) : (
                <div className="flex justify-center">
                    <Button
                        classname="mt-4 py-3 px-5 text-lg mx-auto"
                        disabled={isSubmitting}
                        label=""
                    >
                        <button type="submit" className="w-full h-full">
                            {school ? 'Update' : 'Create'} school
                        </button>
                    </Button>
                </div>
            )}
        </form>
    )
}

export default SchoolForm
