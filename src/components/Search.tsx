import { useRef, useState } from 'react'
import { FaSearch } from 'react-icons/fa'
import { useClickOutside } from '@/hooks/useClickOutside'

interface Props {
    list?: string[]
    placeholder: string
    onChange?: (value: string) => void
    onSearch?: (value: string) => void
    filter?: string
}

const Search = ({
    list = [],
    placeholder,
    onChange,
    filter,
    onSearch,
}: Props) => {
    const [search, setSearch] = useState({
        isOpen: false,
        value: '',
        focused: false,
    })
    const wrapperRef = useRef(null)
    useClickOutside(wrapperRef, () =>
        setSearch(prev => ({ ...prev, isOpen: false })),
    )
    return (
        <form
            onClick={() =>
                setSearch(prev => ({ ...prev, isOpen: !prev.isOpen }))
            }
            ref={wrapperRef}
            className={
                'w-full ml-1 rounded-xl relative overflow-hidden h-8 text-sm bg-bg2 bg-opacity-10 flex items-center outline xs:text-xs  ' +
                (search.focused
                    ? 'outline-primary dark:outline-primary outline-2'
                    : 'outline-font3 outline-1')
            }
            onSubmit={e => {
                e.preventDefault()
                if (onSearch && search.value) {
                    onSearch(search.value)
                }
            }}
        >
            <input
                type="text"
                className="w-full bg-transparent placeholder:text-font2 outline-none px-4 dark:text-bg dark:placeholder:text-bg2"
                placeholder={placeholder}
                value={filter ? filter : search.value}
                onFocus={() => setSearch(prev => ({ ...prev, focused: true }))}
                onBlur={() => setSearch(prev => ({ ...prev, focused: false }))}
                onChange={
                    onChange
                        ? e => onChange(e.target.value.toLowerCase())
                        : e =>
                              setSearch(prev => ({
                                  ...prev,
                                  value: e.target.value.toLowerCase(),
                              }))
                }
            />
            <div
                className="flex justify-center items-center bg-primary dark:bg-primarysec h-full px-4 cursor-pointer"
                onClick={() => {
                    if (onSearch && search.value) {
                        onSearch(search.value)
                    }
                }}
            >
                <FaSearch className="text-base text-bg " />
            </div>
            {search.isOpen && list && (
                <div className="absolute top-full left-0 w-full  flex flex-col rounded-md backdrop-blur-sm bg-[#00000070] z-10">
                    {list
                        .filter(item =>
                            item.toLowerCase().includes(search.value),
                        )
                        .map((item, index) => (
                            <div
                                key={index}
                                className="cursor-pointer px-5 py-2 rounded-md hover:bg-primary text-bg hover:bg-opacity-20"
                                onClick={() =>
                                    setSearch(prev => ({
                                        ...prev,
                                        value: item,
                                        isOpen: false,
                                    }))
                                }
                            >
                                {item}
                            </div>
                        ))}
                </div>
            )}
        </form>
    )
}

export default Search
