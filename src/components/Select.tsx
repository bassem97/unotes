import { useRef, useState } from 'react'
import { useClickOutside } from '@/hooks/useClickOutside'
import { Course, Document, School } from '@prisma/client'

interface Props {
    list: Array<Course | School | Document>
    handleChange: (value: Course | School | Document) => void
    selected: Course | School | Document | null
    disabled?: boolean
}

const Select = ({ list, handleChange, selected, disabled = false }: Props) => {
    const [isOpen, setIsOpen] = useState<boolean>(false)
    const wrapperRef = useRef(null)
    useClickOutside(wrapperRef, () => setIsOpen(() => false))

    return (
        <div
            className={
                'select capitalize select-none py-3 pl-3 pr-10  text-font3 font-poppins  bg-transparent relative w-full rounded-2xl border-2  border-bg2 font-semibold ' +
                (disabled ? 'cursor-not-allowed' : 'cursor-pointer') +
                ' xs:text-sm xs:py-2'
            }
            onClick={() => setIsOpen(!isOpen)}
            ref={wrapperRef}
        >
            {selected ? selected.name : '-- Please Select --'}
            {isOpen && !disabled && (
                <div className="max-h-40 tall:max-h-24 rounded-lg scroller overflow-y-auto flex flex-col w-full absolute top-[100%] left-0 bg-[#00000070] backdrop-blur-sm z-10 ">
                    {list?.map((item, key) => (
                        <div
                            className="min-h-[2.5rem] capitalize py-2 pl-3 text-base pr-1 bg-transparent text-bg font-thin hover:bg-sec hover:bg-opacity-20 cursor-pointer
               lg:text-lg xs:text-sm xs:py-1 xs:min-h-[2rem] whitespace-nowrap text-ellipsis max-w-full overflow-hidden"
                            key={key}
                            title={item.name}
                            onClick={() => handleChange(item)}
                        >
                            {item.name}
                        </div>
                    ))}
                </div>
            )}
        </div>
    )
}

export default Select
