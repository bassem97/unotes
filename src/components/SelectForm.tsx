import { Option } from '@/types/OptionType'
import { ReactNode, useState } from 'react'
import Select, { ActionMeta, MultiValue, SingleValue } from 'react-select'

interface Props {
    options: Option[]
    defaultValue?: Option
    selected?: Option | Option[] | null
    disabled?: boolean
    isMulti?: boolean
    label: string
    noOptions?: (inputValue: string) => ReactNode
    onChange:
        | ((
              newValue: MultiValue<Option> | SingleValue<Option>,
              actionMeta: ActionMeta<Option>,
          ) => void)
        | undefined
}

const SelectForm = ({
    options,
    disabled = false,
    label,
    onChange,
    defaultValue,
    isMulti,
    selected = null,
    noOptions,
}: Props) => {
    const [isOpen, setIsOpen] = useState(false)
    return (
        <div
            className={
                'flex flex-wrap gap-2 w-full relative ' + (isOpen ? 'z-10' : '')
            }
        >
            <Select
                isDisabled={disabled}
                className="w-full custom-react-select"
                options={options}
                onChange={onChange}
                defaultValue={defaultValue}
                isMulti={isMulti}
                onMenuOpen={() => setIsOpen(true)}
                onMenuClose={() => setIsOpen(false)}
                name={label}
                id={label}
                noOptionsMessage={
                    noOptions && (obj => noOptions(obj.inputValue))
                }
                value={selected}
                isOptionSelected={option => {
                    return selected
                        ? Array.isArray(selected)
                            ? selected.some(s => option.value === s.value)
                            : option.value === selected.value
                        : false
                }}
            />
            <label
                htmlFor={label}
                className={
                    'px-1 text-xs dark:text-bg capitalize dark:bg-darkbg font-semibold text-gray-500 absolute -top-2 left-2 ' +
                    (disabled ? 'hsl(0,0%,95%)' : 'bg-bg')
                }
            >
                {label}
            </label>
        </div>
    )
}

export default SelectForm
