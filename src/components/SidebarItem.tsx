import Link from 'next/link'
import { useRouter } from 'next/router'
import { MouseEvent } from 'react'

interface Props {
    route: string
    icon: JSX.Element
    label: string
    handleClick?: (e: MouseEvent<HTMLAnchorElement>) => void
    classname?: string
    isSideBarOpen: boolean
}

const SidebarItem = ({
    route,
    icon,
    label,
    handleClick = () => '',
    classname = '',
    isSideBarOpen,
}: Props) => {
    const { pathname } = useRouter()
    return (
        <div className="relative" title={label}>
            <Link
                onClick={handleClick ?? undefined}
                href={route}
                className={
                    'flex gap-2 p-2 rounded-2xl md:w-40 md:justify-center items-center hover:text-bg hover:bg-primary dark:hover:bg-primarysec ' +
                    'hover:text-white animation relative transition-all ' +
                    (isSideBarOpen ? ' ' : 'justify-center ') +
                    (pathname === route
                        ? 'bg-primary text-bg dark:bg-primarysec '
                        : 'text-[#9f9c9c] dark:text-bg') +
                    ' ' +
                    classname
                }
            >
                {icon}
                <span
                    className={
                        'font-bold tracking-wide font-poppins text-sm md:pr-5 ' +
                        (isSideBarOpen ? '' : 'hidden')
                    }
                >
                    {label}
                </span>
            </Link>
        </div>
    )
}

export default SidebarItem
