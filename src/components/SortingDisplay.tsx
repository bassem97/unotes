import React from 'react'
import Search from './Search'

type filter = {
    value: string
    type: string
}
type Props = {
    options: string[]
    list?: string[]
    filter: filter
    setFilter: (filter: filter) => void
    optionPerRow?: number
}

const SortingDisplay = ({
    filter,
    options,
    setFilter,
    optionPerRow = 4,
    list,
}: Props) => {
    options = ['all', ...options]
    const displayOptions = options.reduce((acc: string[][], option, index) => {
        const opt = option.replace('_', ' ')
        if (index % optionPerRow === 0) acc.push([opt])
        else acc[acc.length - 1].push(opt)
        return acc
    }, [])

    return (
        <>
            {displayOptions.map((opts, key) => (
                <div
                    key={key}
                    style={{
                        width: opts.length * (100 / optionPerRow) + '%',
                        gridTemplateColumns:
                            'repeat(' + opts.length + ', minmax(0, 1fr))',
                    }}
                    className={
                        'grid gap-8 border-b-2 border-[#d3d4d5] justify-between sm:gap-2'
                    }
                >
                    {opts.map((option, index) => (
                        <h1
                            key={index}
                            onClick={() =>
                                setFilter({ ...filter, type: option })
                            }
                            title={option}
                            className={
                                'border-b-2 whitespace-nowrap overflow-hidden text-ellipsis text-center py-4 font-bold cursor-pointer text-xl justify-center uppercase sm:text-sm sm:py-2 ' +
                                (filter.type === option
                                    ? 'text-primary  mb-[-2px] border-primary '
                                    : 'text-font2 dark:text-bg border-transparent')
                            }
                        >
                            {option}
                            {list && (
                                <>
                                    {' ('}
                                    {option === 'all'
                                        ? list?.length
                                        : list?.filter(
                                              item =>
                                                  item.replace('_', ' ') ===
                                                  option,
                                          ).length}
                                    {')'}
                                </>
                            )}
                        </h1>
                    ))}
                </div>
            ))}
            <div className="w-full flex gap-4 items-center mt-6 justify-center md:px-2 sm:mt-2">
                <div className="w-2/3">
                    <Search
                        filter={filter.value}
                        onChange={value => setFilter({ ...filter, value })}
                        placeholder="Filter..."
                    />
                </div>
            </div>
        </>
    )
}

export default SortingDisplay
