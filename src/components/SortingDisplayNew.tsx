import React, { Dispatch, SetStateAction, useMemo } from 'react'
import Search from './Search'

type Props = {
    options: Option[]
    filter: string
    query: string
    setFilter: Dispatch<SetStateAction<string>>
    setQuery: Dispatch<SetStateAction<string>>
}

type Option = {
    type: string
    count: number
}

const SortingDisplay = ({
    filter,
    setFilter,
    options,
    setQuery,
    query,
}: Props) => {
    const opts = useMemo(() => {
        const totalCount = options.reduce((prev, el) => el.count + prev, 0)
        const newOpts: Option = {
            type: 'all',
            count: totalCount,
        }
        return [newOpts, ...options]
    }, [options])

    return (
        <>
            <div className="w-full flex flex-wrap justify-around border-b-2 border-[#d3d4d5]">
                {opts.map((option, index) => (
                    <h2
                        key={index}
                        onClick={() =>
                            option.type === 'all'
                                ? setFilter('')
                                : setFilter(option.type)
                        }
                        title={option.type}
                        className={
                            'border-b-2 whitespace-nowrap overflow-hidden text-ellipsis text-center py-4 font-bold cursor-pointer text-lg justify-center capitalize ' +
                            'sm:text-sm sm:py-2 flex-shrink-0 sm:w-1/2 md:w-1/3 lg:w-1/8 ' +
                            (filter === option.type || (!filter && index === 0)
                                ? 'text-primary  mb-[-2px] border-primary '
                                : 'text-font2 dark:text-bg border-transparent ')
                        }
                    >
                        {option.type.toLowerCase().replace('_', ' ')} {' ('}{' '}
                        {option.count} {')'}
                    </h2>
                ))}
            </div>
            <div className="w-full flex gap-4 items-center mt-6 justify-center md:px-2 sm:mt-2">
                <div className="w-2/3">
                    <Search
                        filter={query}
                        onChange={value => setQuery(value)}
                        placeholder="Filter..."
                    />
                </div>
            </div>
        </>
    )
}

export default SortingDisplay
export type { Option }
