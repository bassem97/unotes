import Switch from 'react-switch'

type Props = {
    onChange: (
        checked: boolean,
        event: React.SyntheticEvent<MouseEvent | KeyboardEvent> | MouseEvent,
        id: string,
    ) => void
    checked: boolean | null
    label: string
    disabled?: boolean
}

const SwitchInput = ({ onChange, checked, label, disabled = false }: Props) => {
    return (
        <div className="flex gap-2 mt-2 items-center">
            <label
                htmlFor={label}
                className={
                    'text-xl min-h-[1.125rem] w-fit text-font2 dark:text-bg xs:text-sm capitalize leading-none '
                }
            >
                {label}
            </label>
            <Switch
                id={label}
                className="text-sm"
                height={25}
                handleDiameter={20}
                onChange={onChange}
                checked={checked ?? false}
                disabled={disabled}
            />
        </div>
    )
}

export default SwitchInput
