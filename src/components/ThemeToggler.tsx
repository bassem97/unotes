import { BsMoonStarsFill } from 'react-icons/bs'
import { FaSun } from 'react-icons/fa'

interface Props {
    isDarkMode: boolean
    handleThemeChange: () => void
}

const ThemeToggler = ({ handleThemeChange, isDarkMode }: Props) => {
    return (
        <div
            onClick={handleThemeChange}
            className="rounded-full cursor-pointer border-2 items-center relative aspect-[10/5] w-12 flex p-[1px]"
        >
            <div
                className={`p-[1px] transition-all duration-500 flex justify-center items-center rounded-full aspect-square absolute h-[130%] ${
                    isDarkMode
                        ? 'bg-black left-[calc(100%-1.25rem)] custom-dark-shadow'
                        : 'bg-yellow-400 -left-2 custom-shadow'
                }`}
            >
                {isDarkMode ? (
                    <BsMoonStarsFill className="text-xs text-bg" />
                ) : (
                    <FaSun className="text-lg text-bg" />
                )}
            </div>
        </div>
    )
}

export default ThemeToggler
