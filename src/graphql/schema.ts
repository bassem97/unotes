import { makeSchema, connectionPlugin } from 'nexus'
import { join } from 'path'
import * as typeDefs from './typeDefs'

export const schema = makeSchema({
    types: typeDefs,
    plugins: [connectionPlugin()],
    outputs: {
        typegen: join(
            process.cwd(),
            'node_modules',
            '@typeDefs',
            'nexus-typegen',
            'index.d.ts',
        ),
        schema: join(process.cwd(), 'src', 'graphql', 'schema.graphql'),
    },
    contextType: {
        export: 'Context',
        module: join(process.cwd(), 'src', 'graphql', 'context.ts'),
    },
})
