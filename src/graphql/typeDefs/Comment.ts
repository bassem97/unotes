import {
    extendType,
    inputObjectType,
    intArg,
    nonNull,
    objectType,
    stringArg,
} from 'nexus'
import { User } from '@/graphql/typeDefs/User'
import { notifyUser } from '@/lib/notifyUser'
import { NotificationSettings } from '@prisma/client'

export const Comment = objectType({
    name: 'Comment',
    definition(t) {
        t.nonNull.string('id')
        t.nonNull.string('body')
        t.nonNull.string('createdAt')
        t.string('updatedAt')
        t.nonNull.string('userId')
        t.nonNull.string('documentId')
        t.nonNull.field('user', {
            type: nonNull('User'),
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.comment
                    .findUnique({
                        where: { id: _parent.id },
                    })
                    .user()
            },
        })
        t.nonNull.field('document', {
            type: nonNull('Document'),
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.comment
                    .findUnique({
                        where: { id: _parent.id },
                    })
                    .document()
            },
        })
        t.list.field('likedBy', {
            type: User,
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.comment
                    .findUnique({
                        where: { id: _parent.id },
                        include: {
                            likedBy: true,
                        },
                    })
                    .likedBy()
            },
        })
        t.list.field('dislikedBy', {
            type: 'User',
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.comment.findUnique({
                    where: { id: _parent.id },
                    include: {
                        dislikedBy: true,
                    },
                })
            },
        })
    },
})

export const CreateCommentInput = inputObjectType({
    name: 'CreateCommentInput',
    definition(t) {
        t.nonNull.string('body')
        t.nonNull.string('userId')
        t.nonNull.string('documentId')
    },
})

export const UpdateCommentInput = inputObjectType({
    name: 'UpdateCommentInput',
    definition(t) {
        t.nonNull.string('id')
        t.nonNull.string('body')
    },
})

// Queries
export const comments = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('comments', {
            type: nonNull(Comment),
            args: {
                skip: intArg(),
                limit: intArg(),
            },
            async resolve(_parent, { skip = 0, limit }, ctx) {
                return await ctx.prisma.comment.findMany({
                    skip,
                    ...(limit && { take: limit }),
                })
            },
        })
    },
})

export const comment = extendType({
    type: 'Query',
    definition(t) {
        t.field('comment', {
            type: Comment,
            args: {
                id: nonNull(stringArg()),
            },
            async resolve(_parent, { id }, ctx) {
                return await ctx.prisma.comment.findUnique({
                    where: { id },
                })
            },
        })
    },
})

// Mutations

export const createComment = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createComment', {
            type: Comment,
            args: {
                input: nonNull(CreateCommentInput),
            },
            async resolve(_parent, { input }, ctx) {
                const comment = await ctx.prisma.comment.create({
                    data: input,
                })

                const document = await ctx.prisma.document.findUnique({
                    where: { id: input.documentId },
                    include: {
                        uploadedBy: {
                            include: { notificationSettings: true },
                        },
                    },
                })
                const notificationSetting =
                    document.uploadedBy.notificationSettings.find(
                        (notificationSetting: NotificationSettings) =>
                            notificationSetting.type === 'COMMENT_ON_DOCUMENT',
                    )
                if (notificationSetting)
                    await notifyUser(
                        document.uploadedBy,
                        `Comment on your document ${document.name}`,
                        `Someone created a comment on your document ${document.name}`,
                        '/documents/' + document.id,
                        ctx.prisma,
                    )

                return comment
            },
        })
    },
})

export const updateComment = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('updateComment', {
            type: Comment,
            args: {
                input: nonNull(UpdateCommentInput),
            },
            async resolve(_parent, { input }, ctx) {
                return await ctx.prisma.comment.update({
                    where: { id: input.id },
                    data: {
                        body: input.body,
                    },
                })
            },
        })
    },
})

export const deleteComment = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('deleteComment', {
            type: Comment,
            args: {
                id: nonNull(stringArg()),
            },
            async resolve(_parent, { id }, ctx) {
                return await ctx.prisma.comment.delete({
                    where: { id },
                })
            },
        })
    },
})

/**
 * Like a comment
 */
export const likeComment = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('likeComment', {
            type: Comment,
            args: {
                id: nonNull(stringArg()),
                userId: nonNull(stringArg()),
            },
            async resolve(_parent, { id, userId }, ctx) {
                return await ctx.prisma.comment.update({
                    where: { id },
                    data: {
                        likedBy: {
                            connect: {
                                id: userId,
                            },
                        },
                        dislikedBy: {
                            disconnect: {
                                id: userId,
                            },
                        },
                    },
                })
            },
        })
    },
})

/**
 * Remove a like from a comment
 */
export const unLikeComment = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('unLikeComment', {
            type: Comment,
            args: {
                id: nonNull(stringArg()),
                userId: nonNull(stringArg()),
            },
            async resolve(_parent, { id, userId }, ctx) {
                return await ctx.prisma.comment.update({
                    where: { id },
                    data: {
                        likedBy: {
                            disconnect: {
                                id: userId,
                            },
                        },
                    },
                })
            },
        })
    },
})

/**
 * Dislike a comment
 */
export const dislikeComment = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('dislikeComment', {
            type: Comment,
            args: {
                id: nonNull(stringArg()),
                userId: nonNull(stringArg()),
            },
            async resolve(_parent, { id, userId }, ctx) {
                return await ctx.prisma.comment.update({
                    where: { id },
                    data: {
                        dislikedBy: {
                            connect: {
                                id: userId,
                            },
                        },
                        likedBy: {
                            disconnect: {
                                id: userId,
                            },
                        },
                    },
                })
            },
        })
    },
})

/**
 * Remove a dislike from a comment
 */
export const unDislikeComment = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('unDislikeComment', {
            type: Comment,
            args: {
                id: nonNull(stringArg()),
                userId: nonNull(stringArg()),
            },
            async resolve(_parent, { id, userId }, ctx) {
                return await ctx.prisma.comment.update({
                    where: { id },
                    data: {
                        dislikedBy: {
                            disconnect: {
                                id: userId,
                            },
                        },
                    },
                })
            },
        })
    },
})
