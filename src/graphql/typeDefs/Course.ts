import {
    booleanArg,
    extendType,
    inputObjectType,
    intArg,
    nonNull,
    objectType,
    stringArg,
} from 'nexus'
import { School } from '@/graphql/typeDefs/School'
import { Document } from '@/graphql/typeDefs/Document'
import { notifyUser } from '@/lib/notifyUser'
import { NotificationSettings } from '@prisma/client'

export const Course = objectType({
    name: 'Course',
    definition(t) {
        t.nonNull.string('id')
        t.nonNull.string('name')
        t.nonNull.string('code')
        t.nonNull.boolean('deleted')
        t.nonNull.boolean('verified')
        t.nonNull.string('schoolId')
        t.string('image')
        t.nonNull.field('school', {
            type: nonNull(School),
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.course
                    .findUnique({
                        where: { id: _parent.id },
                    })
                    .school()
            },
        })
        t.nonNull.list.field('documents', {
            type: Document,
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.course
                    .findUnique({
                        where: { id: _parent.id },
                    })
                    .documents()
            },
        })
    },
})

export const CreateCourseInput = inputObjectType({
    name: 'CreateCourseInput',
    definition(t) {
        t.nonNull.string('name')
        t.nonNull.string('code')
        t.nonNull.string('schoolId')
        t.boolean('deleted')
        t.boolean('verified')
        t.nonNull.list.string('moderators')
        t.string('image')
    },
})

export const UpdateCourseInput = inputObjectType({
    name: 'UpdateCourseInput',
    definition(t) {
        t.nonNull.string('id')
        t.nonNull.string('name')
        t.nonNull.string('code')
        t.nonNull.string('schoolId')
        t.list.string('moderators')
        t.boolean('deleted')
        t.boolean('verified')
        t.string('image')
    },
})

// Queries
export const courses = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('courses', {
            type: nonNull(Course),
            args: {
                schoolId: stringArg(),
                skip: intArg(),
                limit: intArg(),
                name: stringArg(),
            },
            resolve: (_, { schoolId, skip = 0, limit, name }, ctx) => {
                return ctx.prisma.course.findMany({
                    skip,
                    ...(limit && { take: limit }),
                    where: {
                        schoolId,
                        ...(name && {
                            name: { contains: name, mode: 'insensitive' },
                        }),
                    },
                })
            },
        })
    },
})

export const favoriteCourses = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('favoriteCourses', {
            type: nonNull(Course),
            args: {
                userId: nonNull(stringArg()),
                skip: intArg(),
                limit: intArg(),
            },
            resolve: async (_, { userId, skip = 0, limit }, ctx) => {
                return ctx.prisma.course.findMany({
                    skip,
                    ...(limit && { take: limit }),
                    where: {
                        users: {
                            some: {
                                id: userId,
                            },
                        },
                    },
                })
            },
        })
    },
})

export const course = extendType({
    type: 'Query',
    definition(t) {
        t.field('course', {
            type: nonNull(Course),
            args: {
                id: nonNull(stringArg()),
            },
            resolve: (_, { id }, ctx) => {
                return ctx.prisma.course.findUnique({
                    where: { id },
                })
            },
        })
    },
})

// Mutations
export const createCourse = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createCourse', {
            type: nonNull(Course),
            args: {
                input: nonNull(CreateCourseInput),
            },
            resolve: async (_, { input }, ctx) => {
                const schoolId = input.schoolId
                delete input.schoolId
                const course = await ctx.prisma.course.create({
                    data: {
                        ...input,
                        school: { connect: { id: schoolId } },
                        moderators: {
                            connect:
                                input.moderators.map((moderatorId: string) => ({
                                    id: moderatorId,
                                })) || [],
                        },
                    },
                })

                // Send notification to users following the school
                const school = await ctx.prisma.school.findUnique({
                    where: { id: schoolId },
                    include: {
                        users: {
                            include: {
                                notificationSettings: true,
                            },
                        },
                    },
                })
                for (const user of school.users) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting = user.notificationSettings.find(
                        (notificationSetting: NotificationSettings) =>
                            notificationSetting.type === 'FAVORITE_SCHOOL',
                    )
                    if (notificationSetting)
                        await notifyUser(
                            user,
                            `Course ${course.name} created`,
                            `Course ${course.name} created`,
                            '/courses/' + course.id,
                            ctx.prisma,
                        )
                }

                return course
            },
        })
    },
})

export const updateCourse = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('updateCourse', {
            type: nonNull(Course),
            args: {
                input: nonNull(UpdateCourseInput),
            },
            resolve: async (_, { input }, ctx) => {
                const schoolId = input.schoolId
                delete input.schoolId
                const updatedCourse = await ctx.prisma.course.update({
                    where: { id: input.id },
                    data: {
                        ...input,
                        school: { connect: { id: schoolId } },
                        moderators: {
                            connect:
                                input.moderators.map((moderatorId: string) => ({
                                    id: moderatorId,
                                })) || [],
                        },
                    },
                })

                // Send notification to moderators
                const course = await ctx.prisma.course.findUnique({
                    where: { id: input.id },
                    include: {
                        moderators: {
                            include: {
                                notificationSettings: true,
                            },
                        },
                        school: {
                            include: {
                                users: {
                                    include: {
                                        notificationSettings: true,
                                    },
                                },
                            },
                        },
                    },
                })
                for (const moderator of course.moderators) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting =
                        moderator.notificationSettings.find(
                            (notificationSetting: NotificationSettings) =>
                                notificationSetting.type === 'FAVORITE_SCHOOL',
                        )
                    if (notificationSetting)
                        await notifyUser(
                            moderator,
                            `Course ${course.name} Updated`,
                            `Course ${course.name} Updated`,
                            '/courses/' + course.id,
                            ctx.prisma,
                        )
                }

                // Send notification to users following the school
                for (const user of course.school.users) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting = user.notificationSettings.find(
                        (notificationSetting: NotificationSettings) =>
                            notificationSetting.type === 'FAVORITE_SCHOOL',
                    )
                    if (notificationSetting)
                        await notifyUser(
                            user,
                            `Course ${course.name} Updated`,
                            `Course ${course.name} Updated`,
                            '/courses/' + course.id,
                            ctx.prisma,
                        )
                }

                return updatedCourse
            },
        })
    },
})

export const deleteCourse = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('deleteCourse', {
            type: nonNull(Course),
            args: {
                id: nonNull(stringArg()),
            },
            resolve: async (_, { id }, ctx) => {
                // Send notification to moderators
                const course = await ctx.prisma.course.findUnique({
                    where: { id },
                    include: {
                        moderators: {
                            include: {
                                notificationSettings: true,
                            },
                        },
                        school: {
                            include: {
                                users: {
                                    include: {
                                        notificationSettings: true,
                                    },
                                },
                            },
                        },
                    },
                })
                for (const moderator of course.moderators) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting =
                        moderator.notificationSettings.find(
                            (notificationSetting: NotificationSettings) =>
                                notificationSetting.type === 'FAVORITE_SCHOOL',
                        )
                    if (notificationSetting)
                        await notifyUser(
                            moderator,
                            `Course ${course.name} deleted`,
                            `Course ${course.name} deleted`,
                            null,
                            ctx.prisma,
                        )
                }

                // Send notification to users following the school
                for (const user of course.school.users) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting = user.notificationSettings.find(
                        (notificationSetting: NotificationSettings) =>
                            notificationSetting.type === 'FAVORITE_SCHOOL',
                    )
                    if (notificationSetting)
                        await notifyUser(
                            user,
                            `Course ${course.name} Deleted`,
                            `Course ${course.name} Deleted`,
                            null,
                            ctx.prisma,
                        )
                }
                return await ctx.prisma.course.delete({
                    where: { id },
                })
            },
        })
    },
})

export const deleteCoursesForTest = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('deleteCoursesForTest', {
            type: nonNull(Course),
            args: {
                code: nonNull(stringArg()),
            },
            resolve: async (_, { code }, ctx) => {
                return await ctx.prisma.course.deleteMany({
                    where: { code },
                })
            },
        })
    },
})
