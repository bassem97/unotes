import {
    booleanArg,
    enumType,
    extendType,
    inputObjectType,
    intArg,
    nonNull,
    objectType,
    stringArg,
} from 'nexus'
import { Course } from '@/graphql/typeDefs/Course'
import { notifyUser } from '@/lib/notifyUser'
import { NotificationSettings } from '@prisma/client'
import { FileDocument } from './FileDocument'

const Season = enumType({
    name: 'Season',
    members: ['FALL', 'WINTER', 'SUMMER'],
})

const DocumentType = enumType({
    name: 'DocumentType',
    members: [
        'FINAL_EXAM',
        'MIDTERM_EXAM',
        'QUIZZES',
        'LABS',
        'EXERCISES',
        'OTHERS',
    ],
})

export const DocumentsBySchoolID = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('DocumentsBySchoolID', {
            type: nonNull(Document),
            args: {
                courseId: stringArg(),
                skip: intArg(),
                limit: intArg(),
                verified: booleanArg(),
                name: stringArg(),
            },
            resolve: (
                _,
                { courseId, verified, skip = 0, limit, name },
                ctx,
            ) => {
                return ctx.prisma.document.findMany({
                    skip,
                    ...(limit && { take: limit }),
                    where: {
                        courseId,
                        deleted: false,
                        verified,
                        ...(name && {
                            name: { contains: name, mode: 'insensitive' },
                        }),
                    },
                })
            },
        })
    },
})

export const DocumentsByUserID = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('DocumentsByUserID', {
            type: nonNull(Document),
            args: {
                userId: nonNull(stringArg()),
                skip: intArg(),
                limit: intArg(),
                name: stringArg(),
                season: Season,
                verified: booleanArg(),
            },
            resolve: (
                _,
                { userId, name, season, verified, skip = 0, limit },
                ctx,
            ) => {
                return ctx.prisma.document.findMany({
                    where: {
                        userId,
                        ...(name && {
                            name: { contains: name, mode: 'insensitive' },
                        }),
                        ...(season && { season: { equals: season } }),
                        ...(verified === undefined && {
                            verified: { equals: verified },
                        }),
                        deleted: false,
                    },
                    orderBy: [
                        {
                            deletedAt: 'desc',
                        },
                        {
                            name: 'asc',
                        },
                    ],
                    skip,
                    ...(limit && { take: limit }),
                })
            },
        })
    },
})

export const Document = objectType({
    name: 'Document',
    definition(t) {
        t.nonNull.string('id')
        t.nonNull.string('userId')
        t.nonNull.string('name')
        t.nonNull.field('season', { type: Season })
        t.nonNull.int('year')
        t.nonNull.field('type', { type: DocumentType })
        t.nonNull.boolean('isCompleted')
        t.nonNull.boolean('isSolutionsIncluded')
        t.nonNull.boolean('deleted')
        t.nonNull.boolean('verified')
        t.nonNull.string('courseId')
        t.nonNull.string('likes')
        t.nonNull.string('dislikes')
        t.nonNull.field('course', {
            type: nonNull(Course),
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.document
                    .findUnique({
                        where: { id: _parent.id },
                    })
                    .course()
            },
        })
        t.nonNull.field('files', {
            type: nonNull(FileDocument),
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.document
                    .findUnique({
                        where: { id: _parent.id },
                    })
                    .files()
            },
        })
    },
})

export const CreateDocumentInput = inputObjectType({
    name: 'CreateDocumentInput',
    definition(t) {
        t.nonNull.string('userId')
        t.nonNull.string('name')
        t.nonNull.field('season', { type: Season })
        t.nonNull.int('year')
        t.nonNull.field('type', { type: DocumentType })
        t.nonNull.boolean('isSolutionsIncluded')
        t.nonNull.string('courseId')
        t.boolean('isCompleted')
        t.boolean('deleted')
        t.boolean('verified')
        t.list.string('tags')
        t.list.string('files')
    },
})

export const UpdateDocumentInput = inputObjectType({
    name: 'UpdateDocumentInput',
    definition(t) {
        t.string('userId')
        t.string('name')
        t.field('season', { type: Season })
        t.int('year')
        t.field('type', { type: DocumentType })
        t.boolean('isCompleted')
        t.boolean('isSolutionsIncluded')
        t.string('courseId')
        t.boolean('deleted')
        t.boolean('verified')
        t.list.string('tags')
        t.list.string('files')
    },
})

// Queries
export const documents = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('documents', {
            type: nonNull(Document),
            args: {
                skip: intArg(),
                limit: intArg(),
            },
            resolve: (_, { skip = 0, limit }, ctx) => {
                return ctx.prisma.document.findMany({
                    skip,
                    ...(limit && { take: limit }),
                })
            },
        })
    },
})

export const document = extendType({
    type: 'Query',
    definition(t) {
        t.field('document', {
            type: nonNull(Document),
            args: {
                id: nonNull(stringArg()),
            },
            resolve: (_, { id }, ctx) => {
                return ctx.prisma.document.findUnique({
                    where: { id },
                })
            },
        })
    },
})

// Mutations
export const createDocument = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createDocument', {
            type: nonNull(Document),
            args: {
                input: nonNull(CreateDocumentInput),
            },
            resolve: async (_, { input }, ctx) => {
                const document = await ctx.prisma.document.create({
                    data: {
                        ...input,
                        tags: {
                            connect:
                                input.tags.map((moderatorId: string) => ({
                                    id: moderatorId,
                                })) || [],
                        },
                        files: {
                            create:
                                input.files.map((file: string) => ({
                                    url: file,
                                })) || [],
                        },
                    },
                    include: {
                        files: true,
                    },
                })

                // send notification to course moderators
                const doc = await ctx.prisma.document.findUnique({
                    where: { id: document.id },
                    include: {
                        uploadedBy: true,
                        course: {
                            include: {
                                moderators: {
                                    include: {
                                        notificationSettings: true,
                                    },
                                },
                                school: {
                                    include: {
                                        users: {
                                            include: {
                                                notificationSettings: true,
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                })

                for (const moderator of doc.course.moderators) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting =
                        moderator.notificationSettings.find(
                            (notificationSetting: NotificationSettings) =>
                                notificationSetting.type === 'FAVORITE_SCHOOL',
                        )
                    if (notificationSetting)
                        await notifyUser(
                            moderator,
                            `Document ${doc.name} Created`,
                            `Document ${doc.name} created in course ${doc.course.name}`,
                            '/documents/' + doc.id,
                            ctx.prisma,
                        )
                }

                // Send notification to users following the school
                for (const user of doc.course.school.users) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting = user.notificationSettings.find(
                        (notificationSetting: NotificationSettings) =>
                            notificationSetting.type === 'FAVORITE_SCHOOL',
                    )
                    if (notificationSetting)
                        await notifyUser(
                            user,
                            `Document ${doc.name} Created`,
                            `Document ${doc.name} created in course ${doc.course.name}`,
                            '/documents/' + doc.id,
                            ctx.prisma,
                        )
                }

                return document
            },
        })
    },
})

export const updateDocument = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('updateDocument', {
            type: nonNull(Document),
            args: {
                id: nonNull(stringArg()),
                input: nonNull(UpdateDocumentInput),
            },
            resolve: async (_, { id, input }, ctx) => {
                const updatedDocument = await ctx.prisma.document.update({
                    data: {
                        ...input,
                        deletedAt: input.deleted ? new Date() : null,
                        tags: {
                            connect:
                                input.tags.map((moderatorId: string) => ({
                                    id: moderatorId,
                                })) || [],
                        },
                    },
                    where: { id },
                })

                // send notification to course moderators
                const doc = await ctx.prisma.document.findUnique({
                    where: { id },
                    include: {
                        uploadedBy: true,
                        course: {
                            include: {
                                moderators: {
                                    include: {
                                        notificationSettings: true,
                                    },
                                },
                                school: {
                                    include: {
                                        users: {
                                            include: {
                                                notificationSettings: true,
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                })

                for (const moderator of doc.course.moderators) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting =
                        moderator.notificationSettings.find(
                            (notificationSetting: NotificationSettings) =>
                                notificationSetting.type === 'FAVORITE_SCHOOL',
                        )
                    if (notificationSetting)
                        await notifyUser(
                            moderator,
                            `Document ${doc.name} updated`,
                            `Document ${doc.name} updated`,
                            '/documents/' + doc.id,
                            ctx.prisma,
                        )
                }

                for (const moderator of doc.course.school.users) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting =
                        moderator.notificationSettings.find(
                            (notificationSetting: NotificationSettings) =>
                                notificationSetting.type === 'FAVORITE_SCHOOL',
                        )
                    if (notificationSetting)
                        await notifyUser(
                            moderator,
                            `Document ${doc.name} updated`,
                            `Document ${doc.name} updated`,
                            '/documents/' + doc.id,
                            ctx.prisma,
                        )
                }

                return updatedDocument
            },
        })
    },
})

export const deleteDocument = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('deleteDocument', {
            type: nonNull(Document),
            args: {
                id: nonNull(stringArg()),
            },
            resolve: async (_, { id }, ctx) => {
                // send notification to course moderators
                const doc = await ctx.prisma.document.findUnique({
                    where: { id },
                    include: {
                        uploadedBy: true,
                        course: {
                            include: {
                                moderators: {
                                    include: {
                                        notificationSettings: true,
                                    },
                                },
                                school: {
                                    include: {
                                        users: {
                                            include: {
                                                notificationSettings: true,
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                })

                for (const moderator of doc.course.moderators) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting =
                        moderator.notificationSettings.find(
                            (notificationSetting: NotificationSettings) =>
                                notificationSetting.type === 'FAVORITE_SCHOOL',
                        )
                    if (notificationSetting)
                        await notifyUser(
                            moderator,
                            `Document ${doc.name} deleted`,
                            `Document ${doc.name} deleted`,
                            null,
                            ctx.prisma,
                        )
                }
                for (const user of doc.course.school.users) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting = user.notificationSettings.find(
                        (notificationSetting: NotificationSettings) =>
                            notificationSetting.type === 'FAVORITE_SCHOOL',
                    )
                    if (notificationSetting)
                        await notifyUser(
                            user,
                            `Document ${doc.name} deleted`,
                            `Document ${doc.name} deleted`,
                            null,
                            ctx.prisma,
                        )
                }

                return await ctx.prisma.document.update({
                    where: { id },
                    data: { deleted: true },
                })
            },
        })
    },
})

/**
 * Like a document
 */
export const likeDocument = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('likeDocument', {
            type: Document,
            args: {
                id: nonNull(stringArg()),
                userId: nonNull(stringArg()),
            },
            async resolve(_parent, { id, userId }, ctx) {
                return await ctx.prisma.document.update({
                    where: { id },
                    data: {
                        likedBy: {
                            connect: {
                                id: userId,
                            },
                        },
                        dislikedBy: {
                            disconnect: {
                                id: userId,
                            },
                        },
                    },
                })
            },
        })
    },
})

/**
 * Remove like from a document
 */
export const unLikeDocument = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('unLikeDocument', {
            type: Document,
            args: {
                id: nonNull(stringArg()),
                userId: nonNull(stringArg()),
            },
            async resolve(_parent, { id, userId }, ctx) {
                return await ctx.prisma.document.update({
                    where: { id },
                    data: {
                        likedBy: {
                            disconnect: {
                                id: userId,
                            },
                        },
                    },
                })
            },
        })
    },
})

/**
 * Dislike a document
 */
export const dislikeDocument = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('dislikeDocument', {
            type: Document,
            args: {
                id: nonNull(stringArg()),
                userId: nonNull(stringArg()),
            },
            async resolve(_parent, { id, userId }, ctx) {
                return await ctx.prisma.document.update({
                    where: { id },
                    data: {
                        dislikedBy: {
                            connect: {
                                id: userId,
                            },
                        },
                        likedBy: {
                            disconnect: {
                                id: userId,
                            },
                        },
                    },
                })
            },
        })
    },
})

/**
 * Remove dislike from a document
 */
export const unDislikeDocument = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('unDislikeDocument', {
            type: Document,
            args: {
                id: nonNull(stringArg()),
                userId: nonNull(stringArg()),
            },
            async resolve(_parent, { id, userId }, ctx) {
                return await ctx.prisma.document.update({
                    where: { id },
                    data: {
                        dislikedBy: {
                            disconnect: {
                                id: userId,
                            },
                        },
                    },
                })
            },
        })
    },
})
