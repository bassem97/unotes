import {
    extendType,
    inputObjectType,
    intArg,
    nonNull,
    objectType,
    stringArg,
} from 'nexus'

export const FileDocument = objectType({
    name: 'FileDocument',
    definition(t) {
        t.string('id')
        t.nonNull.string('url')
        t.nonNull.string('createdAt')
        t.string('updatedAt')
        t.nonNull.string('documentId')
        t.nonNull.field('document', {
            type: nonNull('Document'),
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.fileDocument
                    .findUnique({
                        where: { id: _parent.id },
                    })
                    .document()
            },
        })
    },
})

export const CreateFileDocumentInput = inputObjectType({
    name: 'CreateFileDocumentInput',
    definition(t) {
        t.nonNull.string('url')
        t.nonNull.string('documentId')
    },
})

export const UpdateFileDocumentInput = inputObjectType({
    name: 'UpdateFileDocumentInput',
    definition(t) {
        t.nonNull.string('url')
        t.string('documentId')
    },
})

/**
 * QUERIES
 */
export const getFileDocumentsByDocument = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('fileDocuments', {
            type: nonNull(FileDocument),
            args: {
                documentId: nonNull(stringArg()),
                skip: intArg(),
                limit: intArg(),
            },
            async resolve(_parent, { skip = 0, limit }, ctx) {
                return ctx.prisma.fileDocument.findMany({
                    skip,
                    ...(limit && { take: limit }),
                    where: { documentId: _parent.documentId },
                })
            },
        })
    },
})

export const fileDocument = extendType({
    type: 'Query',
    definition(t) {
        t.field('fileDocument', {
            type: FileDocument,
            args: {
                id: nonNull(stringArg()),
            },
            async resolve(_parent, { id }, ctx) {
                return ctx.prisma.fileDocument.findUnique({
                    where: { id },
                })
            },
        })
    },
})

/**
 * MUTATIONS
 */
export const createFileDocument = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createFileDocument', {
            type: FileDocument,
            args: {
                input: nonNull(CreateFileDocumentInput),
            },
            async resolve(_parent, { input }, ctx) {
                return ctx.prisma.fileDocument.create({
                    data: input,
                })
            },
        })
    },
})

export const updateFileDocument = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('updateFileDocument', {
            type: FileDocument,
            args: {
                input: nonNull(UpdateFileDocumentInput),
            },
            async resolve(_parent, { input }, ctx) {
                return ctx.prisma.fileDocument.update({
                    where: { id: input.id },
                    data: {
                        url: input.url,
                        documentId: input.documentId,
                    },
                })
            },
        })
    },
})

export const deleteFileDocument = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('deleteFileDocument', {
            type: FileDocument,
            args: {
                id: nonNull(stringArg()),
            },
            async resolve(_parent, { id }, ctx) {
                return ctx.prisma.fileDocument.delete({
                    where: { id },
                })
            },
        })
    },
})
