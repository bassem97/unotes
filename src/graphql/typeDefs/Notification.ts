import {
    nonNull,
    objectType,
    extendType,
    inputObjectType,
    intArg,
    stringArg,
} from 'nexus'
import { User } from '@/graphql/typeDefs/User'

export const Notification = objectType({
    name: 'Notification',
    definition(t) {
        t.nonNull.string('id')
        t.nonNull.string('body')
        t.nonNull.boolean('seen')
        t.string('link')
        t.nonNull.string('createdAt')
        t.nonNull.string('userId')
        t.nonNull.field('user', {
            type: nonNull(User),
            resolve(_parent, _args, ctx) {
                return ctx.prisma.notification
                    .findUnique({
                        where: { id: _parent.id },
                    })
                    .user()
            },
        })
    },
})

export const CreateNotificationInput = inputObjectType({
    name: 'CreateNotificationInput',
    definition(t) {
        t.nonNull.string('body')
        t.nonNull.string('userId')
        t.string('link')
    },
})

/**
 * Queries
 */

export const notifications = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('notifications', {
            type: nonNull(Notification),
            args: {
                skip: intArg(),
                limit: intArg(),
            },
            resolve: (_, { skip = 0, limit }, ctx) => {
                return ctx.prisma.notification.findMany({
                    skip,
                    ...(limit && { take: limit }),
                })
            },
        })
    },
})

export const notification = extendType({
    type: 'Query',
    definition(t) {
        t.field('notification', {
            type: nonNull(Notification),
            args: {
                id: nonNull(stringArg()),
            },
            resolve: (_, { id }, ctx) => {
                return ctx.prisma.notification.findUnique({
                    where: { id },
                })
            },
        })
    },
})

export const notificationsByUser = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('notificationsByUser', {
            type: Notification,
            args: {
                userId: nonNull(stringArg()),
                skip: intArg(),
                limit: intArg(),
            },
            resolve: (_, { userId, skip = 0, limit }, ctx) => {
                if (limit)
                    return ctx.prisma.notification.findMany({
                        skip,
                        take: limit,
                        where: { userId },
                        orderBy: { createdAt: 'desc' },
                    })
                return ctx.prisma.notification.findMany({
                    skip,
                    where: { userId },
                    orderBy: { createdAt: 'desc' },
                })
            },
        })
    },
})

/**
 * Mutations
 */
export const createNotification = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createNotification', {
            type: nonNull(Notification),
            args: {
                input: nonNull(CreateNotificationInput),
            },
            resolve: (_, { input }, ctx) => {
                return ctx.prisma.notification.create({
                    data: input,
                })
            },
        })
    },
})

export const setSeenNotification = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('setSeenNotification', {
            type: nonNull(Notification),
            args: {
                id: nonNull(stringArg()),
            },
            resolve: (_, { id }, ctx) => {
                return ctx.prisma.notification.update({
                    data: { seen: true },
                    where: { id },
                })
            },
        })
    },
})

export const deleteNotification = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('deleteNotification', {
            type: nonNull(Notification),
            args: {
                id: nonNull(stringArg()),
            },
            resolve: (_, { id }, ctx) => {
                return ctx.prisma.notification.delete({
                    where: { id },
                })
            },
        })
    },
})

export const deleteNotificationsByUser = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('deleteNotificationsByUser', {
            type: 'String',
            args: {
                userId: stringArg(),
            },
            resolve: async (_, { userId }, ctx) => {
                await ctx.prisma.notification.deleteMany({
                    where: { userId },
                })
                return 'Deleted'
            },
        })
    },
})
