import {
    extendType,
    inputObjectType,
    intArg,
    nonNull,
    objectType,
    stringArg,
} from 'nexus'
import { Course } from '@/graphql/typeDefs/Course'
import { notifyUser } from '@/lib/notifyUser'
import { NotificationSettings } from '@prisma/client'

export const School = objectType({
    name: 'School',
    definition(t) {
        t.nonNull.string('id')
        t.nonNull.string('name')
        t.nonNull.boolean('deleted')
        t.nonNull.boolean('verified')
        t.nonNull.string('userId')
        t.string('image')
        t.nonNull.list.field('courses', {
            type: Course,
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.school
                    .findUnique({
                        where: { id: _parent.id },
                    })
                    .courses()
            },
        })
    },
})

export const CreateSchoolInput = inputObjectType({
    name: 'CreateSchoolInput',
    definition(t) {
        t.nonNull.string('name')
        t.boolean('deleted')
        t.boolean('verified')
        t.nonNull.string('userId')
        t.string('image')
    },
})

export const UpdateSchoolInput = inputObjectType({
    name: 'UpdateSchoolInput',
    definition(t) {
        t.nonNull.string('id')
        t.nonNull.string('name')
        t.boolean('deleted')
        t.boolean('verified')
        t.string('image')
    },
})

// Queries
export const schools = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('schools', {
            type: nonNull(School),
            args: {
                skip: intArg(),
                limit: intArg(),
                name: stringArg(),
            },
            resolve: (_, { skip = 0, limit, name }, ctx) => {
                return ctx.prisma.school.findMany({
                    skip,
                    ...(limit && { take: limit }), // Only include take: limit if limit is truthy
                    where: {
                        deleted: false,
                        verified: true,
                        ...(name && {
                            name: { contains: name, mode: 'insensitive' },
                        }),
                    },
                })
            },
        })
    },
})

export const favoriteSchools = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('favoriteSchools', {
            type: nonNull(School),
            args: {
                userId: nonNull(stringArg()),
                skip: intArg(),
                limit: intArg(),
            },
            resolve: async (_, { userId, skip = 0, limit }, ctx) => {
                return ctx.prisma.school.findMany({
                    skip,
                    ...(limit && { take: limit }),
                    where: {
                        users: {
                            some: {
                                id: userId,
                            },
                        },
                    },
                })
            },
        })
    },
})

export const school = extendType({
    type: 'Query',
    definition(t) {
        t.field('school', {
            type: nonNull(School),
            args: {
                id: nonNull(stringArg()),
            },
            resolve: (_, { id }, ctx) => {
                return ctx.prisma.school.findUnique({
                    where: { id },
                })
            },
        })
    },
})

// Mutations
export const createSchool = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createSchool', {
            type: nonNull(School),
            args: {
                input: nonNull(CreateSchoolInput),
            },
            resolve: (_, { input }, ctx) => {
                return ctx.prisma.school.create({
                    data: input,
                })
            },
        })
    },
})

export const updateSchool = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('updateSchool', {
            type: nonNull(School),
            args: {
                input: nonNull(UpdateSchoolInput),
            },
            resolve: async (_, { input }, ctx) => {
                const updatedSchool = await ctx.prisma.school.update({
                    data: input,
                    where: { id: input.id },
                })

                // send notification to course moderators
                const school = await ctx.prisma.school.findUnique({
                    where: { id: input.id },
                    include: {
                        courses: {
                            include: {
                                moderators: {
                                    include: {
                                        notificationSettings: true,
                                    },
                                },
                            },
                        },
                        users: {
                            include: {
                                notificationSettings: true,
                            },
                        },
                    },
                })
                for (const course of school.courses) {
                    for (const moderator of course.moderators) {
                        // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                        const notificationSetting =
                            moderator.notificationSettings.find(
                                (notificationSetting: NotificationSettings) =>
                                    notificationSetting.type ===
                                    'FAVORITE_SCHOOL',
                            )
                        if (notificationSetting)
                            await notifyUser(
                                moderator,
                                `School ${school.name} updated`,
                                `School ${school.name} updated`,
                                '/schools/' + school.id,
                                ctx.prisma,
                            )
                    }
                }
                for (const user of school.users) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting = user.notificationSettings.find(
                        (notificationSetting: NotificationSettings) =>
                            notificationSetting.type === 'FAVORITE_SCHOOL',
                    )
                    if (notificationSetting)
                        await notifyUser(
                            user,
                            `School ${school.name} updated`,
                            `School ${school.name} updated`,
                            '/schools/' + school.id,
                            ctx.prisma,
                        )
                }

                return updatedSchool
            },
        })
    },
})

export const deleteSchool = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('deleteSchool', {
            type: nonNull(School),
            args: {
                id: nonNull(stringArg()),
            },
            resolve: async (_, { id }, ctx) => {
                // send notification to course moderators
                const school = await ctx.prisma.school.findUnique({
                    where: { id },
                    include: {
                        courses: {
                            include: {
                                moderators: {
                                    include: {
                                        notificationSettings: true,
                                    },
                                },
                            },
                        },
                        users: {
                            include: {
                                notificationSettings: true,
                            },
                        },
                    },
                })

                const result = await ctx.prisma.school.delete({
                    where: { id },
                })
                for (const course of school.courses) {
                    for (const moderator of course.moderators) {
                        // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                        const notificationSetting =
                            moderator.notificationSettings.find(
                                (notificationSetting: NotificationSettings) =>
                                    notificationSetting.type ===
                                    'FAVORITE_SCHOOL',
                            )
                        if (notificationSetting)
                            await notifyUser(
                                moderator,
                                `School ${school.name} deleted`,
                                `School ${school.name} deleted`,
                                null,
                                ctx.prisma,
                            )
                    }
                }
                for (const user of school.users) {
                    // check if moderator.notificationSettings array include 'FAVORITE_SCHOOL'
                    const notificationSetting = user.notificationSettings.find(
                        (notificationSetting: NotificationSettings) =>
                            notificationSetting.type === 'FAVORITE_SCHOOL',
                    )
                    if (notificationSetting)
                        await notifyUser(
                            user,
                            `School ${school.name} deleted`,
                            `School ${school.name} deleted`,
                            null,
                            ctx.prisma,
                        )
                }

                return result
            },
        })
    },
})

export const deleteSchoolsForTest = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('deleteSchoolsForTest', {
            type: nonNull(School),
            args: {
                name: nonNull(stringArg()),
            },
            resolve: async (_, { name }, ctx) => {
                return ctx.prisma.school.deleteMany({
                    where: {
                        name,
                    },
                })
            },
        })
    },
})
