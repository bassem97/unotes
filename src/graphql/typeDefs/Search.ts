import { nonNull, extendType, stringArg, intArg } from 'nexus'
import Fuse from 'fuse.js'
import { Course } from '@/graphql/typeDefs/Course'
import { Document } from '@/graphql/typeDefs/Document'
import { School } from '@/graphql/typeDefs/School'

const options = {
    threshold: 0.55,
    ignoreLocation: true,
}

export const searchSchool = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('searchSchool', {
            type: School,
            args: {
                query: nonNull(stringArg()),
                skip: intArg(),
                limit: intArg(),
            },
            resolve: async (_, { query, skip = 0, limit }, ctx) => {
                const schools = await ctx.prisma.school.findMany({
                    skip,
                    where: {
                        verified: true,
                        deleted: false,
                    },
                    ...(limit && { take: limit }),
                })

                const schoolOptions = {
                    ...options,
                    keys: ['name'],
                }

                const fuse = new Fuse(schools, schoolOptions)
                const result = fuse.search(query)
                return result.map(item => item.item)
            },
        })
    },
})

export const searchCourse = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('searchCourse', {
            type: Course,
            args: {
                query: nonNull(stringArg()),
                skip: intArg(),
                limit: intArg(),
            },
            resolve: async (_, { query, skip = 0, limit }, ctx) => {
                const courses = await ctx.prisma.course.findMany({
                    skip,
                    ...(limit && { take: limit }),
                    where: {
                        verified: true,
                        deleted: false,
                    },
                })

                const courseOptions = {
                    ...options,
                    keys: ['name', 'code'],
                }

                const fuse = new Fuse(courses, courseOptions)
                const result = fuse.search(query)
                return result.map(item => item.item)
            },
        })
    },
})

export const searchDocument = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('searchDocument', {
            type: Document,
            args: {
                query: nonNull(stringArg()),
                skip: intArg(),
                limit: intArg(),
            },
            resolve: async (_, { query, skip = 0, limit }, ctx) => {
                const documents = await ctx.prisma.document.findMany({
                    skip,
                    ...(limit && { take: limit }),
                    where: {
                        verified: true,
                        deleted: false,
                    },
                    include: {
                        tags: true,
                    },
                })

                const documentOptions = {
                    ...options,
                    keys: ['name'],
                }

                const fuse = new Fuse(documents, documentOptions)
                const result = fuse.search(query)
                return result.map(item => item.item)
            },
        })
    },
})
