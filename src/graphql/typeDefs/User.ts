import {
    extendType,
    inputObjectType,
    intArg,
    nonNull,
    objectType,
    stringArg,
} from 'nexus'
import { Notification } from '@/graphql/typeDefs/Notification'
import { School } from '@/graphql/typeDefs/School'
import bcrypt from 'bcryptjs'
import { sendEmail } from '@/lib/email'
import { render } from '@react-email/render'
import WelcomeTemplate from '../../../emails/WelcomeTemplate'
import VerificationRequestTemplate from '../../../emails/VerificationRequestTemplate'
import ResetPasswordRequestTemplate from '../../../emails/ResetPasswordRequestTemplate'
import PasswordChangedTemplate from '../../../emails/PasswordChangedTemplate'
import { notifyUser } from '@/lib/notifyUser'

export const User = objectType({
    name: 'User',
    definition(t) {
        t.nonNull.string('id')
        t.string('name')
        t.string('firstName')
        t.string('lastName')
        t.string('username')
        t.nonNull.string('email')
        t.string('image')
        t.string('password')
        t.nonNull.list.field('notifications', {
            type: Notification,
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.user
                    .findUnique({
                        where: { id: _parent.id },
                    })
                    .notifications()
            },
        })
        t.nonNull.list.field('schools', {
            type: School,
            async resolve(_parent, _args, ctx) {
                return await ctx.prisma.user
                    .findUnique({
                        where: { id: _parent.id },
                    })
                    .schools()
            },
        })
    },
})

export const CreateUserInput = inputObjectType({
    name: 'CreateUserInput',
    definition(t) {
        t.nonNull.string('firstName')
        t.nonNull.string('lastName')
        t.nonNull.string('username')
        t.nonNull.string('email')
        t.string('password')
    },
})

export const UpdateUserInput = inputObjectType({
    name: 'UpdateUserInput',
    definition(t) {
        t.string('id')
        t.string('firstName')
        t.string('lastName')
        t.string('username')
        t.string('email')
        t.string('password')
        t.string('image')
    },
})

// Queries
export const users = extendType({
    type: 'Query',
    definition(t) {
        t.nonNull.list.field('users', {
            type: nonNull(User),
            args: {
                // skip: intArg(),
                // limit: intArg(),
            },
            resolve: (_, args, ctx) => {
                return ctx.prisma.user.findMany({
                    // skip,
                    // ...(limit && { take: limit }),
                })
            },
        })
    },
})

export const user = extendType({
    type: 'Query',
    definition(t) {
        t.field('user', {
            type: nonNull(User),
            args: {
                id: nonNull(stringArg()),
            },
            resolve: (_, { id }, ctx) => {
                return ctx.prisma.user.findUnique({
                    where: { id },
                })
            },
        })
    },
})

// Mutations
export const createUser = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createUser', {
            type: nonNull(User),
            args: {
                input: nonNull(CreateUserInput),
            },
            resolve: async (_, { input }, ctx) => {
                if (input.password) {
                    input.password = await bcrypt.hashSync(
                        input.password,
                        Number(process.env.BCRYPT_SALT_ROUNDS) || 10,
                    )
                }
                input.email = input.email.trim().toLowerCase()

                // check if email already exists
                const userExists = await ctx.prisma.user.findUnique({
                    where: { email: input.email },
                } as any)
                if (userExists) {
                    throw new Error('Email already exists')
                }

                // check if username already exists
                const usernameExists = await ctx.prisma.user.findUnique({
                    where: { username: input.username },
                } as any)
                if (usernameExists) {
                    throw new Error('Username already exists')
                }

                // create user
                const user = await ctx.prisma.user.create({
                    data: input,
                })

                // send welcome email
                await sendEmail({
                    to: user.email,
                    subject: 'Welcome to uNotes 🫡',
                    html: render(
                        WelcomeTemplate({
                            username: user.username || user.name,
                        }),
                    ),
                })

                // create verification request
                const verificationRequest =
                    await ctx.prisma.verificationRequest.create({
                        data: {
                            identifier: user.id,
                            token:
                                Math.random().toString(36).substring(2, 15) +
                                Math.random().toString(36).substring(2, 15),
                            expires: new Date(Date.now() + 60 * 60 * 1000),
                        },
                    })
                await sendEmail({
                    to: user.email,
                    subject: 'Verify your email for uNotes',
                    html: render(
                        VerificationRequestTemplate({
                            token: verificationRequest.token,
                        }),
                    ),
                })
                return user
            },
        })
    },
})

export const updateUser = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('updateUser', {
            type: nonNull(User),
            args: {
                id: nonNull(stringArg()),
                input: UpdateUserInput,
            },
            resolve: async (_, { id, input }, ctx) => {
                if (input.password) {
                    input.password = await bcrypt.hashSync(
                        input.password,
                        Number(process.env.BCRYPT_SALT_ROUNDS) || 10,
                    )
                }
                // Notify User
                const user = await ctx.prisma.user.findUnique({
                    where: { id },
                })
                await notifyUser(
                    user,
                    `Profile updated`,
                    `Profile updated`,
                    null,
                    ctx.prisma,
                )

                return await ctx.prisma.user.update({
                    data: input,
                    where: { id },
                })
            },
        })
    },
})

export const deleteRequestedByUser = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('deleteRequestedByUser', {
            type: nonNull(User),
            args: {
                id: nonNull(stringArg()),
            },
            resolve: (_, { id }, ctx) => {
                return ctx.prisma.user.update({
                    where: { id },
                    data: {
                        deletedAt: new Date(),
                        status: 'DELETE_REQUESTED_BY_USER',
                    },
                })
            },
        })
    },
})

export const deleteUserForever = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('deleteUserForever', {
            type: nonNull(User),
            args: {
                email: nonNull(stringArg()),
            },
            resolve: async (_, { email }, ctx) => {
                const user = await ctx.prisma.user.findUnique({
                    where: { email },
                })
                if (!user) {
                    throw new Error('User not found')
                }

                return await ctx.prisma.user.delete({
                    where: { email },
                })
            },
        })
    },
})

// Password Reset
export const forgetPassword = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('forgetPassword', {
            type: User,
            args: {
                email: nonNull(stringArg()),
            },
            resolve: async (_, { email }, ctx) => {
                const user = await ctx.prisma.user.findUnique({
                    where: { email },
                })
                if (!user) {
                    throw new Error('User not found')
                }
                const resetToken =
                    Math.random().toString(36).substring(2, 15) +
                    Math.random().toString(36).substring(2, 15)
                await ctx.prisma.user.update({
                    where: { id: user.id },
                    data: {
                        resetToken,
                        resetTokenExpires: new Date(
                            Date.now() + 60 * 60 * 1000,
                        ),
                    },
                })

                // create password reset request

                await sendEmail({
                    to: user.email,
                    subject: 'Reset your password for uNotes',
                    html: render(
                        ResetPasswordRequestTemplate({ token: resetToken }),
                    ),
                })
                return user
            },
        })
    },
})

export const changePassword = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('changePassword', {
            type: User,
            args: {
                token: nonNull(stringArg()),
                password: nonNull(stringArg()),
            },
            resolve: async (_, { token, password }, ctx) => {
                const user = await ctx.prisma.user.findFirst({
                    where: { resetToken: token },
                })
                if (!user) {
                    throw new Error('User not found')
                }
                if (user.resetTokenExpires < new Date()) {
                    throw new Error('Token expired')
                }

                password = await bcrypt.hashSync(
                    password,
                    Number(process.env.BCRYPT_SALT_ROUNDS) || 10,
                )
                await sendEmail({
                    to: user.email,
                    subject: 'Your password has been changed',
                    html: render(
                        PasswordChangedTemplate({
                            username: user.username || user.name,
                        }),
                    ),
                })
                await ctx.prisma.notification.create({
                    data: {
                        body: 'Your password has been changed',
                        userId: user.id,
                    },
                })
                return await ctx.prisma.user.update({
                    where: { id: user.id },
                    data: {
                        password,
                        resetToken: null,
                        resetTokenExpires: null,
                    },
                })
            },
        })
    },
})

// Favorite Schools
export const addSchoolToFavorites = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('addSchoolToFavorites', {
            type: nonNull(User),
            args: {
                userId: nonNull(stringArg()),
                schoolId: nonNull(stringArg()),
            },
            resolve: async (_, { userId, schoolId }, ctx) => {
                const user = await ctx.prisma.user.findUnique({
                    where: { id: userId },
                })
                if (!user) {
                    throw new Error('User not found')
                }
                const school = await ctx.prisma.school.findUnique({
                    where: { id: schoolId },
                })
                if (!school) {
                    throw new Error('School not found')
                }
                return await ctx.prisma.user.update({
                    where: { id: userId },
                    data: {
                        favoriteSchools: {
                            connect: { id: schoolId },
                        },
                    },
                })
            },
        })
    },
})

export const removeSchoolFromFavorites = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('removeSchoolFromFavorites', {
            type: nonNull(User),
            args: {
                userId: nonNull(stringArg()),
                schoolId: nonNull(stringArg()),
            },
            resolve: async (_, { userId, schoolId }, ctx) => {
                const user = await ctx.prisma.user.findUnique({
                    where: { id: userId },
                })
                if (!user) {
                    throw new Error('User not found')
                }
                const school = await ctx.prisma.school.findUnique({
                    where: { id: schoolId },
                })
                if (!school) {
                    throw new Error('School not found')
                }
                return await ctx.prisma.user.update({
                    where: { id: userId },
                    data: {
                        favoriteSchools: {
                            disconnect: { id: schoolId },
                        },
                    },
                })
            },
        })
    },
})

// Favorite courses
export const addCourseToFavorites = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('addCourseToFavorites', {
            type: nonNull(User),
            args: {
                userId: nonNull(stringArg()),
                courseId: nonNull(stringArg()),
            },
            resolve: async (_, { userId, courseId }, ctx) => {
                const user = await ctx.prisma.user.findUnique({
                    where: { id: userId },
                })
                if (!user) {
                    throw new Error('User not found')
                }
                const course = await ctx.prisma.course.findUnique({
                    where: { id: courseId },
                })
                if (!course) {
                    throw new Error('Course not found')
                }
                return await ctx.prisma.user.update({
                    where: { id: userId },
                    data: {
                        favoriteCourses: {
                            connect: { id: courseId },
                        },
                    },
                })
            },
        })
    },
})

export const removeCourseFromFavorites = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('removeCourseFromFavorites', {
            type: nonNull(User),
            args: {
                userId: nonNull(stringArg()),
                courseId: nonNull(stringArg()),
            },
            resolve: async (_, { userId, courseId }, ctx) => {
                const user = await ctx.prisma.user.findUnique({
                    where: { id: userId },
                })
                if (!user) {
                    throw new Error('User not found')
                }
                const course = await ctx.prisma.course.findUnique({
                    where: { id: courseId },
                })
                if (!course) {
                    throw new Error('Course not found')
                }
                return await ctx.prisma.user.update({
                    where: { id: userId },
                    data: {
                        favoriteCourses: {
                            disconnect: { id: courseId },
                        },
                    },
                })
            },
        })
    },
})

// Notification Settings
export const removeNotificationSetting = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('removeNotificationSetting', {
            type: nonNull(User),
            args: {
                userId: nonNull(stringArg()),
                notificationSettingId: nonNull(stringArg()),
            },
            resolve: async (_, { userId, notificationSettingId }, ctx) => {
                const user = await ctx.prisma.user.findUnique({
                    where: { id: userId },
                })
                if (!user) {
                    throw new Error('User not found')
                }
                const setting =
                    await ctx.prisma.notificationSettings.findUnique({
                        where: { id: notificationSettingId },
                    })
                if (!setting) {
                    throw new Error('Setting not found')
                }
                return await ctx.prisma.user.update({
                    where: { id: userId },
                    data: {
                        notificationSettings: {
                            disconnect: { id: notificationSettingId },
                        },
                    },
                })
            },
        })
    },
})

export const addNotificationSetting = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('addNotificationSetting', {
            type: nonNull(User),
            args: {
                userId: nonNull(stringArg()),
                notificationSettingId: nonNull(stringArg()),
            },
            resolve: async (_, { userId, notificationSettingId }, ctx) => {
                const user = await ctx.prisma.user.findUnique({
                    where: { id: userId },
                })
                if (!user) {
                    throw new Error('User not found')
                }
                const setting =
                    await ctx.prisma.notificationSettings.findUnique({
                        where: { id: notificationSettingId },
                    })
                if (!setting) {
                    throw new Error('Setting not found')
                }
                return await ctx.prisma.user.update({
                    where: { id: userId },
                    data: {
                        notificationSettings: {
                            connect: { id: notificationSettingId },
                        },
                    },
                })
            },
        })
    },
})
