import { extendType, nonNull, stringArg } from 'nexus'
import { User } from '@/graphql/typeDefs/User'
import prisma from '@/lib/prisma'
import { sendEmail } from '@/lib/email'
import { render } from '@react-email/render'
import VerificationRequestTemplate from '../../../emails/VerificationRequestTemplate'

export const sendVerificationRequest = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('sendVerificationRequest', {
            type: User,
            args: {
                email: nonNull(stringArg()),
            },
            resolve: async (_, { email }, ctx) => {
                const user = await ctx.prisma.user.findUnique({
                    where: { email },
                })
                if (!user) {
                    throw new Error('No user found with that email')
                }
                const token =
                    Math.random().toString(36).substring(2, 15) +
                    Math.random().toString(36).substring(2, 15)
                const accountVerification =
                    await prisma.verificationRequest.update({
                        where: {
                            identifier: user.id,
                        },
                        data: {
                            token,
                            expires: new Date(Date.now() + 60 * 60 * 1000),
                        },
                    })
                if (!accountVerification) {
                    throw new Error('User account is already verified')
                }
                await sendEmail({
                    to: user.email,
                    subject: 'Verify your email for uNotes',
                    html: render(VerificationRequestTemplate({ token })),
                })
                return user
            },
        })
    },
})
