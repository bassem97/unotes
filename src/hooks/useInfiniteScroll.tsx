import { useEffect } from 'react'
import { useQuery } from '@apollo/client'
import { useInView } from 'react-intersection-observer'
import { Course, School, Notification } from '@prisma/client'
import { TemporaryDocumentType } from '@/types/TemporaryDocumentType'

type Params<T> = {
    query: any
    queryVariables?: any
    items: T[]
    setItems: (items: T[]) => void
    skip: number
    limit: number
}

export const useInfiniteScroll = <
    T extends School | Document | Course | Notification | TemporaryDocumentType,
>({
    query,
    queryVariables,
    items,
    setItems,
    skip,
    limit,
}: Params<T>) => {
    const { ref, inView } = useInView()

    const { networkStatus, error, fetchMore, loading } = useQuery(query, {
        notifyOnNetworkStatusChange: true,
        variables: {
            ...queryVariables,
            skip,
            limit,
        },
        onCompleted: data => {
            setItems(Object.values(data)[0] as T[])
        },
        onError: error => {
            console.error(error)
        },
    })

    useEffect(() => {
        const loadMore = async () => {
            const result = await fetchMore({
                variables: {
                    skip: items.length,
                    limit,
                },
            })
            const resultData = Object.values(result.data)[0] as T[]
            setItems([...items, ...resultData])
        }
        inView && loadMore()
    }, [inView])

    return {
        networkStatus,
        error,
        ref,
        loading,
        fetchMore,
    }
}
