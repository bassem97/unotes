import io from 'socket.io-client'
import { useEffect, useState } from 'react'

let socket: any

type Message = {
    author?: string
    action?: string
    message: string
    data?: any
}

export const useSocket = () => {
    const [message, setMessage] = useState({} as Message)

    useEffect(() => {
        socketInitializer()
    }, [])
    const socketInitializer = async () => {
        // We just call it because we don't need anything else out of it
        await fetch('/api/socket')

        socket = io()

        socket.on('newIncomingMessage', (msg: Message) => {
            setMessage(msg)
        })
    }

    return { message, socket }
}
