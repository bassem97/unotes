const Footer = () => {
    return (
        <div className="flex justify-center items-center text-xs text-white fixed left-0 bottom-[2px] w-full md:min-h-fit md:h-8 md:text-primary sm:hidden">
            Copyright © <span>{new Date().getFullYear()}</span>
            <span className="ml-1">- UNOTES</span>.
        </div>
    )
}

export default Footer
