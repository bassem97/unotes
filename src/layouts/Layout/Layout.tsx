import { ReactNode, useEffect, useState } from 'react'
import Footer from '../Footer/Footer'
import Navbar from '../Navbar/Navbar'
import Sidebar from '../Sidebar/Sidebar'
import { useSession } from 'next-auth/react'
import Image from 'next/image'
import { BiHomeHeart } from 'react-icons/bi'
import { SlGlobeAlt } from 'react-icons/sl'
import { AiOutlineStar, AiOutlineUpload } from 'react-icons/ai'
import { FiSettings, FiFileText } from 'react-icons/fi'

type Props = {
    children: ReactNode
}

const Layout = ({ children }: Props) => {
    const { data: session } = useSession()
    const [isDarkMode, setDarkMode] = useState<boolean>(true)

    const handleThemeChange = () => {
        localStorage.setItem('theme', isDarkMode ? 'light' : 'dark')
        setDarkMode(prevState => !prevState)
    }

    useEffect(() => {
        setDarkMode(
            !!(
                localStorage.getItem('theme') &&
                localStorage.getItem('theme') === 'dark'
            ),
        )
    }, [])

    const sidebarItems = [
        {
            route: '/',
            icon: <BiHomeHeart className="text-xl" />,
            label: 'Home',
            classname: 'animate-[slide_ease-out_700ms]',
        },
        {
            route: '/schools',
            icon: <SlGlobeAlt className="text-xl" />,
            label: 'Explore',
            classname: 'animate-[slide_ease-out_750ms]',
        },
        {
            route: '/favorites',
            icon: <AiOutlineStar className="text-xl" />,
            label: 'Favorites',
            classname: 'animate-[slide_ease-out_800ms]',
        },
        {
            route: '/documents/new',
            icon: <AiOutlineUpload className="text-xl" />,
            label: 'Add',
            classname: 'animate-[slide_ease-out_850ms]',
        },
        // Add the user documents and settings item only when the user is authenticated
        ...(session?.user
            ? [
                  {
                      route: '/documents',
                      icon: <FiFileText className="text-xl" />,
                      label: 'My Documents',
                      classname: 'animate-[slide_ease-out_900ms]',
                  },
                  {
                      route: '/settings',
                      icon: <FiSettings className="text-xl" />,
                      label: 'Account Settings',
                      classname: 'animate-[slide_ease-out_950ms]',
                  },
              ]
            : []),
    ]

    return (
        <div className={`h-full w-full ${isDarkMode ? 'dark' : 'light'}`}>
            <div
                className={
                    'bg-primary dark:bg-primarysec dark:bg-opacity-90 px-1 pt-1 pb-5 h-full md:p-0 flex flex-col md:h-full'
                }
            >
                <div className="flex w-full h-full bg-bg dark:bg-darkbg rounded-3xl max-w-[1920px] mx-auto relative md:rounded-none">
                    <Sidebar sidebarItems={sidebarItems} />
                    <div className="flex flex-col w-full max-w-full overflow-hidden">
                        <Navbar
                            sidebarItems={sidebarItems}
                            isDarkMode={isDarkMode}
                            handleThemeChange={handleThemeChange}
                        />
                        <div
                            className={
                                'h-full background relative flex flex-col mx-4 mb-2 ml-0 rounded-3xl dark:border-bg border-2 border-gray-100 bg-bg2 bg-opacity-20 dark:bg-opacity-5 overflow-y-hidden lg:mr-1 md:px-0 md:py-0 md:mb-0 md:mx-2 xs:mx-2 md:gap-2'
                            }
                        >
                            <div className="h-full flex flex-col z-10 bg-bg dark:bg-darkfont">
                                {children}
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        </div>
    )
}

export default Layout
