import { useEffect, useState } from 'react'
import { SlBadge } from 'react-icons/sl'
import { RxAvatar, RxCross2, RxHamburgerMenu } from 'react-icons/rx'
import Link from 'next/link'
import Image from 'next/image'
import { signOut, useSession } from 'next-auth/react'
import { Session } from 'next-auth'
import { useRouter } from 'next/router'
import { useWindowSize } from '@/hooks/useWindowSize'
import {
    ThemeToggler,
    Notifications,
    Search,
    Button,
    SidebarItem,
} from '@/components'
import { gql, useQuery } from '@apollo/client'
import { FiLogOut } from 'react-icons/fi'
import AvatarImage from '@/components/AvatarImage'

interface Props {
    handleThemeChange: () => void
    isDarkMode: boolean
    sidebarItems: {
        route: string
        icon: JSX.Element
        label: string
        classname?: string
    }[]
}

const Navbar = ({ handleThemeChange, isDarkMode, sidebarItems }: Props) => {
    const width = useWindowSize()
    const [open, setOpen] = useState(!!(width && width > 767))
    const { data } = useSession() as unknown as { data: Session }

    const { data: imageData } = useQuery(
        gql`
            query user($id: String!) {
                user(id: $id) {
                    id
                    image
                }
            }
        `,
        {
            variables: {
                id: data?.user?.id || '',
            },
        },
    )

    useEffect(() => {
        setOpen(!!(width && width > 767))
    }, [width])
    const router = useRouter()
    return (
        <nav className="w-full pr-6 pl-0 py-3 flex justify-between items-center gap-12 lg:gap-4 md:py-2 md:pr-3 md:gap-6 md:pl-14">
            <div className="mx-auto w-full ">
                <Search
                    placeholder="Search for Index, Courses and Documents... "
                    onSearch={value => {
                        if (
                            ['?', '/', '?', ':', ';', '&', '='].some(char =>
                                value.includes(char),
                            )
                        )
                            return
                        router.push(`/search/${value}`)
                    }}
                />
            </div>
            <div className="text-3xl hidden md:flex md:gap-4">
                {data?.user?.id && <Notifications />}
                <RxHamburgerMenu
                    className="cursor-pointer dark:text-white"
                    onClick={() => setOpen(() => true)}
                />
            </div>
            {open && (
                <div
                    className={
                        'flex gap-4 items-center 2xl:gap-4 min-w-fit ' +
                        'md:absolute md:py-6 md:z-30 md:px-4 md:right-0 md:top-0 md:flex-col-reverse md:bg-bg dark:md:bg-darkbg md:h-screen md:justify-end md:w-full'
                    }
                >
                    <div className="hidden md:flex md:absolute md:right-4 md:top-4">
                        <RxCross2
                            className="text-3xl cursor-pointer"
                            onClick={() => setOpen(() => false)}
                        />
                    </div>
                    <div className="flex gap-6 md:flex-col items-center text-font2 w-full">
                        {data?.user?.id ? (
                            <span className="md:hidden">
                                <Notifications />
                            </span>
                        ) : (
                            <div className="flex items-center gap-2">
                                <Link href="/login">
                                    <Button label="Login" />
                                </Link>
                                /
                                <Link href="/signup">
                                    <Button label="sign up" />
                                </Link>
                            </div>
                        )}
                        <ThemeToggler
                            isDarkMode={isDarkMode}
                            handleThemeChange={handleThemeChange}
                        />
                        <div
                            className={
                                'hidden md:flex flex-col gap-4 md:items-center'
                            }
                        >
                            {sidebarItems.map((item, index) => (
                                <SidebarItem
                                    isSideBarOpen={open}
                                    key={index}
                                    route={item.route}
                                    icon={item.icon}
                                    label={item.label}
                                    classname={item.classname}
                                    handleClick={() => setOpen(() => false)}
                                />
                            ))}
                            {data?.user?.id && (
                                <SidebarItem
                                    isSideBarOpen={open}
                                    classname={'animate-[slide_ease-out_950ms]'}
                                    route="/api/auth/signout"
                                    icon={<FiLogOut className="text-xl" />}
                                    label="Logout"
                                    handleClick={e => {
                                        e.preventDefault()
                                        signOut()
                                    }}
                                />
                            )}
                        </div>
                    </div>

                    <div className="flex gap-4 items-center md:flex-col-reverse">
                        <div className="flex flex-col items-end gap-1 lg:gap-0 md:items-center">
                            <h1 className="capitalize whitespace-nowrap dark:text-bg lg:text-sm">
                                {(data?.user?.name || data?.user?.username) ??
                                    'Guest'}
                            </h1>
                            <div className="text-font3 dark:text-bg2 flex items-center gap-1 ">
                                <h2
                                    title={
                                        data?.user?.id
                                            ? 'Hello cutie :)'
                                            : '“The oldest and strongest emotion of mankind is fear, and the oldest and strongest kind of fear is fear of the unknown”\n-H.P. Lovecraft'
                                    }
                                    className="capitalize text-sm lg:text-xs dark:text-bg2 text-font3 font-poppins font-bold whitespace-nowrap"
                                >
                                    {data?.user?.id ? 'Basic' : 'Unknown'}{' '}
                                    Member
                                </h2>
                                <SlBadge />
                            </div>
                        </div>
                        {data?.user?.id ? (
                            <AvatarImage imageUrl={imageData?.user?.image} />
                        ) : (
                            <RxAvatar className="rounded-full w-12 h-12 flex object-contain border " />
                        )}
                    </div>
                </div>
            )}
        </nav>
    )
}

export default Navbar
