import { FiLogOut } from 'react-icons/fi'
import Link from 'next/link'
import Image from 'next/image'
import { signOut, useSession } from 'next-auth/react'
import { Session } from 'next-auth'
import { SidebarItem } from '@/components'
import { IoIosArrowDropleftCircle } from 'react-icons/io'
import { useState } from 'react'

interface Props {
    sidebarItems: {
        route: string
        icon: JSX.Element
        label: string
        classname?: string
    }[]
}

const Sidebar = ({ sidebarItems }: Props) => {
    const { data } = useSession() as unknown as { data: Session }
    const [open, setOpen] = useState(true)

    return (
        <div
            className={
                'transition-all px-5 pt-2 pb-2 h-full flex flex-col' +
                ' lg:px-2 md:flex-col md:px-0 md:fixed md:z-10 md:left-0 md:pt-2 md:pb-8 ' +
                (open
                    ? 'w-72 xl:w-64 lg:w-50 md:w-32 sm:w-14'
                    : 'w-28 lg:w-24 md:w-14')
            }
        >
            <Link
                href={'/'}
                className={
                    'relative max-w-full w-72 md:w-32 sm:w-32 mb-4 h-12 min-h-[3rem] md:min-h-fit object-contain md:px-2 md:mb-2 flex items-center md:h-8 ' +
                    (open ? 'px-5' : '')
                }
            >
                <Image
                    src={'/logo.png'}
                    alt="logo"
                    height={'17'}
                    width={'200'}
                    priority
                    className="md:hidden w-auto h-auto"
                />
                <Image
                    src={'/logo-mobile.png'}
                    alt="logo"
                    height={'48'}
                    width={'48'}
                    priority
                    className="hidden md:block"
                />
            </Link>
            <div
                className={
                    'border-2 border-gray-100 rounded-3xl px-4 pt-8 pb-4 h-full flex relative flex-col justify-between md:hidden ' +
                    ' md:mx-0 md:border-0 md:pt-2 md:px-2 md:pb-2 md:flex-row md:h-fit md:w-screen '
                }
            >
                <div className="absolute top-0 left-[calc(100%-1rem)] flex items-center h-full md:hidden">
                    <IoIosArrowDropleftCircle
                        onClick={() => setOpen(prev => !prev)}
                        className={
                            'text-3xl text-primary dark:text-primarysec dark:rounded-full dark:bg-white cursor-pointer transition-all ' +
                            (open ? '' : 'transform rotate-180')
                        }
                    />
                </div>

                <div
                    className={
                        'flex flex-col gap-4 md:items-center md:w-fit ' +
                        'md:flex-row'
                    }
                >
                    {sidebarItems.map((item, index) => (
                        <SidebarItem
                            isSideBarOpen={open}
                            key={index}
                            route={item.route}
                            icon={item.icon}
                            label={item.label}
                            classname={item.classname}
                        />
                    ))}
                </div>
                {data?.user && (
                    <SidebarItem
                        isSideBarOpen={open}
                        route="/api/auth/signout"
                        icon={<FiLogOut className="text-xl" />}
                        label="Logout"
                        handleClick={e => {
                            e.preventDefault()
                            signOut()
                        }}
                    />
                )}
            </div>
        </div>
    )
}

export default Sidebar
