import { User } from '.prisma/client'
import { PrismaClient } from '@prisma/client'
import { sendEmail } from '@/lib/email'
import { render } from '@react-email/render'
import ActionTemplate from '../../emails/ActionTemplate'

export const notifyUser = async (
    user: User,
    subject: string,
    body: string,
    link: string | null,
    prisma: PrismaClient,
) => {
    try {
        await prisma.notification.create({
            data: {
                body,
                userId: user.id,
                link,
            },
        })
        await sendEmail({
            to: user.email!,
            subject,
            html: render(
                ActionTemplate({
                    username: user.username || user.name!,
                    body,
                    link,
                }),
            ),
        })
    } catch (e) {
        console.error(e)
    }
}
