import { Socket } from 'socket.io'

const messageHandler = (io: any, socket: Socket) => {
    const createdMessage = (msg: string) => {
        socket.broadcast.emit('newIncomingMessage', msg)
    }

    socket.on('createdMessage', createdMessage)
}

export default messageHandler
