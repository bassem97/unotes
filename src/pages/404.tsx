import Link from 'next/link'
import { useRouter } from 'next/router'
import Head from 'next/head'
import Header from '@/components/Header'

const NotFound = () => {
    const router = useRouter()
    return (
        <>
            <Header description={'404 Not found'} />
            <div className="w-full gap-6 h-full flex-col flex justify-center items-center">
                <div>
                    <h1 className="text-[70px] font-bold text-center">404</h1>
                    <h2 className="text-[50px] text-center">NOT FOUND</h2>
                    {/*// 2 buttons  that can them to create new schools / courses or documents*/}
                    <div className="flex flex-row gap-4 justify-center items-center">
                        <button
                            className="bg-primary hover:bg-opacity-80 text-white font-bold py-2 px-4 rounded"
                            onClick={() => router.push('/schools/new')}
                        >
                            Create new school
                        </button>
                        <button
                            className="bg-primary hover:bg-opacity-80 text-white font-bold py-2 px-4 rounded"
                            onClick={() => router.push('/courses/new')}
                        >
                            Create new course
                        </button>
                    </div>
                </div>
            </div>
        </>
    )
}

export default NotFound
