import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { SessionProvider } from 'next-auth/react'
import { ApolloProvider } from '@apollo/client'
import { client } from '@/lib/apollo'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import Image from 'next/image'

const Layout = dynamic(() => import('@/layouts/Layout/Layout'), {
    ssr: false,
    loading: () => (
        <div className="flex justify-center items-center h-full w-full">
            <div className="h-32 w-32 relative">
                <Image
                    className="object-cover"
                    src="/spin.gif"
                    priority
                    fill
                    alt=""
                />
            </div>
        </div>
    ),
})

export default function App({
    Component,
    pageProps: { session, ...pageProps },
}: AppProps) {
    const { pathname } = useRouter()
    const pagesLayoutless = [
        '/login',
        '/404',
        '/signup',
        '/verification/[email]',
        '/reset-password/emailform',
        '/reset-password/[token]',
        '/tempUpload',
    ]
    return (
        <>
            <ApolloProvider client={client}>
                <SessionProvider session={session}>
                    {pagesLayoutless && pagesLayoutless.includes(pathname) ? (
                        <Component {...pageProps} />
                    ) : (
                        <Layout>
                            <Component {...pageProps} />
                        </Layout>
                    )}
                </SessionProvider>
            </ApolloProvider>
        </>
    )
}
