import React, { useEffect, useState } from 'react'
import Image from 'next/image'
import Header from '@/components/Header'

const Maintenance = () => {
    const [isDarkMode, setIsDarkMode] = useState<string>(
        localStorage.getItem('theme') === 'dark' ? 'true' : 'false',
    )

    useEffect(() => {
        setIsDarkMode(
            localStorage.getItem('theme') === 'dark' ? 'true' : 'false',
        )
    }, [])

    console.log(isDarkMode)

    return (
        <>
            <Header description={'Maintenance page'} />
            <div className="flex flex-col items-center justify-center h-screen dark:bg-gray-900 bg-gray-100">
                <div className="flex flex-col items-center justify-center h-screen">
                    <Image
                        src={
                            isDarkMode === 'true'
                                ? '/maintenance-light.svg'
                                : '/maintenance-dark.svg'
                        }
                        alt="uNotes Logo"
                        width={200}
                        height={200}
                    />
                    <h1 className="text-4xl font-bold dark:text-white text-gray-700 mb-2">
                        Maintenance Mode
                    </h1>
                    <p className="dark:text-white text-gray-700 text-center mb-8">
                        We are currently performing scheduled maintenance.
                        Please check back later.
                    </p>
                    <button
                        className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-3 px-6 rounded"
                        onClick={() => window.location.reload()}
                    >
                        Refresh
                    </button>
                </div>
            </div>
        </>
    )
}

export default Maintenance
