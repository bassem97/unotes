import { NextApiRequest, NextApiResponse } from 'next'
import prisma from '@/lib/prisma'

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse,
) {
    const { token } = req.query as { token: string }
    await prisma.verificationRequest.delete({
        where: {
            token: token,
        },
    })
    res.redirect('/login')
}
