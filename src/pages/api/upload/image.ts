import type { NextApiRequest, NextApiResponse } from 'next'
import nextConnect from 'next-connect'
import multer from 'multer'
import { S3 } from '@aws-sdk/client-s3'

interface ExtendedNextApiRequest extends NextApiRequest {
    files: {
        fieldname: string
        originalname: string
        encoding: string
        mimetype: string
        buffer: Buffer
        size: number
    }[]
}

const handler = nextConnect<ExtendedNextApiRequest, NextApiResponse>({
    onError(error, _req, res) {
        res.status(501).json({
            error: `Sorry something Happened! ${error.message}`,
        })
    },
    onNoMatch(req, res) {
        res.status(405).json({ error: `Method "${req.method}" Not Allowed` })
    },
})

handler.use(multer().any())

const {
    NEXT_PUBLIC_UNOTES_SPACE_REGION: region,
    NEXT_PUBLIC_UNOTES_SPACE_ENDPOINT: endpoint,
    NEXT_PUBLIC_UNOTES_SPACE_KEY: accessKeyId = '',
    NEXT_PUBLIC_UNOTES_SPACE_SECRET: secretAccessKey = '',
} = process.env

handler.post(async (req, res) => {
    const client = new S3({
        region,
        endpoint,
        credentials: {
            accessKeyId,
            secretAccessKey,
        },
    })

    const { files } = req
    const image = files[0]

    const key = `${Date.now()}__${image.originalname}`
    client.putObject(
        {
            Bucket: process.env.NEXT_PUBLIC_UNOTES_IMAGES_SPACE || '',
            Key: key,
            Body: image.buffer,
            ContentType: image.mimetype,
            ACL: 'public-read',
        },
        (err, _data) => {
            if (err) return res.status(500).json({ error: err.message })

            res.status(200).json({
                response: 'Image uploaded successfully.',
                key,
            })
        },
    )

    res.status(200).json({ response: 'Image uploaded successfully.', key })
})

export const config = {
    api: {
        bodyParser: false,
    },
}

export default handler
