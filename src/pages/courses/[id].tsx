import prisma from '@/lib/prisma'
import { Course, DocumentType } from '@prisma/client'
import { GetServerSideProps, InferGetServerSidePropsType } from 'next'
import { getSession } from 'next-auth/react'
import { Key, useEffect, useState } from 'react'
import { SwitchInput } from '@/components'
import { Session } from 'next-auth'
import Head from 'next/head'
import { capitalizeFirst } from '@/lib/capitalizeFirst'
import SortingDisplayNew, { Option } from '@/components/SortingDisplayNew'
import DocumentCard from '@/components/Document/DocumentCard'
import { TemporaryDocumentType } from '@/types/TemporaryDocumentType'
import { gql, NetworkStatus, useQuery } from '@apollo/client'
import { useInfiniteScroll } from '@/hooks/useInfiniteScroll'
import BaseSpinner from '@/components/BaseSpinner'
import Header from '@/components/Header'
import { useRouter } from 'next/router'
import { useDebounce } from '@/hooks/useDebounce'

const GET_DOCUMENTS = gql`
    query DocumentsBySchoolID(
        $courseId: String!
        $skip: Int!
        $limit: Int!
        $verified: Boolean
        $name: String
    ) {
        DocumentsBySchoolID(
            courseId: $courseId
            skip: $skip
            limit: $limit
            verified: $verified
            name: $name
        ) {
            id
            name
            season
            year
            type
            verified
        }
    }
`
type Props = {
    course: Course
    documents: TemporaryDocumentType[]
}
const CourseID = ({ course }: Props) => {
    const router = useRouter()
    const [documents, setDocuments] = useState<TemporaryDocumentType[]>([])
    const [docType, setDocType] = useState<string>('')

    const [query, setQuery] = useState<string>('')
    const searchQuery = useDebounce<string>(query, 500)

    const [filter, setFilter] = useState<{
        verified: boolean
    }>({ verified: false })

    const { id, name } = router.query
    useEffect(() => {
        router.push({
            pathname: router.pathname,
            query: {
                id: id,
                ...(searchQuery && { name: searchQuery }),
            },
        })
    }, [searchQuery])

    const { ref, networkStatus, error } =
        useInfiniteScroll<TemporaryDocumentType>({
            query: GET_DOCUMENTS,
            queryVariables: {
                courseId: course.id,
                ...(filter.verified && { verified: true }),
                ...(name && { name: name as string }),
            },
            items: documents,
            setItems: setDocuments,
            skip: 0,
            limit: 10,
        })

    if (error) {
        console.log(error)
    }

    return (
        <>
            <Header
                description={capitalizeFirst(course.name) + ' documents page'}
            />

            <div className="flex flex-col items-center mx-8 mt-6 mb-8 min-h-fit md:mx-4 md:mt-4 md:mb-2 sm:mx-2">
                <SortingDisplayNew
                    setFilter={setDocType}
                    filter={docType}
                    query={query}
                    setQuery={setQuery}
                    options={Object.values(DocumentType).map(type => ({
                        type,
                        count: documents.filter(doc => doc.type === type)
                            .length,
                    }))}
                />
                <div className="flex justify-end mt-4">
                    <SwitchInput
                        label={'Hide Unverified'}
                        checked={filter.verified}
                        onChange={() =>
                            setFilter(prev => ({
                                ...prev,
                                verified: !filter.verified,
                            }))
                        }
                    />
                </div>
            </div>
            <div
                className={
                    'scroller flex flex-wrap gap-4 content-start overflow-y-auto overflow-x-hidden h-full p-8 ' +
                    ' lg:px-2 md:mx-4 sm:mx-2'
                }
            >
                {documents
                    .filter(doc => !docType || docType === (doc.type as string))
                    .filter(doc => !filter.verified || doc.verified)
                    .map(document => (
                        <DocumentCard key={document.id} document={document} />
                    ))}
                <div ref={ref} />
            </div>
        </>
    )
}

export default CourseID

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null

    if (!context.params?.id)
        return { redirect: { destination: '/', permanent: false } }

    const id = context.params.id.toString()

    const course = await prisma.course.findUnique({
        where: {
            id: id,
        },
        include: {
            documents: {
                where: {
                    deleted: false,
                },
                include: {
                    tags: true,
                },
            },
        },
    })

    if (!course) return { redirect: { destination: '/404', permanent: false } }
    const documents = course.documents

    return {
        props: {
            session,
            course: JSON.parse(JSON.stringify(course)),
            documents: JSON.parse(JSON.stringify(documents)),
        },
    }
}
