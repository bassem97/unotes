import { Course, School, User } from '@prisma/client'
import { useEffect, useState } from 'react'
import CourseForm from '@/components/Course/CourseForm'
import { GetServerSideProps } from 'next'
import { getSession } from 'next-auth/react'
import prisma from '@/lib/prisma'
import { useRouter } from 'next/router'
import { Session } from 'next-auth'
import Head from 'next/head'
import { capitalizeFirst } from '@/lib/capitalizeFirst'
import Header from '@/components/Header'

type Props = {
    course: Course
    schools: Array<School>
    users: Array<User>
}

const Id = ({ course, schools, users }: Props) => {
    const [result, setResult] = useState('')
    const [redirectTo, setRedirectTo] = useState('')
    const router = useRouter()

    useEffect(() => {
        const redirect = async () => {
            if (redirectTo !== '') await router.push(redirectTo)
        }
        redirect()
    }, [redirectTo])

    return (
        <>
            <Header description={capitalizeFirst(course.name)} />
            <div className="flex justify-center h-full items-center">
                <div className="bg-bg content-start dark:bg-darkbg scroller2 px-8 pt-8 pb-12 flex flex-wrap gap-4 w-full h-full min-h-full overflow-y-auto">
                    <h1 className="w-full h-fit text-2xl pointer-events-none dark:text-bg font-bold mb-2 text-center">
                        Update Course
                    </h1>
                    <div className="flex justify-center">
                        <h1 className="text-primary text-lg text-center">
                            {result}
                        </h1>
                    </div>
                    <CourseForm
                        course={course}
                        schools={schools}
                        users={users}
                        setResult={setResult}
                        setRedirectTo={setRedirectTo}
                    />
                </div>
            </div>
        </>
    )
}

export default Id

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null
    if (!session)
        return {
            redirect: {
                destination: `/login?callbackUrl=${context.resolvedUrl}`,
                permanent: false,
            },
        }

    if (!context.params?.id)
        return { redirect: { destination: '/', permanent: false } }

    const id = context.params.id.toString()
    const course = await prisma.course.findUnique({
        where: {
            id: id,
        },
        include: {
            moderators: true,
        },
    })
    if (!course) return { redirect: { destination: '/404', permanent: false } }

    const userId = session.user?.id

    // Check if user can edit the course
    if (!course.moderators.find((moderator: User) => moderator.id === userId))
        return { redirect: { destination: '/404', permanent: false } }

    const schools = await prisma.school.findMany({
        where: {
            verified: true,
            deleted: false,
        },
    })
    const users = JSON.parse(JSON.stringify(await prisma.user.findMany()))
    return {
        props: {
            course: JSON.parse(JSON.stringify(course)),
            schools: JSON.parse(JSON.stringify(schools)),
            users,
        },
    }
}
