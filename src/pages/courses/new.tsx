import prisma from '@/lib/prisma'
import { GetServerSideProps } from 'next'
import { useEffect, useState } from 'react'
import { getSession } from 'next-auth/react'
import { School, User } from '@prisma/client'
import CourseForm from '@/components/Course/CourseForm'
import { useRouter } from 'next/router'
import { Session } from 'next-auth'
import Head from 'next/head'
import Header from '@/components/Header'

type Props = {
    schools: Array<School>
    users: Array<User>
}

const New = ({ schools, users }: Props) => {
    const router = useRouter()
    const { callBackUrl } = router.query

    const [result, setResult] = useState('')
    const [redirectTo, setRedirectTo] = useState('')

    useEffect(() => {
        redirectTo &&
            (callBackUrl
                ? router.push(callBackUrl as string)
                : router.push(redirectTo))
    }, [redirectTo])

    return (
        <>
            <Header description={'New Course'} />

            <div className="flex justify-center h-full items-center w-full bg-bg ">
                <div className="bg-bg content-start dark:bg-darkbg scroller2 px-8 pt-8 pb-12 flex flex-wrap gap-4 w-full h-full min-h-full overflow-y-auto">
                    <h1 className="w-full h-fit text-2xl pointer-events-none dark:text-bg font-bold mb-2 text-center">
                        New Course
                    </h1>
                    {result && (
                        <div className="flex justify-center">
                            <h1 className="text-primary text-lg text-center">
                                {result}
                            </h1>
                        </div>
                    )}
                    <CourseForm
                        schools={schools}
                        users={users}
                        setResult={setResult}
                        setRedirectTo={setRedirectTo}
                    />
                </div>
            </div>
        </>
    )
}

export default New

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null
    if (!session)
        return {
            redirect: {
                destination: `/login?callbackUrl=${context.resolvedUrl}`,
                permanent: false,
            },
        }

    const schools = await prisma.school.findMany({
        where: {
            verified: true,
            deleted: false,
        },
    })
    const users = JSON.parse(JSON.stringify(await prisma.user.findMany()))

    return {
        props: {
            schools: JSON.parse(JSON.stringify(schools)),
            users: users,
        },
    }
}
