import { Comment, Course, Document, User } from '@prisma/client'
import { GetServerSideProps } from 'next'
import prisma from '@/lib/prisma'
import * as Yup from 'yup'
import { gql, useMutation } from '@apollo/client'
import { useFormik } from 'formik'
import { useSession } from 'next-auth/react'
import { Session } from 'next-auth'
import BaseSpinner from '@/components/BaseSpinner'
import { useEffect, useState } from 'react'
import { useSocket } from '@/hooks/useSocket'
import { useRouter } from 'next/router'
import PDFViewer from '@/components/Document/PDFViewer'
import { TemporaryFileDocumentType } from '@/types/TemporaryFileDocumentType'
import { IDocument } from '@cyntler/react-doc-viewer'
import CommentPost from '@/components/Comment/CommentPost'
import CommentsList from '@/components/Comment/CommentsList'
import Header from '@/components/Header'
import DocumentDetails from '@/components/Document/DocumentDetails'

type Props = {
    documentModel: Document & { files: TemporaryFileDocumentType[] }
    filedocs: IDocument[]
    course: Course
}

const validationSchema = Yup.object().shape({
    body: Yup.string().required('Required'),
})

const createCommentMutation = gql`
    mutation createCommentMutation($input: CreateCommentInput!) {
        createComment(input: $input) {
            id
            body
            createdAt
            user {
                id
                name
                username
                image
            }
            document {
                id
            }
        }
    }
`

const deleteDocumentMutation = gql`
    mutation deleteDocumentMutation($id: String!) {
        deleteDocument(id: $id) {
            id
        }
    }
`

const DocumentId = ({ documentModel, course }: Props) => {
    const [fileDocs, setFileDocs] = useState<IDocument[]>([])

    console.log(documentModel)

    const [comments, setComments] = useState<Comment[]>(
        (documentModel as Document as any).comments,
    )

    const { socket } = useSocket()
    const router = useRouter()

    const { data: userData } = useSession() as { data: Session }

    useEffect(() => {
        const fetchDocumentFiles = async () => {
            const filePromises = documentModel.files.map(async file => {
                const response = await fetch(file.url)
                const extension = file.url.split('.').pop()
                const arrayBuffer = await response.arrayBuffer()

                return {
                    uri: file.url,
                    fileData: arrayBuffer,
                    fileType: extension,
                }
            })
            const filedocs = await Promise.all(filePromises)
            setFileDocs(filedocs)
        }

        fetchDocumentFiles()
    }, [])

    const [createComment] = useMutation(createCommentMutation, {
        onCompleted: async data => {
            data.createComment.likedBy = []
            data.createComment.dislikedBy = []
            setComments([...comments, data.createComment])
            values.body = ''
            // await socket.emit('createdMessage', {
            //     action: 'notification',
            // })
        },
    })

    const [deleteDocument, { loading: loadingDeleteDocument }] = useMutation(
        deleteDocumentMutation,
        {
            onCompleted: async () => {
                await socket.emit('createdMessage', {
                    action: 'notification',
                })
                await router.push('/courses/' + documentModel.courseId)
            },
        },
    )
    const isDeleteButtonVisible =
        (documentModel as Document as any).course.moderators.find(
            (moderator: User) => moderator.id === userData?.user?.id,
        ) !== undefined

    const removeComment = (id: string) => {
        setComments(comments.filter(comment => comment.id !== id))
    }

    const handleDeleteDocument = async () => {
        await deleteDocument({
            variables: {
                id: documentModel.id,
            },
        })
    }

    const formik = useFormik({
        initialValues: {
            body: '',
        },
        validationSchema,
        onSubmit: async values => {
            await createComment({
                variables: {
                    input: {
                        body: values.body,
                        userId: userData.user.id,
                        documentId: documentModel.id,
                    },
                },
            })
        },
    })

    const {
        handleSubmit,
        errors,
        touched,
        isSubmitting,
        values,
        setFieldValue,
    } = formik

    const downloadFiles = async () => {
        fileDocs.forEach(file => {
            const blob = new Blob([file.fileData as BlobPart])
            const link = document.createElement('a')
            link.href = window.URL.createObjectURL(blob)
            link.download = file.uri
            link.click()
        })
    }

    return (
        <>
            <Header description={documentModel.name} />

            <div className="flex border-t-2 h-full md:flex-wrap">
                {/*--PDF-VIEWER--*/}
                <div className="w-2/3 md:w-full md:h-5/6">
                    {fileDocs && fileDocs.length > 0 ? (
                        <PDFViewer files={fileDocs} />
                    ) : (
                        <BaseSpinner />
                    )}
                </div>

                <div
                    className={
                        'w-1/3 flex flex-col items-center gap-1 py-2 px-6 bg-white border-l-2 ' +
                        'lg:px-3 md:w-full md:h-1/6 md:py-2 overflow-y-auto scroller dark:bg-darkbg'
                    }
                >
                    <DocumentDetails
                        isDeleteButtonVisible={isDeleteButtonVisible}
                        downloadFiles={downloadFiles}
                        handleDeleteDocument={handleDeleteDocument}
                        loadingDeleteDocument={loadingDeleteDocument}
                        documentModel={documentModel}
                        course={course}
                    />

                    <br />
                    {userData && (
                        <CommentPost
                            handleSubmit={handleSubmit}
                            setFieldValue={setFieldValue}
                            values={values}
                            errors={errors}
                            touched={touched}
                            isSubmitting={isSubmitting}
                        />
                    )}
                    <div className="w-full flex flex-col py-4 gap-4">
                        <CommentsList
                            comments={comments}
                            userData={userData}
                            documentModel={documentModel}
                            removeComment={removeComment}
                        />
                    </div>
                </div>
            </div>
        </>
    )
}

export default DocumentId

export const getServerSideProps: GetServerSideProps = async context => {
    if (!context.params?.id)
        return { redirect: { destination: '/', permanent: false } }

    const id = context.params.id.toString()
    const document = await prisma.document.findUnique({
        where: {
            id,
        },
        include: {
            comments: {
                include: {
                    user: true,
                    likedBy: true,
                    dislikedBy: true,
                },
            },
            course: {
                include: {
                    moderators: true,
                },
            },
            files: true,
            tags: true,
        },
    })
    if (!document || document.deleted)
        return { redirect: { destination: '/404', permanent: false } }

    const documentModel = JSON.parse(JSON.stringify(document))
    return {
        props: {
            documentModel,
            course: documentModel.course,
        },
    }
}
