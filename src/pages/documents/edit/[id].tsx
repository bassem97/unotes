import { Course, Document, School, Tag } from '@prisma/client'
import { GetServerSideProps } from 'next'
import { getSession } from 'next-auth/react'
import prisma from '@/lib/prisma'
import { DragEvent, useEffect, useState } from 'react'
// import DocumentAdd from '@/components/Document/DocumentAdd'
import { useRouter } from 'next/router'
import { Session } from 'next-auth'
import { capitalizeFirst } from '@/lib/capitalizeFirst'
import Head from 'next/head'
import Header from '@/components/Header'

interface Props {
    schools: School[]
    tags: Tag[]
    document: Document
    userId: string
    selectedCourse: Course
    selectedSchool: School
}

/**
 * This page is used to edit a document
 * @param schools
 * @param tags
 * @param document
 * @param userId
 * @constructor
 */
const Id = ({
    schools,
    tags,
    document,
    selectedSchool,
    selectedCourse,
}: Props) => {
    const [dragging, setDragging] = useState<boolean>(false)
    const [result, setResult] = useState('')
    const [redirectTo, setRedirectTo] = useState('')
    const router = useRouter()

    useEffect(() => {
        const redirect = async () => {
            if (redirectTo !== '') await router.push(redirectTo)
        }
        redirect()
    }, [redirectTo])

    const handleDragEnter = (e: DragEvent<HTMLDivElement>) => {
        e.preventDefault()
        e.stopPropagation()
        setDragging(() => true)
    }

    return (
        <>
            <Header description={capitalizeFirst(document.name)} />

            <div
                className="flex justify-center h-full items-center relative"
                onDragEnter={handleDragEnter}
            >
                <div className="bg-bg content-start dark:bg-darkbg scroller2 px-8 pt-8 pb-12 flex flex-wrap gap-4 w-full h-full min-h-full overflow-y-auto">
                    <h1 className="w-full text-2xl pointer-events-none dark:text-bg font-bold mb-2 text-center">
                        Edit Document
                    </h1>
                    <div className="flex justify-center">
                        <h1 className="text-primary text-lg text-center">
                            {result}
                        </h1>
                    </div>
                    {/* <DocumentAdd
                    selectedCourse={selectedCourse}
                    selectedSchool={selectedSchool}
                    document={document}
                    schools={schools}
                    tags={tags}
                    setResult={setResult}
                    setDragging={setDragging}
                    dragging={dragging}
                    setRedirectTo={setRedirectTo}
                /> */}
                </div>
            </div>
        </>
    )
}

export default Id

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null
    if (!session)
        return {
            redirect: {
                destination: `/login?callbackUrl=${context.resolvedUrl}`,
                permanent: false,
            },
        }

    if (!context.params?.id)
        return { redirect: { destination: '/', permanent: false } }

    const id = context.params.id.toString()
    const document = await prisma.document.findUnique({
        where: {
            id: id,
        },
        include: {
            course: {
                include: {
                    moderators: true,
                },
            },
            tags: true,
        },
    })

    if (!document)
        return { redirect: { destination: '/404', permanent: false } }

    const userId = session.user?.id

    // Check if user can update the document (is moderator or owner of document)
    if (
        document.course.moderators.findIndex(
            moderator => moderator.id === userId,
        ) === -1 &&
        document.userId !== userId
    )
        return { redirect: { destination: '/', permanent: false } }

    const schools = await prisma.school.findMany({
        where: {
            verified: true,
            deleted: false,
        },
        include: {
            courses: true,
        },
    })

    const tags = await prisma.tag.findMany()

    const selectedCourse = await prisma.course.findUnique({
        where: {
            id: document.courseId,
        },
    })

    const selectedSchool = await prisma.school.findUnique({
        where: {
            id: selectedCourse?.schoolId,
        },
    })

    return {
        props: {
            document: JSON.parse(JSON.stringify(document)),
            selectedCourse: JSON.parse(JSON.stringify(selectedCourse)),
            selectedSchool: JSON.parse(JSON.stringify(selectedSchool)),
            schools: JSON.parse(JSON.stringify(schools)),
            tags: tags.map((tag: Tag) => ({
                id: tag.id,
                label: tag.label,
            })),
        },
    }
}
