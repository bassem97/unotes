import { useEffect, useState } from 'react'
import { GetServerSideProps, InferGetServerSidePropsType } from 'next'
import { getSession } from 'next-auth/react'
import { gql, NetworkStatus } from '@apollo/client'
import { useRouter } from 'next/router'
import { Session } from 'next-auth'

import { DocumentType, Season } from '@prisma/client'
import DocumentCard from '@/components/Document/DocumentCard'
import { TemporaryDocumentType } from '@/types/TemporaryDocumentType'
import BaseSpinner from '@/components/BaseSpinner'
import prisma from '@/lib/prisma'
import Header from '@/components/Header'
import { SwitchInput } from '@/components'
import SortingDisplayNew, { Option } from '@/components/SortingDisplayNew'
import { useDebounce } from '@/hooks/useDebounce'
import { useInfiniteScroll } from '@/hooks/useInfiniteScroll'

type Props = {
    userSession: Record<string, unknown>
    counts: Option[]
}

// TODO: finish up filters at one point after release - not a high priority
// const FilterSchema = Yup.object().shape({
//     name: Yup.string().min(2, 'Too Short!').max(50, 'Too Long!'),
//     verified: Yup.boolean(),
//     season: Yup.string()
//         .oneOf(['FALL', 'WINTER', 'SUMMER'], 'Invalid season')
//         .transform((value, originalValue) => {
//             if (originalValue && value === originalValue.toUpperCase()) {
//                 return originalValue.toUpperCase() as Season
//             }
//             return undefined
//         }),
// })

const GET_DOCUMENTS = gql`
    query DocumentsByUserID(
        $userId: String!
        $name: String
        $season: Season
        $verified: Boolean
        $skip: Int!
        $limit: Int!
    ) {
        DocumentsByUserID(
            userId: $userId
            skip: $skip
            limit: $limit
            name: $name
            season: $season
            verified: $verified
        ) {
            id
            name
            season
            year
            type
        }
    }
`
export default function Index({
    counts,
    userSession,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
    const [documents, setDocuments] = useState<TemporaryDocumentType[]>([])

    const [docType, setDocType] = useState<string>('')

    const [query, setQuery] = useState<string>('')
    const searchQuery = useDebounce<string>(query, 500)

    // TODO: finish up filters at one point after release - not a high priority
    const [filter, setFilter] = useState<{
        verified: boolean
        Season: Season | 'All'
    }>({ verified: false, Season: 'All' })

    const router = useRouter()
    const { name } = router.query

    useEffect(() => {
        searchQuery
        router.push({
            pathname: router.pathname,
            ...(searchQuery && { query: { name: searchQuery } }),
        })
    }, [searchQuery])

    const { ref, networkStatus, error } =
        useInfiniteScroll<TemporaryDocumentType>({
            query: GET_DOCUMENTS,
            queryVariables: {
                userId: userSession.id,
                ...(name && { name: name as string }),
            },
            items: documents,
            setItems: setDocuments,
            skip: 0,
            limit: 10,
        })

    if (error) {
        console.log(error)
    }

    return (
        <>
            <Header description="My documents" />

            <div className="flex flex-col items-center w-11/12 mx-auto">
                <SortingDisplayNew
                    setFilter={setDocType}
                    filter={docType}
                    query={query}
                    setQuery={setQuery}
                    options={counts}
                />
                <div className="flex justify-end mt-4">
                    <SwitchInput
                        label={'Hide Unverified'}
                        checked={filter.verified}
                        onChange={() =>
                            setFilter(prev => ({
                                ...prev,
                                verified: !filter.verified,
                            }))
                        }
                    />
                </div>
            </div>

            {networkStatus === NetworkStatus.loading ? (
                <div className="w-5 mx-auto my-2">
                    <BaseSpinner />
                </div>
            ) : (
                <div
                    className={
                        'scroller flex flex-wrap gap-4 content-start overflow-y-auto overflow-x-hidden h-full p-8 ' +
                        ' lg:px-2 md:mx-4 sm:mx-2'
                    }
                >
                    {documents
                        .filter(
                            doc => !docType || docType === (doc.type as string),
                        )
                        .filter(doc => !filter.verified || doc.verified)
                        .map(document => (
                            <DocumentCard
                                key={document.id}
                                document={document}
                            />
                        ))}
                    <div ref={ref} />
                </div>
            )}
        </>
    )
}

export const getServerSideProps: GetServerSideProps<Props> = async context => {
    const session = (await getSession(context)) as Session | null
    if (!session) {
        return {
            redirect: {
                destination: '/api/auth/signin?callbackUrl=/documents',
                permanent: false,
            },
        }
    }

    const { name } = context.query

    const typeCounts = await prisma.document.groupBy({
        by: ['type'],
        where: {
            userId: session.user?.id,
            deleted: false,
            ...(name && {
                name: { contains: name as string, mode: 'insensitive' },
            }),
        },
        _count: {
            _all: true,
        },
    })

    const opts: Option[] = typeCounts.map(count => ({
        type: count.type as string,
        count: count._count._all,
    }))

    const countedTypes = opts.map(option => option.type)
    const allTypes = Object.values(DocumentType)

    allTypes.forEach(type => {
        if (!countedTypes.includes(type)) {
            opts.push({
                type: type,
                count: 0,
            })
        }
    })

    return {
        props: {
            userSession: { id: session.user.id },
            counts: JSON.parse(JSON.stringify(opts)),
        },
    }
}
