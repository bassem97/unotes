import prisma from '@/lib/prisma'
import { GetServerSideProps } from 'next'
import { DragEvent, useEffect, useState } from 'react'
import { getSession } from 'next-auth/react'
import { Course, School, Tag } from '@prisma/client'
import { Session } from 'next-auth'
import { useRouter } from 'next/router'
import Head from 'next/head'
import DocumentAdd from '@/components/Document/DocumentAdd'
import Header from '@/components/Header'

interface Props {
    schools: School[]
    tags: Tag[]
    selectedSchool: School
    selectedCourse: Course
}

const New = ({ schools, tags, selectedCourse, selectedSchool }: Props) => {
    const [dragging, setDragging] = useState<boolean>(false)
    const [result, setResult] = useState('')
    const router = useRouter()
    const [redirectTo, setRedirectTo] = useState('')

    useEffect(() => {
        const redirect = async () => {
            if (redirectTo !== '') await router.push(redirectTo)
        }
        redirect()
    }, [redirectTo])

    const handleDragEnter = (e: DragEvent<HTMLDivElement>) => {
        e.preventDefault()
        e.stopPropagation()
        setDragging(() => true)
    }

    return (
        <>
            <Header description={'New Document'} />

            <div
                className="flex justify-center h-full items-center relative"
                onDragEnter={handleDragEnter}
            >
                <div className="bg-bg content-start dark:bg-darkbg scroller2 px-4 py-4 flex flex-col w-full h-full min-h-full overflow-y-auto">
                    {result && (
                        <div className="flex justify-center">
                            <h1 className="text-primary text-lg text-center">
                                {result}
                            </h1>
                        </div>
                    )}

                    <DocumentAdd
                        schools={schools}
                        selectedCourse={selectedCourse}
                        selectedSchool={selectedSchool}
                        tags={tags}
                        setResult={setResult}
                        setDragging={setDragging}
                        dragging={dragging}
                        setRedirectTo={setRedirectTo}
                    />
                </div>
            </div>
        </>
    )
}

export default New

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null
    if (!session)
        return {
            redirect: {
                destination: `/login?callbackUrl=${context.resolvedUrl}`,
                permanent: false,
            },
        }
    const schools = await prisma.school.findMany({
        where: {
            verified: true,
            deleted: false,
        },
        include: {
            courses: {
                include: {
                    moderators: true,
                },
            },
        },
    })

    const tags = await prisma.tag.findMany()

    // get school and course from query
    const { school_id, course_id } = context.query
    const selectedSchool = schools.find(
        school => school.id === (school_id as string),
    )
    const selectedCourse = selectedSchool?.courses.find(
        course => course.id === (course_id as string),
    )

    return {
        props: {
            schools: JSON.parse(JSON.stringify(schools)),
            tags: JSON.parse(JSON.stringify(tags)),
            selectedSchool: selectedSchool
                ? JSON.parse(JSON.stringify(selectedSchool))
                : null,
            selectedCourse: selectedCourse
                ? JSON.parse(JSON.stringify(selectedCourse))
                : null,
        },
    }
}
