import { Course, School } from '@prisma/client'
import { GetServerSideProps } from 'next'
import { getSession } from 'next-auth/react'
import { CourseCard, SchoolCard } from '@/components'
import { Session } from 'next-auth'
import { FC, useState } from 'react'
import Head from 'next/head'
import { gql } from '@apollo/client'
import { useInfiniteScroll } from '@/hooks/useInfiniteScroll'
import {
    Tab,
    TabPanel,
    Tabs,
    TabsBody,
    TabsHeader,
} from '@material-tailwind/react'
import prisma from '@/lib/prisma'
import Filter from '@/components/Filter'
import { useRouter } from 'next/router'
import Header from '@/components/Header'

const FAVORITE_SCHOOLS = gql`
    query favoriteSchools($userId: String!, $limit: Int, $skip: Int) {
        favoriteSchools(userId: $userId, limit: $limit, skip: $skip) {
            id
            name
        }
    }
`

const FAVORITE_COURSES = gql`
    query favoriteCourses($userId: String!, $limit: Int, $skip: Int) {
        favoriteCourses(userId: $userId, limit: $limit, skip: $skip) {
            id
            name
        }
    }
`

type Props = {
    user?: Session['user']
}

const FavoriteSchools: FC<Props> = ({ user }: Props) => {
    const router = useRouter()
    const [schoolsFilter, setSchoolsFilter] = useState<string>('')
    const [coursesFilter, setCoursesFilter] = useState<string>('')

    const [state, setState] = useState({
        schools: [],
        courses: [],
        tabsInitialValue: 'schools',
    })

    const { schools, courses, tabsInitialValue } = state

    const setItems = (key: string, items: School[] | Course[]) => {
        setState(prevState => ({
            ...prevState,
            [key]: items,
        }))
    }

    const { ref: refschools } = useInfiniteScroll<School>({
        query: FAVORITE_SCHOOLS,
        items: schools,
        setItems: items => setItems('schools', items),
        skip: 0,
        limit: 10,
        queryVariables: {
            userId: user.id,
        },
    })

    const { ref: refcourses } = useInfiniteScroll<Course>({
        query: FAVORITE_COURSES,
        items: courses,
        setItems: items => setItems('courses', items),
        skip: 0,
        limit: 10,
        queryVariables: {
            userId: user.id,
        },
    })

    const tabs = [
        { value: 'schools', label: 'Schools' },
        { value: 'courses', label: 'Courses' },
    ]

    const renderSchoolsCards = (schools: School[]) => {
        return schools.map((school: School, index: number) => (
            <SchoolCard
                key={index}
                school={school}
                userId={user.id}
                isFavorite={user.favoriteSchools.some(
                    (favoriteSchool: School) => favoriteSchool.id === school.id,
                )}
            />
        ))
    }

    const renderCoursesCards = (courses: Course[]) => {
        return courses.map((course: Course, index: number) => (
            <CourseCard
                key={index}
                course={course}
                userId={user.id}
                isFavorite={user.favoriteCourses.some(
                    (favoriteCourse: Course) => favoriteCourse.id === course.id,
                )}
            />
        ))
    }

    const renderTabPanel = (value: string) => {
        const items = state[value]
        return (
            <TabPanel key={value} value={value}>
                <Filter
                    buttonLabel={value}
                    itemsFilter={
                        value === 'schools' ? schoolsFilter : coursesFilter
                    }
                    setItemsFilter={
                        value === 'schools'
                            ? setSchoolsFilter
                            : setCoursesFilter
                    }
                    handleClick={() => {
                        router.push(`/${value}/add`)
                    }}
                />
                <div
                    className={
                        'scroller grid grid-cols-5 gap-8 content-start overflow-y-auto overflow-x-hidden h-full p-8 ' +
                        'xl:grid-cols-4 lg:grid-cols-3 lg:px-2 md:grid-cols-2 xs:grid-cols-1 md:mx-4 sm:mx-1 sm:px-3'
                    }
                >
                    {items.length === 0 ? (
                        <div className="flex flex-col items-center justify-center h-full w-full">
                            <p className="text-xl text-center dark:text-bg">
                                No {value} found
                            </p>
                        </div>
                    ) : (
                        <>
                            {value === 'schools'
                                ? schoolsFilter
                                    ? renderSchoolsCards(
                                          user.favoriteSchools.filter(
                                              (school: School) =>
                                                  school.name
                                                      .toLowerCase()
                                                      .includes(
                                                          schoolsFilter.toLowerCase(),
                                                      ),
                                          ),
                                      )
                                    : renderSchoolsCards(items)
                                : coursesFilter
                                ? renderCoursesCards(
                                      user.favoriteCourses.filter(
                                          (course: Course) =>
                                              course.name
                                                  .toLowerCase()
                                                  .includes(
                                                      coursesFilter.toLowerCase(),
                                                  ),
                                      ),
                                  )
                                : renderCoursesCards(items)}
                            <div
                                ref={
                                    value === 'schools'
                                        ? refschools
                                        : refcourses
                                }
                            />
                        </>
                    )}
                </div>
            </TabPanel>
        )
    }

    return (
        <>
            <Header description={'Favorites'} />
            <div className="flex flex-col items-center my-8 gap-2 min-h-fit md:mx-4 md:mt-8">
                <h1 className="font-bold text-font2 dark:text-bg2 text-xl justify-center uppercase">
                    Favorites
                </h1>
            </div>
            <div>
                <Tabs id="custom-animation" value={tabsInitialValue}>
                    <TabsHeader>
                        {tabs.map(tab => (
                            <Tab
                                key={tab.value}
                                value={tab.value}
                                className={
                                    'border-b-2 text-center py-4 font-bold cursor-pointer text-xl justify-center uppercase ' +
                                    'dark:text-bg'
                                }
                                activeClassName={
                                    'border-b-2 text-center py-4 font-bold cursor-pointer text-xl justify-center uppercase mb-[-2px] border-primary text-primary ' +
                                    '  '
                                }
                            >
                                {tab.label}
                            </Tab>
                        ))}
                    </TabsHeader>
                    <TabsBody
                        animate={{
                            initial: { y: 250 },
                            mount: { y: 0 },
                            unmount: { y: 250 },
                        }}
                        className="scroller h-full overflow-y-auto overflow-x-hidden"
                    >
                        {tabs.map(tab => renderTabPanel(tab.value))}
                    </TabsBody>
                </Tabs>
            </div>
        </>
    )
}

export default FavoriteSchools

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null
    let user
    if (session) {
        const id = session.user?.id
        user = await prisma.user.findUnique({
            where: { id },
            select: {
                id: true,
                favoriteSchools: {
                    select: {
                        id: true,
                        name: true,
                    },
                },
                favoriteCourses: {
                    select: {
                        id: true,
                        name: true,
                    },
                },
            },
        })
    } else
        return {
            redirect: {
                destination: `/login?callbackUrl=${context.resolvedUrl}`,
                permanent: false,
            },
        }

    return {
        props: {
            ...(session && { user: JSON.parse(JSON.stringify(user)) }),
        },
    }
}
