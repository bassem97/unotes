import { useState } from 'react'
import prisma from '@/lib/prisma'
import { getSession } from 'next-auth/react'
import { GetServerSideProps } from 'next'
import { FaSearch } from 'react-icons/fa'
import Image from 'next/image'
import { Course, School } from '@prisma/client'
import { useRouter } from 'next/router'
import { Button, Select, SelectForm } from '@/components'
import Head from 'next/head'
import { Session } from 'next-auth'
import { Option, toOption } from '@/types/OptionType'
import { TemporaryCourseType } from '@/types/TemporaryCourseType'
import Header from '@/components/Header'

export default function Home(props: any) {
    const [school, setSchool] = useState<School | null>(null)
    const [course, setCourse] = useState<Course | null>(null)
    const { schools } = props
    const router = useRouter()
    const courses = school
        ? schools.find((sch: School) => sch?.id === school?.id).courses
        : []

    return (
        <>
            <Header description="Find the best courses for your career" />
            <div className="h-full  overflow-hidden  relative before:left-0 before:top-0 w-full before:content-[''] md:z-0 before:rounded-xl before:absolute before:w-full before:h-full before:z-10 before:bg-font dark:before:bg-opacity-40 before:bg-opacity-60">
                <Image
                    className="w-full block h-full object-cover rounded-3xl "
                    fill
                    src="/bg.jpg"
                    alt=""
                />
                <div className=" scroller top-0  content-start overflow-y-auto  overflow-x-hidden absolute padding h-full w-full flex flex-col gap-12 justify-center bottom-0 left-0 lg:gap-8 z-20">
                    <div className="text-bg w-8/12 flex flex-col gap-4 lg:w-full md:items-center sm:gap-2">
                        <h1
                            className={
                                'uppercase font-bold text-[45px] leading-snug ' +
                                'xl:text-3xl md:leading-8 md:text-3xl md:text-center sm:text-2xl xs:text-xl'
                            }
                        >
                            Let’s fill in the gaps in your college{' '}
                            <span className="text-primary bg-bg dark:bg-darkbg px-1 ">
                                education
                            </span>
                        </h1>
                        <p className="mb-2 xs:text-sm md:text-center ">
                            Find the best courses for your career
                        </p>
                        <Button
                            label={'explore'}
                            onClick={() => router.push(`/schools`)}
                        />
                    </div>
                    <div
                        className={
                            'mx-auto rounded-3xl w-full bg-bg flex items-end justify-center gap-12 py-8 px-12 xl:gap-8 ' +
                            'lg:flex-col lg:px-4 lg:items-center lg:gap-6 lg:py-6 dark:bg-darkbg'
                        }
                    >
                        <div className="w-96 lg:w-full">
                            <div className="mb-2 text-xl uppercase whitespace-nowrap tracking-wider dark:text-bg text-font2 font-bold xl:text-lg xs:text-base">
                                select your school
                            </div>
                            <SelectForm
                                selected={toOption(school)}
                                options={schools.map((s: School) =>
                                    toOption(s),
                                )}
                                label="school"
                                onChange={items => {
                                    setSchool(
                                        schools.find(
                                            (s: School) =>
                                                (items as Option).value ===
                                                s.id,
                                        ),
                                    )
                                    setCourse(null)
                                }}
                            />
                        </div>
                        <div className="w-96 lg:w-full">
                            <div className="mb-2 text-xl uppercase whitespace-nowrap text-ellipsis dark:text-bg tracking-wider text-font2 font-bold xl:text-lg xs:text-base">
                                Select Your Course
                            </div>
                            <SelectForm
                                selected={toOption(course)}
                                disabled={!school}
                                options={courses.map((s: TemporaryCourseType) =>
                                    toOption(s),
                                )}
                                label="course"
                                onChange={items => {
                                    setCourse(
                                        courses.find(
                                            (c: TemporaryCourseType) =>
                                                (items as Option).value ===
                                                c.id,
                                        ),
                                    )
                                }}
                            />
                        </div>
                        <Button
                            label="search"
                            classname="px-6 py-3 border-2 xs:py-2"
                            disabled={!(school && course)}
                            icon={<FaSearch />}
                            onClick={() =>
                                router.push(`/courses/${course?.id}`)
                            }
                        />
                    </div>
                </div>
            </div>
        </>
    )
}

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null
    const schools = await prisma.school.findMany({
        where: {
            verified: true,
            deleted: false,
        },
        include: {
            courses: true,
        },
    })
    return {
        props: {
            session,
            schools: JSON.parse(JSON.stringify(schools)),
        },
    }
}
