import { useState } from 'react'
import { BsPersonFill, BsTwitter } from 'react-icons/bs'
import { AiFillEye, AiFillEyeInvisible, AiFillLock } from 'react-icons/ai'
import { FaDiscord, FaFacebookF, FaGooglePlusG } from 'react-icons/fa'
import { FcGoogle } from 'react-icons/fc'
import { MdOtherHouses } from 'react-icons/md'
import { RxGithubLogo } from 'react-icons/rx'
import { useRouter } from 'next/router'
import { getSession, signIn } from 'next-auth/react'
import Image from 'next/image'
import { GetServerSideProps } from 'next'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { Button, DropdownContainer } from '@/components'
import Link from 'next/link'
import BaseSpinner from '@/components/BaseSpinner'
import { Session } from 'next-auth'
import Head from 'next/head'
import Header from '@/components/Header'

const validationSchema = Yup.object().shape({
    email: Yup.string().email().required('Required'),
    password: Yup.string()
        .min(5, 'Password is too short - should be 8 chars minimum.')
        .required('Required'),
})

const Login = () => {
    const [loading, setLoading] = useState(false)
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validationSchema,
        onSubmit: async ({ email, password }) => {
            setLoading(true)
            await signIn('credentials', {
                email: email.trim().toLowerCase(),
                password,
                redirect: false,
            }).then(res => {
                if (res?.error) {
                    const errors = {
                        ERROR_CODE_USER_NOT_FOUND:
                            'No user found with that email, please sign up',
                        ERROR_CODE_INACTIVE_USER:
                            'Your account has been deleted. Please contact our uNotes team if you require to recover your account.',
                        ERROR_CODE_NO_PASSWORD_SET:
                            'User account has no password set, please try to sign in with Google',
                        ERROR_CODE_ACCOUNT_NOT_VERIFIED:
                            'Your account has not been verified. Please check your email for a verification link.',
                        ERROR_CODE_INCORRECT_PASSWORD:
                            'Incorrect password, please try again',
                    }
                    setError(errors[res.error])
                    setLoading(false)
                } else if (router.query.callbackUrl)
                    router.push(router.query.callbackUrl.toString())
                else router.push('/')
            })
        },
    })
    const {
        handleSubmit,
        errors,
        touched,
        handleChange,
        handleBlur,
        isSubmitting,
        values,
    } = formik
    const [shown, setShown] = useState(false)
    const [error, setError] = useState('')
    const router = useRouter()

    return (
        <>
            <Header description="Login to your uNotes account" />
            <div className="md:flex-col-reverse min-h-fit flex justify-end items-center h-full w-full relative overflow-hidden ">
                {/* ________ Login-Page Redirect-to-SignUp-page Sectinon ________ */}
                <div
                    className={
                        'w-1/2 relative flex justify-center flex-col items-center h-full bg-primary' +
                        ' md:w-full md:h-fit md:py-8 '
                    }
                >
                    <h1 className=" text-bg text-center relative font-bold text-3xl h-fit mb-4 ">
                        You don`t have an account?
                    </h1>
                    <p className=" text-bg w-2/3 text-center mb-4">
                        Start your member account now !
                    </p>
                    <Button
                        variant="secondary"
                        onClick={() => {
                            router.push('/signup')
                        }}
                        label={'Sign up'}
                        classname="border-2 px-2 py-1 text-lg "
                    />
                    <div className="relative w-full mt-4 h-80 md:hidden sm:h-60 xs:h-32 xs:mt-0">
                        <Image
                            src={'/signup.png'}
                            fill
                            className="object-contain"
                            alt=""
                        />
                    </div>
                </div>

                {/* ________ Login-Page SignIn/LogIn Sectinon ________ */}
                <div
                    className={
                        'flex-col flex overflow-y-auto py-12 gap-4 px-32 lg:px-10 w-1/2 justify-center relative bg-bg2 bg-opacity-20 h-full min-h-fit' +
                        '  md:w-full md:px-32 sm:px-16 xs:gap-2'
                    }
                >
                    <div className="flex justify-center">
                        <h1 className="text-3xl font-bold">Sign in</h1>
                    </div>
                    {/* error div */}
                    <div className="flex justify-center">
                        <h1 className="text-red-500 text-lg text-center">
                            {error}
                        </h1>
                    </div>
                    {error ===
                        'User account is not verified , please check you email for verification link or try to sign in with Google' && (
                        <div className="flex justify-center">
                            <div className="text-red-500 text-lg text-center">
                                If you have not received the verification email,
                                please click
                                <Link
                                    href={'/verification/' + values.email}
                                    className="text-primary"
                                >
                                    {' '}
                                    here
                                </Link>
                            </div>
                        </div>
                    )}
                    <form onSubmit={handleSubmit}>
                        <div className="flex flex-col gap-1 bg-transparent">
                            <label
                                htmlFor="email"
                                className="text-font2 text-lg"
                            >
                                Email
                            </label>
                            <div
                                className={`border-2 bg-bg2 bg-opacity-50 rounded-3xl flex px-4 py-2 border-bg2 items-center border
                      ${
                          ((errors.email && touched.email) || error) &&
                          'border-red-500'
                      } `}
                            >
                                <BsPersonFill className="text-font2 text-lg" />
                                <input
                                    className="pl-4 w-full outline-none placeholder:text-font2 bg-transparent text-lg"
                                    type="email"
                                    name="email"
                                    id="email"
                                    placeholder="Email"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </div>
                            {errors.email && touched.email && (
                                <div className="text-red-500 text-xs italic">
                                    {errors.email}
                                </div>
                            )}
                        </div>

                        <div className="flex flex-col gap-1 bg-transparent">
                            <label
                                htmlFor="password"
                                className="text-font2 text-lg"
                            >
                                Password
                            </label>
                            <div
                                className={`border-2 bg-bg2 bg-opacity-50 rounded-3xl border-bg2 items-center  flex pl-4 py-2
            ${
                ((errors.password && touched.password) || error) &&
                'border-red-500'
            } `}
                            >
                                <AiFillLock className="text-font2 text-lg" />
                                <input
                                    className="pl-4 w-full outline-none placeholder:text-font2 bg-transparent text-lg "
                                    type={shown ? 'text' : 'password'}
                                    name="password"
                                    id="password"
                                    placeholder="Password"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                {shown ? (
                                    <AiFillEye
                                        className=" mr-4 cursor-pointer"
                                        onClick={() => setShown(false)}
                                    />
                                ) : (
                                    <AiFillEyeInvisible
                                        className=" mr-4 cursor-pointer"
                                        onClick={() => setShown(true)}
                                    />
                                )}
                            </div>
                            {errors.password && touched.password && (
                                <div className="text-red-500 text-xs italic">
                                    {errors.password}
                                </div>
                            )}
                            <h2 className="text-end mb-2 text-sm pr-2 cursor-pointer underline underline-offset-2">
                                <Link href="/reset-password/emailform">
                                    Forgot your password?
                                </Link>
                            </h2>
                        </div>
                        {loading ? (
                            <div className="h-20 w-20 mx-auto">
                                <BaseSpinner />
                            </div>
                        ) : (
                            <div className="flex justify-center">
                                <Button
                                    classname="text-xl px-4 py-1"
                                    disabled={isSubmitting}
                                    label=""
                                >
                                    <button
                                        type="submit"
                                        className="w-full h-full"
                                    >
                                        Login
                                    </button>
                                </Button>
                            </div>
                        )}
                    </form>
                    <h2 className="text-font uppercase text-center my-2">Or</h2>
                    <div className="text-xl flex gap-4 justify-center items-center justify-items-center text-bg">
                        <div className="group p-2 rounded-full cursor-pointer bg-[#4267B2] hidden">
                            {' '}
                            {/* remove hidden class to unhide fb-icon */}
                            <FaFacebookF className="group-hover:-translate-y-1 transition-all" />
                        </div>
                        <div className="group p-2 rounded-full cursor-pointer bg-bg">
                            <FcGoogle
                                className="group-hover:animate-[500ms_hop_ease-in-out] transition-all"
                                onClick={() =>
                                    signIn('google', {
                                        callbackUrl:
                                            router.query.callbackUrl?.toString() ||
                                            '/',
                                    })
                                }
                            />
                        </div>
                        <div className="hidden">
                            {' '}
                            {/* remove hidden class to unhide DropdownContainer */}
                            <DropdownContainer
                                position="bottom-11 right-0 py-2 px-4"
                                head={
                                    <div
                                        title="Others..."
                                        className="group p-2 rounded-full cursor-pointer bg-font2 flex justify-center items-center"
                                    >
                                        <MdOtherHouses className="group-hover:-translate-y-1 transition-all" />
                                    </div>
                                }
                            >
                                <div className="grid grid-cols-3 gap-4 w-40 py-6 justify-items-center">
                                    <div className="group p-2 rounded-full cursor-pointer bg-[#1DA1F2]">
                                        <BsTwitter className="group-hover:-translate-y-1 transition-all" />
                                    </div>
                                    <div className="group p-2 rounded-full cursor-pointer bg-[#DB4437]">
                                        <FaGooglePlusG className="group-hover:-translate-y-1 transition-all" />
                                    </div>
                                    <div className="group p-2 rounded-full cursor-pointer bg-[#5865F2]">
                                        <FaDiscord className="group-hover:-translate-y-1 transition-all" />
                                    </div>
                                    <div className="group p-2 rounded-full cursor-pointer bg-[#171515]">
                                        <RxGithubLogo className="group-hover:-translate-y-1 transition-all" />
                                    </div>
                                </div>
                            </DropdownContainer>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Login

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null
    if (session) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }
    return {
        props: {},
    }
}
