import { useRef, useState } from 'react'
import { Button } from '@/components'
import Image from 'next/image'
import { useFormik } from 'formik'
import { AiFillEye, AiFillEyeInvisible, AiFillLock } from 'react-icons/ai'
import { object, ref, string } from 'yup'
import PasswordChecker from '@/components/PasswordStrength/PasswordChecker'
import { GetServerSideProps } from 'next'
import { useRouter } from 'next/router'
import prisma from '@/lib/prisma'
import { gql, useMutation } from '@apollo/client'
import BaseSpinner from '@/components/BaseSpinner'
import Head from 'next/head'
import Header from '@/components/Header'

interface NewPasswordProps {
    token: string
}

const changePasswordMutation = gql`
    mutation changePassword($token: String!, $password: String!) {
        changePassword(token: $token, password: $password) {
            id
        }
    }
`

const validationSchema = object().shape({
    password: string()
        .min(5)
        .matches(/[A-Z]/)
        .matches(/[0-9]/)
        .matches(/[!@#$%^&*+-]/)
        .required(),
    confirmPassword: string()
        .oneOf([ref('password')])
        .required(),
})

const NewPassword = ({ token }: NewPasswordProps) => {
    const router = useRouter()
    const [shown, setShown] = useState(false)
    const [changePassword, { loading, error }] = useMutation(
        changePasswordMutation,
        {
            onCompleted: async () => {
                await router.push('/login')
            },
        },
    )
    const password = useRef<HTMLInputElement>(null)

    const formik = useFormik({
        initialValues: {
            password: '',
            confirmPassword: '',
        },
        validationSchema,
        onSubmit: async () => {
            await changePassword({
                variables: {
                    token,
                    password,
                },
            })
        },
    })

    const { handleSubmit, errors, touched, handleChange, handleBlur, values } =
        formik

    return (
        <>
            <Header description={'New Password'} />
            <div className="flex justify-end items-center h-full w-full relative overflow-hidden ">
                <div className="w-1/2 relative flex justify-center flex-col items-center h-full bg-primary dark:bg-primarysec ">
                    <h1 className=" text-bg text-center relative font-bold text-3xl h-fit mb-4 ">
                        Hooray ! You are almost there
                    </h1>
                    <p className=" text-bg w-2/3 text-center mb-4">
                        Please enter your new password to continue
                    </p>
                    <Button
                        variant="secondary"
                        onClick={() => {
                            router.push('/signup')
                        }}
                        label={'Sign up'}
                        classname="border-2 px-2 py-1 text-lg "
                    />
                    <div className="relative w-full mt-4 h-80">
                        <Image
                            src={'/reset password.png'}
                            fill
                            className="object-contain"
                            alt=""
                        />
                    </div>
                </div>

                <div className="flex-col flex gap-4 px-32 w-1/2 justify-center relative bg-bg2 bg-opacity-20 h-full">
                    <div className="flex justify-center">
                        <h1 className="text-3xl font-bold">Reset Password</h1>
                    </div>
                    {/* error div */}
                    <div className="flex justify-center">
                        <h1 className="text-red-500 text-lg text-center">
                            {error?.message}
                        </h1>
                    </div>
                    {error?.message ===
                        'User account is not verified , please check you email for verification link or try to sign in with Google' && (
                        <div className="flex justify-center"></div>
                    )}
                    <form onSubmit={handleSubmit}>
                        <div className="flex flex-col gap-1 bg-transparent my-3">
                            <label
                                htmlFor="password"
                                className="text-font2 text-lg xs:text-sm"
                            >
                                Password
                            </label>
                            <div
                                className={`border-2 bg-bg2 bg-opacity-50 rounded-3xl border-bg2 items-center  flex pl-4 py-2 xs:py-1
                        ${
                            ((errors.password && touched.password) || error) &&
                            'border-red-500'
                        } `}
                            >
                                <AiFillLock className="text-font2 text-lg xs:text-sm" />
                                <input
                                    className="pl-4 w-full outline-none placeholder:text-font2 bg-transparent text-lg xs:text-base "
                                    type={shown ? 'text' : 'password'}
                                    name="password"
                                    id="password"
                                    placeholder="Password"
                                    ref={password}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                {shown ? (
                                    <AiFillEye
                                        className=" mr-4 cursor-pointer"
                                        onClick={() => setShown(false)}
                                    />
                                ) : (
                                    <AiFillEyeInvisible
                                        className=" mr-4 cursor-pointer"
                                        onClick={() => setShown(true)}
                                    />
                                )}
                            </div>
                        </div>
                        <div className="flex flex-col gap-1 bg-transparent my-3">
                            <label
                                htmlFor="confirmPassword"
                                className="text-font2 text-lg xs:text-sm"
                            >
                                Confirm Password
                            </label>
                            <div
                                className={`border-2 bg-bg2 bg-opacity-50 rounded-3xl border-bg2 items-center  flex pl-4 py-2 xs:py-1
                        ${
                            ((errors.confirmPassword &&
                                touched.confirmPassword) ||
                                error) &&
                            'border-red-500'
                        } `}
                            >
                                <AiFillLock className="text-font2 text-lg xs:text-sm" />
                                <input
                                    className="pl-4 w-full outline-none placeholder:text-font2 bg-transparent text-lg xs:text-base "
                                    type={shown ? 'text' : 'password'}
                                    name="confirmPassword"
                                    id="confirmPassword"
                                    placeholder="Confirm Password"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                {shown ? (
                                    <AiFillEye
                                        className=" mr-4 cursor-pointer"
                                        onClick={() => setShown(false)}
                                    />
                                ) : (
                                    <AiFillEyeInvisible
                                        className=" mr-4 cursor-pointer"
                                        onClick={() => setShown(true)}
                                    />
                                )}
                            </div>
                        </div>
                        <PasswordChecker
                            password={values.password}
                            confirmPassword={values.confirmPassword}
                        />
                        {loading ? (
                            <BaseSpinner />
                        ) : (
                            <div className="flex justify-center">
                                <Button
                                    label=""
                                    classname="text-xl px-4 py-1 mt-3 xs:text-base"
                                    disabled={loading}
                                >
                                    <button
                                        type="submit"
                                        className="w-full h-full"
                                    >
                                        Reset Password
                                    </button>
                                </Button>
                            </div>
                        )}
                    </form>
                    );
                </div>
            </div>
        </>
    )
}

export default NewPassword

export const getServerSideProps: GetServerSideProps = async context => {
    const { token } = context.query
    const user = await prisma.user.findFirst({
        where: {
            resetToken: token as string,
        },
    })
    if (!user) {
        return {
            notFound: true,
        }
    }
    return {
        props: {
            token: token as string,
        },
    }
}
