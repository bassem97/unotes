import { useRouter } from 'next/router'
import Button from '../../components/Button'
import Image from 'next/image'
import { useState } from 'react'
import { useFormik } from 'formik'
import { BsPersonFill } from 'react-icons/bs'
import * as Yup from 'yup'
import { gql, useMutation } from '@apollo/client'
import BaseSpinner from '@/components/BaseSpinner'
import Head from 'next/head'
import Header from '@/components/Header'

const forgetPasswordMutation = gql`
    mutation forgetPassword($email: String!) {
        forgetPassword(email: $email) {
            id
        }
    }
`

const validationSchema = Yup.object().shape({
    email: Yup.string().email().required('Required'),
})

const Emailform = () => {
    const [success, setSuccess] = useState('')
    const [iterations, setIterations] = useState(0)
    const router = useRouter()
    const [forgetPassword, { loading, error }] = useMutation(
        forgetPasswordMutation,
        {
            onCompleted: async () => {
                setSuccess('Check your email for a link to reset your password')
            },
        },
    )

    const formik = useFormik({
        initialValues: {
            email: '',
        },
        validationSchema,
        onSubmit: async ({ email }) => {
            setIterations(iterations + 1)
            await forgetPassword({ variables: { email } })
        },
    })
    const {
        handleSubmit,
        errors,
        touched,
        handleChange,
        handleBlur,
        isSubmitting,
    } = formik

    return (
        <>
            <Header description={'Reset Password'} />
            <div className="flex justify-end items-center h-full w-full relative overflow-hidden ">
                <div className="w-1/2 relative flex justify-center flex-col items-center h-full bg-primary ">
                    <h1 className=" text-bg text-center relative font-bold text-3xl h-fit mb-4 ">
                        Don&apos;t worry it happens to the best of us
                    </h1>
                    <div className="relative w-full mt-4 h-80">
                        <Image
                            src={'/reset password.png'}
                            fill
                            className="object-contain"
                            alt=""
                        />
                    </div>
                    <Button
                        variant="secondary"
                        onClick={() => {
                            router.push('/login')
                        }}
                        label={'Sign in'}
                        classname="border-2 px-2 py-1 text-lg sm:text-xl xs:text-sm"
                    />
                </div>

                <div className="flex-col flex gap-4 px-32 w-1/2 justify-center relative bg-bg2 bg-opacity-20 h-full">
                    <div className="flex justify-center">
                        <h1 className="text-3xl font-bold">Reset password</h1>
                    </div>
                    {error ? (
                        <div className="flex justify-center">
                            <h1 className="text-red-500 text-lg">
                                {error.message}
                            </h1>
                        </div>
                    ) : (
                        <div className="flex justify-center">
                            <h1 className="text-green-500 text-lg">
                                {success}
                            </h1>
                        </div>
                    )}

                    <form onSubmit={handleSubmit}>
                        <div className="flex flex-col gap-1 bg-transparent">
                            <label
                                htmlFor="email"
                                className="text-font2 text-lg"
                            >
                                Email
                            </label>
                            <div
                                className={`border-2 bg-bg2 bg-opacity-50 rounded-3xl flex px-4 py-2 border-bg2 items-center border
                      ${
                          ((errors.email && touched.email) || error) &&
                          'border-red-500'
                      } `}
                            >
                                <BsPersonFill className="text-font2 text-lg" />
                                <input
                                    className="pl-4 w-full outline-none placeholder:text-font2 bg-transparent text-lg"
                                    type="email"
                                    name="email"
                                    id="email"
                                    placeholder="Email"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </div>
                            {errors.email && touched.email && (
                                <div className="text-red-500 text-xs italic">
                                    {errors.email}
                                </div>
                            )}
                        </div>

                        {loading ? (
                            <BaseSpinner />
                        ) : (
                            <div className="flex justify-center mt-2">
                                {iterations >= 3 ? (
                                    <p>Too many attempts, try again later</p>
                                ) : (
                                    <Button
                                        classname="text-xl px-4 py-1"
                                        disabled={isSubmitting}
                                        label=""
                                    >
                                        <button
                                            type="submit"
                                            className="w-full h-full"
                                        >
                                            Send link
                                        </button>
                                    </Button>
                                )}
                            </div>
                        )}
                    </form>
                </div>
            </div>
        </>
    )
}

export default Emailform
