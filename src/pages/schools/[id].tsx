import prisma from '@/lib/prisma'
import { Course, School } from '@prisma/client'
import { GetServerSideProps } from 'next'
import { getSession } from 'next-auth/react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { Button, CourseCard, Search } from '@/components'
import { Session } from 'next-auth'
import { gql, NetworkStatus, useQuery } from '@apollo/client'
import { useInfiniteScroll } from '@/hooks/useInfiniteScroll'
import BaseSpinner from '@/components/BaseSpinner'
import Head from 'next/head'
import { capitalizeFirst } from '@/lib/capitalizeFirst'
import Header from '@/components/Header'
import { useDebounce } from '@/hooks/useDebounce'
type Props = {
    school: School
    courses: Course[]
    user?: Session['user']
}

const GET_COURSES = gql`
    query courses($school_id: String!, $limit: Int, $skip: Int, $name: String) {
        courses(schoolId: $school_id, limit: $limit, skip: $skip, name: $name) {
            id
            name
            image
        }
    }
`

const SchoolID = ({ school, courses: schoolCourses, user }: Props) => {
    const [filter, setFilter] = useState<string>('')
    const [courses, setCourses] = useState<Course[]>([])
    const router = useRouter()
    const [query, setQuery] = useState<string>('')
    const searchQuery = useDebounce<string>(query, 500)

    const { id, name } = router.query
    useEffect(() => {
        searchQuery
        router.push({
            pathname: router.pathname,
            query: {
                id: id,
                ...(searchQuery && { name: searchQuery }),
            },
        })
    }, [searchQuery])

    const { ref, networkStatus, error } = useInfiniteScroll<Course>({
        query: GET_COURSES,
        queryVariables: {
            school_id: school.id,
            ...(name && { name: name as string }),
        },
        items: courses,
        setItems: setCourses,
        skip: 0,
        limit: 10,
    })

    if (networkStatus === NetworkStatus.loading) {
        return <BaseSpinner />
    }
    if (error) {
        console.log(error)
    }

    return (
        <>
            <Header
                description={capitalizeFirst(school.name) + ' courses page'}
            />
            <div className="flex flex-col items-center my-8 gap-2 min-h-fit md:mx-4">
                <div className="relative h-24 w-1/4 lg:w-1/2 mt-8">
                    <Image
                        fill
                        src={school.image || '/school-default.png'}
                        alt=""
                        className="object-contain"
                        sizes={'100%'}
                    />
                </div>
                <h1 className="font-bold text-font2 dark:text-bg2 text-xl justify-center uppercase">
                    {school.name}
                </h1>
                <div className="w-full flex gap-4 items-center mt-4 justify-center md:px-2">
                    <div className="w-2/3">
                        <Search
                            filter={query}
                            onChange={newFilter => {
                                setQuery(newFilter)
                                router.push({
                                    pathname: router.pathname,
                                    query: { ...router.query, name: newFilter },
                                })
                            }}
                            placeholder="Filter..."
                        />
                    </div>
                    <div>
                        <Button
                            label="Add Course"
                            onClick={() =>
                                router.push({
                                    pathname: '/courses/new/',
                                    query: { school_id: school.id },
                                })
                            }
                        />
                    </div>
                </div>
            </div>
            <div
                className={
                    'scroller grid grid-cols-5 gap-8 content-start overflow-y-auto overflow-x-hidden h-full p-8 ' +
                    'lg:grid-cols-2 lg:px-2 md:grid-cols-1 md:mx-4'
                }
            >
                {filter
                    ? schoolCourses
                          .filter(course =>
                              course.name
                                  .toLowerCase()
                                  .includes(filter.toLowerCase()),
                          )
                          .map((course, index) =>
                              user ? (
                                  <CourseCard
                                      key={index}
                                      course={course}
                                      userId={user.id}
                                      isFavorite={user.favoriteCourses.some(
                                          (fav: Course) => fav.id === course.id,
                                      )}
                                  />
                              ) : (
                                  <CourseCard key={index} course={course} />
                              ),
                          )
                    : courses.map((course, index) =>
                          user ? (
                              <CourseCard
                                  key={index}
                                  course={course}
                                  userId={user.id}
                                  isFavorite={user.favoriteCourses.some(
                                      (fav: Course) => fav.id === course.id,
                                  )}
                              />
                          ) : (
                              <CourseCard key={index} course={course} />
                          ),
                      )}
                <div ref={ref} />
            </div>
        </>
    )
}

export default SchoolID

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null

    if (!context.params?.id)
        return { redirect: { destination: '/', permanent: false } }
    const filter = (context.query.name as string) || '' // Define the filter here

    const id = context.params.id.toString()
    const school = await prisma.school.findUnique({
        where: {
            id: id,
        },
    })

    const courses = await prisma.course.findMany({
        where: {
            schoolId: id,
            name: {
                contains: filter,
                mode: 'insensitive',
            },
        },
    })

    if (!school) return { redirect: { destination: '/404', permanent: false } }

    let user
    if (session) {
        const id = session.user?.id
        user = await prisma.user.findUnique({
            where: { id },
            select: {
                id: true,
                favoriteCourses: {
                    select: {
                        id: true,
                    },
                },
            },
        })
    }

    return {
        props: {
            school: JSON.parse(JSON.stringify(school)),
            courses: JSON.parse(JSON.stringify(courses)),
            ...(session && { user: JSON.parse(JSON.stringify(user)) }),
        },
    }
}
