import { getSession } from 'next-auth/react'
import { GetServerSideProps } from 'next'
import { useEffect, useState } from 'react'
import { School } from '@prisma/client'
import prisma from '@/lib/prisma'
import SchoolForm from '@/components/School/SchoolForm'
import { useRouter } from 'next/router'
import { Session } from 'next-auth'
import Head from 'next/head'
import { capitalizeFirst } from '@/lib/capitalizeFirst'
import Header from '@/components/Header'

type Props = {
    school: School
}

const Id = ({ school }: Props) => {
    const [result, setResult] = useState('')
    const [redirectTo, setRedirectTo] = useState('')
    const router = useRouter()

    useEffect(() => {
        const redirect = async () => {
            if (redirectTo !== '') await router.push(redirectTo)
        }
        redirect()
    }, [redirectTo])

    return (
        <>
            <Header description={capitalizeFirst(school.name)} />
            <div className="flex justify-center h-full w-full items-center bg-bg">
                <div className="bg-bg h-full dark:bg-darkbg rounded-3xl px-8 pt-16 pb-20 flex flex-col gap-4 w-1/2 md:w-fit ">
                    <h1 className="w-full h-fit text-2xl pointer-events-none dark:text-bg font-bold mb-2 text-center">
                        Edit School
                    </h1>
                    <div className="flex justify-center">
                        <h1 className="text-primary text-lg text-center">
                            {result}
                        </h1>
                    </div>
                    <SchoolForm
                        school={school}
                        setResult={setResult}
                        setRedirectTo={setRedirectTo}
                    />
                </div>
            </div>
        </>
    )
}

export default Id

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null
    if (!session)
        return {
            redirect: {
                destination: `/login?callbackUrl=${context.resolvedUrl}`,
                permanent: false,
            },
        }

    if (!context.params?.id)
        return { redirect: { destination: '/', permanent: false } }

    const id = context.params.id.toString()
    const school = await prisma.school.findUnique({
        where: {
            id: id,
        },
        include: {
            courses: {
                include: {
                    moderators: true,
                },
            },
        },
    })
    if (!school) return { redirect: { destination: '/404', permanent: false } }

    const userId = session.user?.id

    if (school.userId !== userId)
        return { redirect: { destination: '/404', permanent: false } }

    return {
        props: {
            school: JSON.parse(JSON.stringify(school)),
        },
    }
}
