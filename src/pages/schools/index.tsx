import prisma from '@/lib/prisma'
import { School, User } from '@prisma/client'
import { GetServerSideProps } from 'next'
import { getSession } from 'next-auth/react'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { Button, SchoolCard, Search } from '@/components'
import { Session } from 'next-auth'
import { gql, NetworkStatus } from '@apollo/client'
import BaseSpinner from '@/components/BaseSpinner'
import { useInfiniteScroll } from '@/hooks/useInfiniteScroll'
import Head from 'next/head'
import Header from '@/components/Header'
import { useDebounce } from '@/hooks/useDebounce'

type Props = {
    user?: Session['user']
    schools: School[]
}

const GET_SCHOOLS = gql`
    query schools($limit: Int, $skip: Int, $name: String) {
        schools(limit: $limit, skip: $skip, name: $name) {
            id
            name
            image
        }
    }
`

const Schools = ({ user, schools }: Props) => {
    const [filter, setFilter] = useState<string>('')
    const router = useRouter()

    const [query, setQuery] = useState<string>('')
    const searchQuery = useDebounce<string>(query, 500)
    const [schoolsList, setSchoolsList] = useState<School[]>([])
    const { name } = router.query

    useEffect(() => {
        searchQuery
        router.push({
            pathname: router.pathname,
            ...(searchQuery && { query: { name: searchQuery } }),
        })
    }, [searchQuery])

    const { ref, networkStatus, error } = useInfiniteScroll<School>({
        query: GET_SCHOOLS,
        queryVariables: {
            ...(name && { name: name as string }),
        },
        items: schoolsList,
        setItems: setSchoolsList,
        skip: 0,
        limit: 10,
    })

    if (networkStatus === NetworkStatus.loading) {
        return (
            <div className="h-20 w-20 mx-auto">
                <BaseSpinner />
            </div>
        )
    }
    if (error) {
        console.log(error)
    }

    return (
        <>
            <Header description={'Schools page'} />
            <div className="flex flex-col items-center my-8 gap-2 min-h-fit md:mx-4 md:mt-8">
                <h1 className="font-bold text-font2 dark:text-bg2 text-xl justify-center uppercase">
                    schools
                </h1>
                <div className="w-full flex gap-4 items-center mt-4 justify-center md:px-2">
                    <div className="w-2/3">
                        <Search
                            filter={query}
                            onChange={newFilter => {
                                setQuery(newFilter)
                                router.push({
                                    pathname: router.pathname,
                                    query: { ...router.query, name: newFilter },
                                })
                            }}
                            placeholder="Filter..."
                        />
                    </div>
                    <div>
                        <Button
                            label="Add School"
                            onClick={() => router.push('/schools/new')}
                        />
                    </div>
                </div>
            </div>
            <div
                className={
                    'scroller grid grid-cols-5 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-8 ' +
                    'content-start overflow-y-auto overflow-x-hidden h-full p-8 md:mx-4 sm:px-3'
                }
            >
                {filter
                    ? schools
                          .filter(school =>
                              school.name.toLowerCase().includes(filter),
                          )
                          .map((school, index) =>
                              user ? (
                                  <SchoolCard
                                      key={index}
                                      school={school}
                                      userId={user.id}
                                      isFavorite={user.favoriteSchools?.some(
                                          (favoriteSchool: School) =>
                                              favoriteSchool.id === school.id,
                                      )}
                                  />
                              ) : (
                                  <SchoolCard key={index} school={school} />
                              ),
                          )
                    : schoolsList.map((school, index) =>
                          user ? (
                              <SchoolCard
                                  key={index}
                                  school={school}
                                  userId={user.id}
                                  isFavorite={user.favoriteSchools.some(
                                      (favoriteSchool: School) =>
                                          favoriteSchool.id === school.id,
                                  )}
                              />
                          ) : (
                              <SchoolCard key={index} school={school} />
                          ),
                      )}
                <div ref={ref} />
            </div>
        </>
    )
}

export default Schools

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null
    /**
     * TODO: handle this condition in the query on top
     */
    const filter = (context.query.name as string) || '' // Define the filter here

    const schools = await prisma.school.findMany({
        where: {
            verified: true,
            deleted: false,
            name: {
                contains: filter,
                mode: 'insensitive',
            },
        },
    })

    let user
    if (session) {
        const id = session.user?.id
        user = await prisma.user.findUnique({
            where: { id },
            select: {
                id: true,
                favoriteSchools: {
                    select: {
                        id: true,
                    },
                },
            },
        })
    }

    return {
        props: {
            schools: JSON.parse(JSON.stringify(schools)),
            ...(session && { user: JSON.parse(JSON.stringify(user)) }),
        },
    }
}
