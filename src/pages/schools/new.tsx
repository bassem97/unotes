import { getSession } from 'next-auth/react'
import { GetServerSideProps } from 'next'
import { useEffect, useState } from 'react'
import SchoolForm from '@/components/School/SchoolForm'
import { useRouter } from 'next/router'
import { Session } from 'next-auth'
import Head from 'next/head'
import Header from '@/components/Header'

const New = () => {
    const [result, setResult] = useState('')
    const router = useRouter()
    const { callBackUrl } = router.query

    const [redirectTo, setRedirectTo] = useState('')

    useEffect(() => {
        redirectTo &&
            (callBackUrl
                ? router.push(callBackUrl as string)
                : router.push(redirectTo))
    }, [redirectTo])

    return (
        <>
            <Header description={'New School'} />
            <div className="flex justify-center h-full w-full items-center bg-bg dark:bg-darkbg scroller overflow-y-auto  overflow-x-hidden">
                <div className="bg-bg h-full dark:bg-darkbg rounded-3xl px-8 pt-16 pb-20 flex flex-col gap-4 w-1/2 md:w-fit ">
                    <h1 className="text-2xl dark:text-bg font-bold mb-2 text-center">
                        Create School
                    </h1>
                    <div className="flex justify-center">
                        <h1 className="text-primary text-lg text-center">
                            {result}
                        </h1>
                    </div>

                    <SchoolForm
                        setResult={setResult}
                        setRedirectTo={setRedirectTo}
                    />
                </div>
            </div>
        </>
    )
}

export default New

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null
    if (!session)
        return {
            redirect: {
                destination: `/login?callbackUrl=${context.resolvedUrl}`,
                permanent: false,
            },
        }

    return {
        props: {},
    }
}
