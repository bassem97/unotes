import { GetServerSideProps } from 'next'
import { getSession } from 'next-auth/react'
import { useState } from 'react'
import { gql } from '@apollo/client'
import { Course, Document, School } from '@prisma/client'
import { CourseCard, SchoolCard } from '@/components'
import { Session } from 'next-auth'
import Head from 'next/head'
import { useInfiniteScroll } from '@/hooks/useInfiniteScroll'
import {
    Tabs,
    TabsHeader,
    TabsBody,
    Tab,
    TabPanel,
} from '@material-tailwind/react'
import DocumentCard from '@/components/Document/DocumentCard'
import Header from '@/components/Header'

type Props = {
    query: string
}

const SEARCH_DOCUMENT_QUERY = gql`
    query searchDocument($query: String!, $skip: Int, $limit: Int) {
        searchDocument(query: $query, skip: $skip, limit: $limit) {
            id
            name
            __typename
        }
    }
`

const SEARCH_COURSE_QUERY = gql`
    query searchCourse($query: String!, $skip: Int, $limit: Int) {
        searchCourse(query: $query, skip: $skip, limit: $limit) {
            id
            name
            code
            __typename
        }
    }
`

const SEARCH_SCHOOLS_QUERY = gql`
    query searchSchool($query: String!, $skip: Int, $limit: Int) {
        searchSchool(query: $query, skip: $skip, limit: $limit) {
            id
            name
            __typename
        }
    }
`

const Query = ({ query }: Props) => {
    const [state, setState] = useState({
        schools: [],
        courses: [],
        documents: [],
        tabsInitialValue: 'schools',
    })

    const { schools, courses, documents, tabsInitialValue } = state

    const setItems = (key: string, items: School[] | Course[] | Document[]) => {
        setState(prevState => ({
            ...prevState,
            [key]: items,
        }))
    }

    const { ref: refschools } = useInfiniteScroll<School>({
        query: SEARCH_SCHOOLS_QUERY,
        items: schools,
        setItems: items => setItems('schools', items),
        skip: 0,
        limit: 10,
        queryVariables: { query },
    })

    const { ref: refcourses } = useInfiniteScroll<Course>({
        query: SEARCH_COURSE_QUERY,
        items: courses,
        setItems: items => setItems('courses', items),
        skip: 0,
        limit: 10,
        queryVariables: { query },
    })

    const { ref: refdocuments } = useInfiniteScroll<Document | any>({
        query: SEARCH_DOCUMENT_QUERY,
        items: documents,
        setItems: items => setItems('documents', items),
        skip: 0,
        limit: 10,
        queryVariables: { query },
    })

    const tabs = [
        { value: 'schools', label: 'Schools' },
        { value: 'courses', label: 'Courses' },
        { value: 'documents', label: 'Documents' },
    ]

    const renderTabPanel = (value: string) => {
        const items = state[value]

        return (
            <TabPanel key={value} value={value}>
                <div className="grid grid-cols-3 gap-8 content-start overflow-y-auto overflow-x-hidden h-full p-8 lg:grid-cols-2 lg:px-2 md:grid-cols-1 md:.mx-4">
                    {items.length === 0 ? (
                        <div className="flex flex-col items-center justify-center h-full w-full">
                            <p className="text-xl text-center dark:text-bg">
                                No {value} found
                            </p>
                        </div>
                    ) : (
                        <>
                            {items.map((item: any) =>
                                item.__typename === 'School' ? (
                                    <SchoolCard key={item.id} school={item} />
                                ) : item.__typename === 'Course' ? (
                                    <CourseCard key={item.id} course={item} />
                                ) : (
                                    <DocumentCard
                                        key={item.id}
                                        document={item}
                                    />
                                ),
                            )}
                            <div
                                ref={
                                    value === 'schools'
                                        ? refschools
                                        : value === 'courses'
                                        ? refcourses
                                        : refdocuments
                                }
                            />
                        </>
                    )}
                </div>
            </TabPanel>
        )
    }

    return (
        <>
            <Header description={`Search ${query}`} />
            <Tabs id="custom-animation" value={tabsInitialValue}>
                <TabsHeader>
                    {tabs.map(tab => (
                        <Tab
                            key={tab.value}
                            value={tab.value}
                            className={
                                'border-b-2 text-center py-4 font-bold cursor-pointer text-xl justify-center uppercase ' +
                                'dark:text-bg'
                            }
                            activeClassName={
                                'border-b-2 text-center py-4 font-bold cursor-pointer text-xl justify-center uppercase mb-[-2px] border-primary text-primary dark:text-primarysec ' +
                                '  '
                            }
                        >
                            {tab.label}
                        </Tab>
                    ))}
                </TabsHeader>
                <TabsBody
                    animate={{
                        initial: { y: 250 },
                        mount: { y: 0 },
                        unmount: { y: 250 },
                    }}
                    className="scroller h-full overflow-y-auto overflow-x-hidden"
                >
                    {tabs.map(tab => renderTabPanel(tab.value))}
                </TabsBody>
            </Tabs>
        </>
    )
}

export default Query

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null

    if (!context.params?.query)
        return { redirect: { destination: '/', permanent: false } }

    const query = context.params.query.toString()

    return {
        props: {
            session,
            query,
        },
    }
}
