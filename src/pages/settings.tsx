import { useEffect, useRef, useState, ChangeEvent } from 'react'
import { getSession, useSession } from 'next-auth/react'
import { Session } from 'next-auth'
import Image from 'next/image'
import { Button, SwitchInput } from '@/components'
import { gql, useMutation, useQuery } from '@apollo/client'
import { FaSpinner, FaUpload } from 'react-icons/fa'
import prisma from '@/lib/prisma'
import { NotificationSettings } from '@prisma/client'
import BaseSpinner from '@/components/BaseSpinner'
import Header from '@/components/Header'

type Props = {
    notificationSettings: NotificationSettings[]
    userNotificationSettings: NotificationSettings[]
}

const Settings = ({
    notificationSettings,
    userNotificationSettings: userNotificationSettingsProps,
}: Props) => {
    const { data: userData } = useSession() as { data: Session }
    const [userNotificationSettings, setUserNotificationSettings] = useState(
        userNotificationSettingsProps,
    )

    const ref = useRef<HTMLInputElement>(null)
    const [image, setImage] = useState<string | null>(null)
    const [files, setFiles] = useState<FileList | null>(null)

    const [openNotificationSet, setOpenNotificationSet] = useState(false)
    const [openOtherSet, setOpenOtherSet] = useState(false)
    const [accountSet, setAccountSet] = useState(true)

    const [mutate, { loading, error }] = useMutation(gql`
        mutation updateUser($id: String!, $image: String) {
            updateUser(id: $id, input: { image: $image }) {
                id
            }
        }
    `)

    const [addNotificationSetting, { loading: addNotificationSettingLoading }] =
        useMutation(gql`
            mutation addNotificationSettingMutation(
                $userId: String!
                $notificationSettingId: String!
            ) {
                addNotificationSetting(
                    userId: $userId
                    notificationSettingId: $notificationSettingId
                ) {
                    id
                }
            }
        `)
    const [
        removeNotificationSetting,
        { loading: removeNotificationSettingLoading },
    ] = useMutation(gql`
        mutation removeNotificationSettingMutation(
            $userId: String!
            $notificationSettingId: String!
        ) {
            removeNotificationSetting(
                userId: $userId
                notificationSettingId: $notificationSettingId
            ) {
                id
            }
        }
    `)

    const [deleteUser, { loading: loding_delete_user }] = useMutation(gql`
        mutation deleteRequestedByUser($id: String!) {
            deleteRequestedByUser(id: $id) {
                id
            }
        }
    `)

    const { data, refetch } = useQuery(
        gql`
            query user($id: String!) {
                user(id: $id) {
                    id
                    image
                }
            }
        `,
        {
            variables: {
                id: userData?.user?.id,
            },
            onCompleted: data => {
                console.log(data)
            },
        },
    )

    const handleUpload = async () => {
        if (files) {
            const formData = new FormData()
            for (let i = 0; i < files.length; i++) {
                const element = files[i]
                formData.append('file', element)
            }
            const response = await fetch(
                process.env.NEXT_PUBLIC_BASE_URL + '/api/upload/image',
                {
                    method: 'POST',
                    body: formData,
                },
            )
            if (response.ok) {
                const { key } = await response.json()
                console.log(key)
                key &&
                    mutate({
                        variables: {
                            id: userData?.user?.id,
                            image:
                                process.env.NEXT_PUBLIC_IMAGE_ENDPOINT +
                                '/' +
                                key,
                        },
                        onCompleted: data => {
                            refetch()
                            console.log(data)
                        },
                    })
            }
        }
    }

    useEffect(() => {
        if (files) {
            const reader = new FileReader()
            reader.onload = e => {
                setImage(e.target?.result as string)
            }
            reader.readAsDataURL(files[0])
        }
    }, [files])

    const handleNotificationsSettingsChange = async (
        checked: boolean,
        notificationSetting: NotificationSettings,
    ) => {
        if (checked)
            await addNotificationSetting({
                variables: {
                    userId: userData?.user?.id,
                    notificationSettingId: notificationSetting.id,
                },
                onCompleted: () => {
                    setUserNotificationSettings([
                        ...userNotificationSettings,
                        notificationSetting,
                    ])
                },
            })
        else
            await removeNotificationSetting({
                variables: {
                    userId: userData?.user?.id,
                    notificationSettingId: notificationSetting.id,
                },
                onCompleted: () => {
                    setUserNotificationSettings(
                        userNotificationSettings.filter(
                            userNotificationSetting =>
                                userNotificationSetting.id !==
                                notificationSetting.id,
                        ),
                    )
                },
            })
    }

    return (
        <div className="dark:bg-darkbg">
            <Header description="Setting" />
            {/* <div className="relative w-full h-[30vh] bg-sky-400">
                <h1 className="text-white text-4xl lg:text-3xl xs:text-2xl font-medium w-[1100px] 2xl:w-[60vw] lg:w-full lg:text-center md:w-[80vw] md:text-left xs:text-center m-auto pt-10 xs:pt-8 dark:text-black">
                    Account settings
                </h1>
            </div> */}
            <div className="accountSetting ">
                <div className="absolute top-0 left-0 w-full h-full flex flex-col items-center justify-center">
                    <div className="w-[1300px] h-[70vh] bg-white dark:bg-darkbg asShadow flex 2xl:w-[70vw] lg:w-full lg:h-full md:w-[80vw] md:h-[70vh] xs:flex-col xs:items-center xs:w-full xs:h-full">
                        {/* side bar */}
                        <div className="bg-neutral-100 dark:bg-neutral-800 w-[30%] h-full flex flex-col items-center xl:w-[35%] xs:w-full xs:h-[22%] xs:min-h-[150px] xs:justify-between ">
                            {/* profile */}
                            <div className="flex flex-col items-center text-center justify-center gap-4 py-8 md:py-6 xs:flex-row">
                                <div
                                    className="relative border-4 hover:border-primary rounded-full cursor-pointer w-32 h-32 lg:w-28 lg:h-28 md:w-24 md:h-24 xs:w-16 xs:h-16"
                                    onClick={() => ref?.current?.click()}
                                >
                                    <div>
                                        <input
                                            type="file"
                                            className="hidden"
                                            ref={ref}
                                            onChange={event =>
                                                setFiles(event.target.files)
                                            }
                                        />
                                        <div
                                            className={`absolute grid place-items-center w-full h-full ${
                                                !loading ? 'opacity-0' : ''
                                            } hover:opacity-100`}
                                        >
                                            {loading ? (
                                                <FaSpinner className="text-5xl text-primary dark:text-primarysec animate-spin" />
                                            ) : (
                                                <FaUpload className="text-5xl text-primary dark:text-primarysec" />
                                            )}
                                        </div>
                                        {/* profile pic */}
                                        <Image
                                            height={100}
                                            width={100}
                                            src={
                                                image ||
                                                data?.user?.image ||
                                                '/avatar.png'
                                            }
                                            alt="avatar"
                                            className="rounded-full aspect-square w-full h-full"
                                        />
                                    </div>
                                </div>

                                <h2 className="text-2xl opacity-70 dark:text-bg dark:opacity-90 lg:text-xl">
                                    {userData?.user?.username
                                        ? userData?.user?.username[0].toUpperCase() +
                                          userData?.user?.username.slice(1)
                                        : userData?.user?.name
                                        ? userData?.user?.name[0].toUpperCase() +
                                          userData?.user?.name.slice(1)
                                        : 'User'}
                                </h2>
                            </div>
                            {/* sub nav */}
                            <div className="w-full text-center xs:flex xs:items-center xs:justify-center">
                                <div className="w-full h-px xs:hidden border border-zinc-200 dark:border-black my-3 mt-0" />
                                <p
                                    className={`text-xl font-normal cursor-pointer hover:text-primary lg:text-lg xs:border-b-2 xs:px-[6vw] xs:py-2  ${
                                        accountSet
                                            ? 'text-primary border-sky-300'
                                            : 'text-zinc-400'
                                    }`}
                                    onClick={() => {
                                        setOpenNotificationSet(false)
                                        setOpenOtherSet(false)
                                        setAccountSet(true)
                                    }}
                                >
                                    Account
                                </p>
                                <div
                                    className={`w-full h-px xs:w-px xs:h-full border my-3 xs:hidden ${
                                        accountSet
                                            ? 'border-sky-300'
                                            : 'border-zinc-300 dark:border-black'
                                    }`}
                                />
                                <p
                                    className={`text-xl font-normal cursor-pointer hover:text-primary lg:text-lg xs:border-b-2  xs:px-[6vw] xs:py-2 ${
                                        openNotificationSet
                                            ? 'text-primary border-sky-300'
                                            : 'text-zinc-400'
                                    }`}
                                    onClick={() => {
                                        setOpenNotificationSet(true)
                                        setOpenOtherSet(false)
                                        setAccountSet(false)
                                    }}
                                >
                                    Notification
                                </p>
                                <div
                                    className={`w-full h-px xs:w-px xs:h-full border my-3 xs:hidden ${
                                        openNotificationSet
                                            ? 'border-sky-300'
                                            : 'border-zinc-300 dark:border-black'
                                    }`}
                                />
                                <p
                                    className={`text-xl font-normal cursor-pointer hover:text-primary lg:text-lg xs:border-b-2 xs:px-[6vw] xs:py-2 xs ${
                                        openOtherSet
                                            ? 'text-primary border-sky-300'
                                            : 'text-zinc-400'
                                    }`}
                                    onClick={() => {
                                        setOpenNotificationSet(false)
                                        setOpenOtherSet(true)
                                        setAccountSet(false)
                                    }}
                                >
                                    Others
                                </p>
                                <div
                                    className={`w-full h-px xs:w-px xs:h-full border my-3 xs:hidden ${
                                        openOtherSet
                                            ? 'border-sky-300'
                                            : 'border-zinc-300 dark:border-black'
                                    }`}
                                />
                            </div>
                        </div>
                        {/* main */}
                        <div className="w-[70%] h-full xs:w-full xs:h-[70%] xs:px-6 p-[90px] xl:w-[65%] xl:px-[3vw] ">
                            <div className="w-full h-full ">
                                {accountSet && (
                                    <div className="flex flex-col justify-center items-center gap-4">
                                        <div className="flex items-center gap-4 xs:gap-2">
                                            <p className="text-xl dark:text-bg lg:text-lg md:text-[1rem] xs:text-sm">
                                                Save my profile picture
                                            </p>
                                            <Button
                                                classname="py-2 px-4 md:text-md xs:text-sm xs:py-1 xs:px-2"
                                                label="Save picture"
                                                onClick={handleUpload}
                                                disabled={loading}
                                            />
                                        </div>
                                        <div className="flex items-center gap-4 xs:gap-2">
                                            <p className="text-xl dark:text-bg lg:text-lg md:text-[1rem] xs:text-sm">
                                                Remove my profile picture
                                            </p>
                                            <Button
                                                label="Remove"
                                                classname="py-2 px-4 w-10 border-red-500 bg-red-500 dark:bg-red-500 dark:border-red-500 xs:text-sm xs:py-1 xs:px-2"
                                                onClick={() => {
                                                    mutate({
                                                        variables: {
                                                            id: userData?.user
                                                                ?.id,
                                                            image: null,
                                                        },
                                                        onCompleted: data => {
                                                            refetch()
                                                            console.log(data)
                                                        },
                                                    })
                                                    setImage(null)
                                                }}
                                                disabled={loading}
                                            />
                                        </div>
                                    </div>
                                )}
                                {openNotificationSet && (
                                    <div>
                                        <div className="w-fit flex flex-wrap justify-center mx-auto gap-4">
                                            {notificationSettings.map(
                                                notificationSetting => (
                                                    <div
                                                        key={
                                                            notificationSetting.id
                                                        }
                                                    >
                                                        <label className="flex items-center">
                                                            <SwitchInput
                                                                disabled={
                                                                    addNotificationSettingLoading ||
                                                                    removeNotificationSettingLoading
                                                                }
                                                                checked={userNotificationSettings.some(
                                                                    userNotificationSetting =>
                                                                        userNotificationSetting.id ===
                                                                        notificationSetting.id,
                                                                )}
                                                                label={
                                                                    notificationSetting.label
                                                                }
                                                                onChange={async checked => {
                                                                    if (
                                                                        addNotificationSettingLoading ||
                                                                        removeNotificationSettingLoading
                                                                    )
                                                                        return
                                                                    await handleNotificationsSettingsChange(
                                                                        checked,
                                                                        notificationSetting,
                                                                    )
                                                                }}
                                                            />
                                                        </label>
                                                    </div>
                                                ),
                                            )}
                                        </div>
                                    </div>
                                )}
                                {openOtherSet && (
                                    <div className="flex items-center justify-center gap-4 ">
                                        <h1 className="text-center text-red-700 text-xl lg:text-lg">
                                            Delete my account
                                        </h1>
                                        <Button
                                            label="Delete"
                                            classname="py-2 px-4 w-10 border-red-500 bg-red-500 dark:bg-red-500 dark:border-red-500"
                                            onClick={() => {
                                                deleteUser({
                                                    variables: {
                                                        id: userData?.user?.id,
                                                    },
                                                    onCompleted: data => {
                                                        console.log(data)
                                                    },
                                                })
                                            }}
                                            disabled={loding_delete_user}
                                        />
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Settings

export const getServerSideProps = async (context: any) => {
    const session = (await getSession(context)) as Session | null
    if (!session) {
        return {
            redirect: {
                destination: '/login',
                permanent: false,
            },
        }
    }

    const id = session.user?.id

    const user = await prisma.user.findUnique({
        where: { id },
        include: {
            notificationSettings: true,
        },
    })

    const notificationSettings = await prisma.notificationSettings.findMany()

    return {
        props: {
            userNotificationSettings: JSON.parse(
                JSON.stringify(user!.notificationSettings),
            ),
            notificationSettings: JSON.parse(
                JSON.stringify(notificationSettings),
            ),
        },
    }
}
