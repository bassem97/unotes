import { useRef, useState } from 'react'
import { BsPersonFill, BsTwitter } from 'react-icons/bs'
import { AiFillEye, AiFillEyeInvisible, AiFillLock } from 'react-icons/ai'
import { FaDiscord, FaFacebookF, FaGooglePlusG } from 'react-icons/fa'
import { FcGoogle } from 'react-icons/fc'
import { MdAlternateEmail, MdOtherHouses } from 'react-icons/md'
import { RxGithubLogo } from 'react-icons/rx'
import { useRouter } from 'next/router'
import { GetServerSideProps } from 'next'
import { getSession, signIn } from 'next-auth/react'
import Image from 'next/image'
import { gql, useMutation } from '@apollo/client'
import { useFormik } from 'formik'
import dynamic from 'next/dynamic'
import { object, ref, string } from 'yup'
import { Button, DropdownContainer } from '@/components'
import BaseSpinner from '@/components/BaseSpinner'
import { Session } from 'next-auth'
import Header from '@/components/Header'

const PasswordChecker = dynamic(
    () => import('@/components/PasswordStrength/PasswordChecker'),
    { ssr: false },
)
const CreateUserMutation = gql`
    mutation CreateUser(
        $firstName: String!
        $lastName: String!
        $username: String!
        $email: String!
        $password: String!
    ) {
        createUser(
            input: {
                firstName: $firstName
                lastName: $lastName
                username: $username
                email: $email
                password: $password
            }
        ) {
            id
            username
            password
        }
    }
`

const validationSchema = object().shape({
    firstName: string().required('Required'),
    lastName: string().required('Required'),
    username: string().required('Required'),
    email: string().email().required('Required'),
    password: string()
        .min(5)
        .matches(/[A-Z]/)
        .matches(/[0-9]/)
        .matches(/[!@#$%^&*+-]/)
        .required(),
    confirmPassword: string()
        .oneOf([ref('password')])
        .required(),
})

const Signup = () => {
    const [showPasswordChecker, setShowPasswordChecker] = useState(false)
    const [createUser, { loading, error }] = useMutation(CreateUserMutation, {
        onCompleted: async () => {
            await router.push(
                '/verification/' + email.current?.value.toLowerCase(),
            )
        },
    })

    const [shown, setShown] = useState<boolean>(false)
    const router = useRouter()

    const email = useRef<HTMLInputElement>(null)
    const password = useRef<HTMLInputElement>(null)

    const formik = useFormik({
        initialValues: {
            firstName: '',
            lastName: '',
            username: '',
            email: '',
            password: '',
            confirmPassword: '',
        },
        validationSchema,
        onSubmit: async ({
            firstName,
            lastName,
            username,
            email,
            password,
        }) => {
            await createUser({
                variables: {
                    firstName: firstName.trim().toLowerCase(),
                    lastName: lastName.trim().toLowerCase(),
                    username: username.trim().toLowerCase(),
                    email: email.trim().toLowerCase(),
                    password,
                },
            })
        },
    })
    const { handleSubmit, errors, touched, handleChange, handleBlur, values } =
        formik

    return (
        <>
            <Header description={'Sign up'} />
            <div className="md:flex-col min-h-fit flex justify-end items-center h-full w-full relative overflow-hidden ">
                {/* ________ SignUp-Page SignUp Sectinon ________ */}
                <div
                    className={`flex-col flex overflow-y-auto py-[5vh] gap-4 px-32 lg:px-10 w-1/2 justify-start relative bg-bg2 bg-opacity-20 h-full min-h-fit md:w-full xs:py-[10vh] sm:px-6 xs:gap-2 sm:px-4 md:px-2 lg:px-2 xl:px-4 2xl:px-6`}
                >
                    <div className="flex justify-center xs:mt-2 ">
                        <h1 className="text-3xl xs:text-2xl font-bold -mt-9">
                            Sign up
                        </h1>
                    </div>
                    {/* error div */}
                    <div className="flex justify-center">
                        <h1 className="text-red-500 text-lg">
                            {error?.message}
                        </h1>
                    </div>
                    <form onSubmit={handleSubmit}>
                        <div className="flex flex-col gap-1 bg-transparent my-3 -mt-6">
                            <label
                                htmlFor="firstName"
                                className="text-font2 text-lg xs:text-sm"
                            >
                                First Name
                            </label>
                            <div
                                className={`border-2 bg-bg2 bg-opacity-50 rounded-3xl flex px-4 py-2 border-bg2 items-center xs:py-1 
                                   ${
                                       ((errors.firstName &&
                                           touched.firstName) ||
                                           error) &&
                                       'border-red-500'
                                   }`}
                            >
                                <BsPersonFill className="text-font2 text-lg xs:text-base" />
                                <input
                                    className="pl-4 w-full outline-none placeholder:text-font2 bg-transparent text-lg xs:text-base"
                                    type="text"
                                    name="firstName"
                                    id="firstName"
                                    placeholder="First Name"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </div>
                            {errors.firstName && touched.firstName && (
                                <div className="text-red-500 text-xs italic">
                                    {errors.firstName}
                                </div>
                            )}
                        </div>
                        <div className="flex flex-col gap-1 bg-transparent my-3">
                            <label
                                htmlFor="lastName"
                                className="text-font2 text-lg xs:text-sm"
                            >
                                Last Name
                            </label>
                            <div
                                className={`border-2 bg-bg2 bg-opacity-50 rounded-3xl flex px-4 py-2 border-bg2 items-center xs:py-1
                                   ${
                                       ((errors.lastName && touched.lastName) ||
                                           error) &&
                                       'border-red-500'
                                   }`}
                            >
                                <BsPersonFill className="text-font2 text-lg xs:text-base" />
                                <input
                                    className="pl-4 w-full outline-none placeholder:text-font2 bg-transparent text-lg xs:text-base"
                                    type="text"
                                    name="lastName"
                                    id="lastName"
                                    placeholder="Last Name"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </div>
                            {errors.lastName && touched.lastName && (
                                <div className="text-red-500 text-xs italic">
                                    {errors.lastName}
                                </div>
                            )}
                        </div>
                        <div className="flex flex-col gap-1 bg-transparent my-3">
                            <label
                                htmlFor="username"
                                className="text-font2 text-lg xs:text-sm"
                            >
                                Username
                            </label>
                            <div
                                className={`border-2 bg-bg2 bg-opacity-50 rounded-3xl flex px-4 py-2 border-bg2 items-center xs:py-1
                                   ${
                                       ((errors.username && touched.username) ||
                                           error) &&
                                       'border-red-500'
                                   }`}
                            >
                                <BsPersonFill className="text-font2 text-lg xs:text-base" />
                                <input
                                    className="pl-4 w-full outline-none placeholder:text-font2 bg-transparent text-lg xs:text-base"
                                    type="text"
                                    name="username"
                                    id="username"
                                    placeholder="Username"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </div>
                            {errors.username && touched.username && (
                                <div className="text-red-500 text-xs italic">
                                    {errors.username}
                                </div>
                            )}
                        </div>
                        <div className="flex flex-col gap-1 bg-transparent my-3">
                            <label
                                htmlFor="email"
                                className="text-font2 text-lg xs:text-sm"
                            >
                                Email
                            </label>
                            <div
                                className={`border-2 bg-bg2 bg-opacity-50 rounded-3xl flex px-4 py-2 border-bg2 items-center xs:py-1
                    ${
                        ((errors.email && touched.email) || error) &&
                        'border-red-500'
                    }`}
                            >
                                <MdAlternateEmail className="text-font2 text-lg xs:text-base" />
                                <input
                                    className="pl-4 w-full outline-none placeholder:text-font2 bg-transparent text-lg xs:text-base"
                                    type="email"
                                    name="email"
                                    id="email"
                                    ref={email}
                                    placeholder="Email"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                            </div>
                            {errors.email && touched.email && (
                                <div className="text-red-500 text-xs italic">
                                    {errors.email}
                                </div>
                            )}
                        </div>
                        <div className="flex flex-col gap-1 bg-transparent my-3">
                            <label
                                htmlFor="password"
                                className="text-font2 text-lg xs:text-sm"
                            >
                                Password
                            </label>
                            <div
                                className={`border-2 bg-bg2 bg-opacity-50 rounded-3xl border-bg2 items-center  flex pl-4 py-2 xs:py-1
                        ${
                            ((errors.password && touched.password) || error) &&
                            'border-red-500'
                        } `}
                            >
                                <AiFillLock className="text-font2 text-lg xs:text-sm" />
                                <input
                                    className="pl-4 w-full outline-none placeholder:text-font2 bg-transparent text-lg xs:text-base "
                                    type={shown ? 'text' : 'password'}
                                    name="password"
                                    id="password"
                                    placeholder="Password"
                                    ref={password}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    onClick={() => setShowPasswordChecker(true)}
                                />
                                {shown ? (
                                    <AiFillEye
                                        className=" mr-4 cursor-pointer"
                                        onClick={() => setShown(false)}
                                    />
                                ) : (
                                    <AiFillEyeInvisible
                                        className=" mr-4 cursor-pointer"
                                        onClick={() => setShown(true)}
                                    />
                                )}
                            </div>
                        </div>
                        <div className="flex flex-col gap-1 bg-transparent my-3">
                            <label
                                htmlFor="confirmPassword"
                                className="text-font2 text-lg xs:text-sm"
                            >
                                Confirm Password
                            </label>
                            <div
                                className={`border-2 bg-bg2 bg-opacity-50 rounded-3xl border-bg2 items-center  flex pl-4 py-2 xs:py-1
                        ${
                            ((errors.confirmPassword &&
                                touched.confirmPassword) ||
                                error) &&
                            'border-red-500'
                        } `}
                            >
                                <AiFillLock className="text-font2 text-lg xs:text-sm" />
                                <input
                                    className="pl-4 w-full outline-none placeholder:text-font2 bg-transparent text-lg xs:text-base "
                                    type={shown ? 'text' : 'password'}
                                    name="confirmPassword"
                                    id="confirmPassword"
                                    placeholder="Confirm Password"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                {shown ? (
                                    <AiFillEye
                                        className=" mr-4 cursor-pointer"
                                        onClick={() => setShown(false)}
                                    />
                                ) : (
                                    <AiFillEyeInvisible
                                        className=" mr-4 cursor-pointer"
                                        onClick={() => setShown(true)}
                                    />
                                )}
                            </div>
                        </div>
                        {showPasswordChecker && (
                            <PasswordChecker
                                password={values.password}
                                confirmPassword={values.confirmPassword}
                            />
                        )}
                        {loading ? (
                            <div className="h-20 w-20 mx-auto">
                                <BaseSpinner />
                            </div>
                        ) : (
                            <div className="flex justify-center">
                                <Button
                                    label=""
                                    classname="text-xl px-4 py-1 -mt-2 xs:text-base"
                                    disabled={loading}
                                >
                                    <button
                                        type="submit"
                                        className="w-full h-full"
                                    >
                                        Sign Up
                                    </button>
                                </Button>
                            </div>
                        )}
                    </form>
                    <h2 className="text-font uppercase text-center my-2 -mt-1">
                        Or
                    </h2>
                    <div className="text-xl flex gap-4 justify-center items-center justify-items-center text-bg">
                        <div className="group p-2 rounded-full cursor-pointer bg-[#4267B2] hidden ">
                            {' '}
                            {/* remove hidden class to unhide fb-icon */}
                            <FaFacebookF className="group-hover:animate-[500ms_hop_ease-in-out] transition-all " />
                        </div>
                        <div className="group p-2 rounded-full cursor-pointer bg-bg -mt-3">
                            <FcGoogle
                                className="group-hover:animate-[500ms_hop_ease-in-out] transition-all"
                                onClick={() =>
                                    signIn('google', {
                                        callbackUrl:
                                            router.query.callbackUrl?.toString() ||
                                            '/',
                                    })
                                }
                            />
                        </div>
                        <div className="hidden">
                            {' '}
                            {/* remove hidden class to unhide DropdownContainer */}
                            <DropdownContainer
                                position="bottom-11 right-0 py-2 px-4"
                                head={
                                    <div
                                        title="Others..."
                                        className="group p-2 rounded-full cursor-pointer bg-font2 flex justify-center items-center"
                                    >
                                        <MdOtherHouses className="group-hover:animate-[500ms_hop_ease-in-out] transition-all" />
                                    </div>
                                }
                            >
                                <div className="grid grid-cols-3 gap-4 w-40 py-6 justify-items-center">
                                    <div className="group p-2 rounded-full cursor-pointer bg-[#1DA1F2]">
                                        <BsTwitter className="group-hover:animate-[500ms_hop_ease-in-out] transition-all" />
                                    </div>
                                    <div className="group p-2 rounded-full cursor-pointer bg-[#DB4437]">
                                        <FaGooglePlusG className="group-hover:animate-[500ms_hop_ease-in-out] transition-all" />
                                    </div>
                                    <div className="group p-2 rounded-full cursor-pointer bg-[#5865F2]">
                                        <FaDiscord className="group-hover:animate-[500ms_hop_ease-in-out] transition-all" />
                                    </div>
                                    <div className="group p-2 rounded-full cursor-pointer bg-[#171515]">
                                        <RxGithubLogo className="group-hover:animate-[500ms_hop_ease-in-out] transition-all" />
                                    </div>
                                </div>
                            </DropdownContainer>
                        </div>
                    </div>
                </div>

                {/* ________ SignUp-Page Redirect-to-SignUp-page Sectinon ________ */}
                <div
                    className={
                        'w-1/2 relative flex justify-center flex-col items-center h-full bg-primary' +
                        ' md:w-full md:h-fit md:py-8 '
                    }
                >
                    <h1 className=" text-bg text-center relative font-bold text-3xl h-fit mb-4 xs:text-lg xs:mb-2">
                        You already have an account?
                    </h1>
                    <p className=" text-bg w-2/3 text-center mb-4 xs:text-sm">
                        Connect to your account now !
                    </p>
                    <Button
                        variant="secondary"
                        onClick={() => {
                            router.push('/login')
                        }}
                        label={'Sign in'}
                        classname="border-2 px-2 py-1 text-lg sm:text-xl xs:text-sm"
                    />
                    <div className="relative w-full mt-4 h-80 md:hidden sm:h-60 xs:h-32 xs:mt-0">
                        <Image
                            src={'/login.png'}
                            fill
                            className="object-contain"
                            alt=""
                        />
                    </div>
                </div>
            </div>
        </>
    )
}

export default Signup

export const getServerSideProps: GetServerSideProps = async context => {
    const session = (await getSession(context)) as Session | null
    return session
        ? {
              redirect: {
                  destination: '/',
                  permanent: false,
              },
          }
        : {
              props: {
                  session,
              },
          }
}
