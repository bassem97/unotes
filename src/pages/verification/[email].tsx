import Image from 'next/image'
import Button from '@/components/Button'
import { GetServerSideProps } from 'next'
import prisma from '@/lib/prisma'
import { gql, useMutation } from '@apollo/client'
import { useState } from 'react'
import BaseSpinner from '@/components/BaseSpinner'
import Head from 'next/head'
import Header from '@/components/Header'

interface VerificationPageProps {
    email: string
}

const sendVerificationRequestMutation = gql`
    mutation sendVerificationRequest($email: String!) {
        sendVerificationRequest(email: $email) {
            id
        }
    }
`

const VerificationPage = ({ email }: VerificationPageProps) => {
    const [iterations, setIterations] = useState(0)
    const [sendVerification, { loading }] = useMutation(
        sendVerificationRequestMutation,
    )

    const resendVerification = async () => {
        setIterations(iterations + 1)
        await sendVerification({
            variables: {
                email,
            },
        })
    }

    return (
        <>
            <Header description={'Verification page'} />
            <div className="flex flex-col items-center justify-center h-screen">
                <div className="flex flex-col items-center justify-center w-1/2 h-1/2 bg-white rounded-2xl shadow-lg">
                    <div className="flex justify-center w-full h-20 ">
                        <h1 className="text-2xl font-bold text-font2 dark:text-bg2">
                            Please verify your email
                        </h1>
                    </div>
                    <div className="flex justify-center w-full h-10 ">
                        <div className="text-center text-font2 dark:text-bg2 tracking-wide">
                            You&apos;re almost there! We&apos;ve sent a
                            verification email to <br /> <b>{email}</b>.
                            <br />
                        </div>
                    </div>

                    <br />

                    <div className="flex justify-center w-full h-10 ">
                        <div className="text-center text-font2 dark:text-bg2">
                            Just click on the link in that email to complete
                            your registration. If you don&apos;t see the email,{' '}
                            <b>check your spam folder </b>.
                        </div>
                    </div>

                    <br />

                    <div className="flex justify-center w-full h-10 ">
                        <div className="text-center text-font2 dark:text-bg2">
                            Still can&apos;t find it the email ?
                        </div>
                    </div>
                    {loading ? (
                        <BaseSpinner />
                    ) : (
                        <div className="flex justify-center w-full h-10 ">
                            {iterations >= 3 ? (
                                <p>Too many attempts, try again later</p>
                            ) : (
                                <Button
                                    label="Resend Email"
                                    disabled={loading}
                                    onClick={resendVerification}
                                />
                            )}
                        </div>
                    )}
                    <div className="relative w-full mt-4 h-80">
                        <Image
                            src={'/verification.jpg'}
                            fill
                            className="object-contain"
                            alt=""
                        />
                    </div>
                </div>
            </div>
        </>
    )
}

export default VerificationPage

export const getServerSideProps: GetServerSideProps = async context => {
    const { email } = context.query
    const user = await prisma.user.findUnique({
        where: {
            email: email as string,
        },
    })
    if (!user) {
        return {
            notFound: true,
        }
    } else {
        return {
            props: {
                email: email,
            },
        }
    }
}
