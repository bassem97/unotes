import React from 'react'
import Navbar from '@/layouts/Navbar/Navbar'
import { client } from '@/lib/apollo'
import { ApolloProvider } from '@apollo/client'

describe('Navbar Component', () => {
    it('renders', () => {
        cy.mount(
            <ApolloProvider client={client}>
                <Navbar handleThemeChange={() => {}} isDarkMode={false} />
            </ApolloProvider>,
        )
    })
})
