import { ApolloProvider } from '@apollo/client'
import React from 'react'
import Settings from '@/pages/settings'
import { client } from '@/lib/apollo'

describe('<Settings />', () => {
    it('renders', () => {
        // see: https://on.cypress.io/mounting-react
        cy.mount(
            <ApolloProvider client={client}>
                <Settings
                    notificationSettings={[]}
                    userNotificationSettings={[]}
                />
            </ApolloProvider>,
        )
    })
})
