import React from 'react'
import Signup from '@/pages/signup'
import { MockedProvider } from '@apollo/client/testing'

const mocks: any = []

describe('<Signup />', () => {
    it('renders', () => {
        // see: https://on.cypress.io/mounting-react
        cy.mount(
            <MockedProvider mocks={mocks} addTypename={false}>
                <Signup />
            </MockedProvider>,
        )
    })
})
