import React from 'react'
import SchoolID from '@/pages/schools/[id]'
import { MockedProvider } from '@apollo/client/testing'

const mocks: any = []

const school = {
    school_id: '0b077f62-baff-4563-be7d-bd502e484489',
    id: '0b077f62-baff-4563-be7d-bd502e484489',
    name: 'Test School',
    deleted: false,
    verified: true,
    userId: '0b077f62-baff-4563-be7d-bd502e484489',
    image: 'https://picsum.photos/200',
}

describe('<SchoolID />', () => {
    it('renders', () => {
        // see: https://on.cypress.io/mounting-react
        cy.mount(
            <MockedProvider mocks={mocks} addTypename={false}>
                <SchoolID school={school} />
            </MockedProvider>,
        )
    })
})
