import React from 'react'
import Schools from '@/pages/schools'
import { MockedProvider } from '@apollo/client/testing'
import { gql } from '@apollo/client'

const mocks: any = []

describe('<Schools />', () => {
    it('renders', () => {
        // see: https://on.cypress.io/mounting-react

        cy.mount(
            <MockedProvider mocks={mocks} addTypename={false}>
                <Schools />
            </MockedProvider>,
        )
    })
})
