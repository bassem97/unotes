import React from 'react'
import SwitchInput from '../../../components/SwitchInput'

describe('<SwitchInput />', () => {
    it('renders', () => {
        // see: https://on.cypress.io/mounting-react

        cy.mount(
            <SwitchInput
                checked={false}
                label={'Test'}
                onChange={() => console.log('test')}
            />,
        )
        cy.get('input').should('not.be.checked')
    })
})
