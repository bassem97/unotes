import React from 'react'
import ThemeToggler from '../../../components/ThemeToggler'

describe('<ThemeToggler />', () => {
    it('renders', () => {
        // see: https://on.cypress.io/mounting-react
        cy.mount(
            <ThemeToggler isDarkMode={true} handleThemeChange={() => {}} />,
        )
    })
})
