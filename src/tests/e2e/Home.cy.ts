/// <reference types="cypress" />
describe('test home page', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000')
    })

    it('should have a title "uNotes" ', () => {
        // should have a meta title
        cy.get('head title').should('contain', 'uNotes')
    })

    it('should have a header "Let’s fill in the gaps in your college education"', () => {
        cy.get('h1').should(
            'contain',
            'Let’s fill in the gaps in your college education',
        )
    })
})

export {}
