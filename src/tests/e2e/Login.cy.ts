describe('login', () => {
    before(() => {
        cy.login('unotes.g@gmail.com', '123456')
    })

    it('should be able to login with the default email and password', () => {
        cy.contains('Logout')
        expect(cy.contains('Logout'))
    })

    it('should let me logout', () => {
        cy.contains('Logout').click()
        cy.origin('http://127.0.0.1:3000', () => {
            expect(cy.contains('Login'))
        })
    })
})

export {}
