import { gql } from '@apollo/client'
import { client } from '@/lib/apollo'

const query = gql`
    mutation deleteUserForever($email: String!) {
        deleteUserForever(email: $email) {
            id
        }
    }
`

const email = 'testuser@gmail.com'

describe('template spec', () => {
    before(() => {
        // clear local storage
        cy.clearCookie('next-auth.session-token')
        cy.visit('http://localhost:3000/signup')
    })

    it('should have a title "uNotes |  Register" ', () => {
        // should have a meta title
        cy.get('head title').should('contain', 'uNotes | Register')
    })

    it('should have a header "Sign in"', () => {
        cy.get('h1').should('contain', 'Sign up')
    })

    it('should redirect to verification page after signup', () => {
        cy.get('input[name="firstName"]').type('test')
        cy.get('input[name="lastName"]').type('user')
        cy.get('input[name="username"]').type('testUser')
        cy.get('input[name="email"]').type(`${email}`)
        cy.get('input[name="password"]').type('Azerty123@')
        cy.get('input[name="confirmPassword"]').type('Azerty123@')
        cy.get('button[type="submit"]').click()
        cy.url().should('include', '/verification')
    })

    after(() => {
        cy.wrap(
            client.query({
                query,
                variables: {
                    email,
                },
                // it is important to AVOID any caching here
                // and fetch the current server data
                fetchPolicy: 'no-cache',
            }),
        )
            .its('data.deleteUserForever.id')
            .should('be.a', 'string')
    })
})

export {}
