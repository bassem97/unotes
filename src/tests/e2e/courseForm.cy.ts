import { gql } from '@apollo/client'
import { client } from '@/lib/apollo'

const query = gql`
    mutation deleteCoursesForTest($code: String!) {
        deleteCoursesForTest(code: $code) {
            __typename
        }
    }
`

const courseName = 'course test'
const courseCode = 'course code test'

describe('Create course test', () => {
    before(() => {
        // clear local storage
        cy.clearCookie('next-auth.session-token')
        cy.login('unotes.g@gmail.com', '123456')
    })

    it('should login successfully', () => {
        cy.contains('Logout')
        expect(cy.contains('Logout'))
    })

    it('should load add course page', () => {
        cy.visit('http://localhost:3000/courses/new')
    })

    it('should fill out the form', function () {
        cy.get('input[name="name"]').type(courseName)
        cy.get('input[name="code"]').type(courseCode)
        cy.get('input[type=file]').selectFile('cypress/fixtures/file_test.png')

        // select the first element in the dropdown
        cy.get('button[type="submit"]').click()
    })

    it('should go to course view page', function () {
        cy.contains(courseName).click()
    })

    // after(() => {
    //     cy.wrap(
    //         client.query({
    //             query,
    //             variables: {
    //                 code: courseCode,
    //             },
    //             // it is important to AVOID any caching here
    //             // and fetch the current server data
    //             fetchPolicy: 'no-cache',
    //         })
    //     )
    //         .its('data.deleteCoursesForTest.__typename')
    //         .should('be.a', 'string', 'School')
    // })
})

export {}
