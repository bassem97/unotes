import { client } from '@/lib/apollo'
import { gql } from '@apollo/client'

const query = gql`
    mutation deleteSchoolsForTest($name: String!) {
        deleteSchoolsForTest(name: $name) {
            __typename
        }
    }
`

const schoolName = 'school test'

describe('Create school test', () => {
    before(() => {
        // clear local storage
        cy.clearCookie('next-auth.session-token')
        cy.login('unotes.g@gmail.com', '123456')
    })

    it('should login successfully', () => {
        cy.contains('Logout')
        expect(cy.contains('Logout'))
    })

    it('should go to add school form', function () {
        cy.contains('Explore').click()
        cy.contains('Add School').click()
    })

    it('should fill out the form', function () {
        cy.get('input[name="name"]').type(schoolName)
        cy.get('input[type=file]').selectFile('cypress/fixtures/file_test.png')
        cy.get('button[type="submit"]').click()
    })

    it('should go to school view page', function () {
        cy.contains(schoolName).click()
    })

    after(() => {
        cy.wrap(
            client.query({
                query,
                variables: {
                    name: schoolName,
                },
                // it is important to AVOID any caching here
                // and fetch the current server data
                fetchPolicy: 'no-cache',
            }),
        )
            .its('data.deleteSchoolsForTest.__typename')
            .should('be.a', 'string', 'School')
    })
})

export {}
