/// <reference types="cypress" />

import schools from 'cypress/fixtures/schools.json'

const MOCK_RESPONSES = {
    schools,
}

describe('template spec', () => {
    beforeEach(() => {
        cy.intercept('POST', 'http://localhost:3000/api/graphql', req => {
            req.reply({
                statusCode: 200,
                body: MOCK_RESPONSES[req.body.operationName],
            })
        })
    })

    it('should be mocked', () => {
        cy.visit('http://localhost:3000/schools')
        cy.contains('test school1')
        cy.contains('test school2')
        cy.contains('test school3')
        cy.contains('test school4')
    })

    //     test infinte scroll
    it('should be load more when hover', () => {
        cy.visit('http://localhost:3000/schools')
        cy.get('.school-card').should('have.length', 10)
        cy.get('.school-card').last().trigger('mouseover')
        cy.get('.school-card').should('have.length', 20)

        cy.get('.school-card').first().trigger('mouseover')
        cy.get('.school-card').last().trigger('mouseover')
        cy.get('.school-card').should('have.length', 30)

        cy.get('.school-card').first().trigger('mouseover')
        cy.get('.school-card').last().trigger('mouseover')
        cy.get('.school-card').should('have.length', 40)

        cy.get('.school-card').first().trigger('mouseover')
        cy.get('.school-card').last().trigger('mouseover')
        cy.get('.school-card').should('have.length', 50)
    })
})

export {}
