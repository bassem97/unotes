import React from 'react'
import {
    BsFiletypeTxt,
    BsFiletypePdf,
    BsFiletypeDoc,
    BsFiletypeDocx,
    BsFiletypeXls,
} from 'react-icons/bs'

type Props = {
    type: string
    className: string
}

const FileTypeIcon = ({ type, className }: Props) => {
    let fileTypeIcon = <></>
    const splitType = type.toLowerCase().replace('.gz', '').split('.')
    type = splitType[splitType.length - 1]
    switch (type) {
        case 'pdf':
            fileTypeIcon = (
                <BsFiletypePdf className={'text-primary  ' + className} />
            )
            break
        case 'txt':
            fileTypeIcon = (
                <BsFiletypeTxt className={'text-primary  ' + className} />
            )
            break
        case 'doc':
            fileTypeIcon = (
                <BsFiletypeDocx className={'text-primary  ' + className} />
            )
            break
        case 'docx':
            fileTypeIcon = (
                <BsFiletypeDoc className={'text-primary  ' + className} />
            )
            break
        case '4xls':
            fileTypeIcon = (
                <BsFiletypeXls className={'text-primary  ' + className} />
            )
            break
        default:
            fileTypeIcon = (
                <BsFiletypePdf className={'text-primary  ' + className} />
            )
            break
    }
    return <>{fileTypeIcon}</>
}

export default FileTypeIcon
