import { School, Season, Tag } from '@prisma/client'
import { TemporaryCourseType } from './TemporaryCourseType'
import { TemporaryDocumentType } from './TemporaryDocumentType'

export interface Option {
    value: string
    label: string
}

export type AcceptedType =
    | School
    | TemporaryCourseType
    | TemporaryDocumentType
    | Tag
    | null

export type AcceptedEnum = DocumentType | Season | null

export const toModel = (option: Option, list: AcceptedType[]): AcceptedType =>
    list.find((item: AcceptedType) => item?.id === option.value) || null

export const toModels = (
    option: Option[],
    list: AcceptedType[],
): AcceptedType[] => {
    return list.filter((item: AcceptedType) => {
        return option.some((optionItem: Option) => {
            return item?.id === optionItem.value
        })
    })
}
export const enumToOption = (
    enumObject: AcceptedEnum,
    value: string,
): Option => {
    const index = enumObject
        ? Object.keys(enumObject).findIndex(
              (key: string) => enumObject[key] === value,
          )
        : -1
    return index >= 0
        ? {
              value: index + '',
              label: value,
          }
        : {
              value: '',
              label: '',
          }
}
export const enumsToOptions = (enumObject: AcceptedEnum): Option[] =>
    enumObject
        ? Object.keys(enumObject).map((value: string) =>
              enumToOption(enumObject, value),
          )
        : []
export const enumsToOption = (
    enumObject: AcceptedEnum,
    value: number,
    label = 'name',
): Option => {
    return {
        value: value + '',
        label: enumObject ? enumObject[label] : '',
    }
}
export const toOptions = (
    list: AcceptedType[] = [],
    value = 'id',
    label = 'name',
): Option[] => list.map((item: AcceptedType) => toOption(item, value, label))

export const toOption = (
    object: AcceptedType,
    value = 'id',
    label = 'name',
): Option =>
    object
        ? {
              value: object[value],
              label: object[label],
          }
        : {
              value: '',
              label: '',
          }
