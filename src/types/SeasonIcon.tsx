import { Season } from '@prisma/client'
import React from 'react'
import { BsFillEmojiSunglassesFill } from 'react-icons/bs'
import { FaRegSnowflake } from 'react-icons/fa'
import { GiFallingLeaf } from 'react-icons/gi'

type Props = {
    season: Season
    className: string
}

const SeasonIcon = ({ season, className }: Props) => {
    let seasonIcon = <></>
    switch (season) {
        case Season.FALL:
            seasonIcon = (
                <GiFallingLeaf className={'text-yellow-200 ' + className} />
            )
            break
        case Season.WINTER:
            seasonIcon = (
                <FaRegSnowflake className={'text-primary ' + className} />
            )
            break
        case Season.SUMMER:
            seasonIcon = (
                <BsFillEmojiSunglassesFill
                    className={'text-red-500 ' + className}
                />
            )
            break
        default:
            seasonIcon = (
                <FaRegSnowflake className={'text-primary ' + className} />
            )
            break
    }
    return <>{seasonIcon}</>
}

export default SeasonIcon
