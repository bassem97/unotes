import { User } from '@prisma/client'

export type TemporaryCourseType = {
    id: string
    name: string
    code: string
    schoolId: string
    deleted: boolean
    verified: boolean
    image: string | null
    moderators?: User[]
}
