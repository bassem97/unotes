import { DocumentType, Season, Tag } from '@prisma/client'
import { TemporaryCourseType } from './TemporaryCourseType'
import { TemporaryFileDocumentType } from './TemporaryFileDocumentType'

export type TemporaryDocumentType = {
    id?: string
    name: string
    season: Season
    year: number
    userId: string
    courseId: string
    isSolutionsIncluded: boolean
    isCompleted: boolean
    postedAt?: Date | null
    type: DocumentType
    deleted: boolean | null
    verified: boolean | null
    tags?: Tag[]
    files?: TemporaryFileDocumentType[]
    course?: TemporaryCourseType
}
