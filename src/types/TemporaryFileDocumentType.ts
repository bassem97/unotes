export type TemporaryFileDocumentType = {
    id?: string
    url: string
    documentId?: string
    createdAt?: Date | null
    updatedAt?: Date | null
    deletedAt?: Date | null
}
