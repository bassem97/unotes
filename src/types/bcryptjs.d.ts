declare module 'bcryptjs' {
    export function hashSync(data: string, salt: number): string
    export function compareSync(data: string, encrypted: string): boolean
}
