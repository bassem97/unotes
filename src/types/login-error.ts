// err.ts:

const ERR = Symbol('ERR')
export class LoginError extends Error {
    error_type: ErrTypes
    error_code: ErrCode
    constructor(error_type: ErrTypes, error_code: ErrCode) {
        super(error_type)
        this.error_type = error_type
        this.error_code = error_code
    }
}

/** Optional addition if you want to handle errors differently based on their type */
type ErrTypes =
    | 'ERROR_CODE_USER_NOT_FOUND'
    | 'ERROR_CODE_INACTIVE_USER'
    | 'ERROR_CODE_NO_PASSWORD_SET'
    | 'ERROR_CODE_ACCOUNT_NOT_VERIFIED'
    | 'ERROR_CODE_INCORRECT_PASSWORD'
type ErrCode = 'ERR00' | 'ERR01' | 'ERR02' | 'ERR03' | 'ERR04'

export function isErr(x: unknown): x is LoginError {
    return typeof x === 'object' && x != null && ERR in x
}

// export function LoginErrorThrow( error_type: ErrTypes, error_code: ErrCode): LoginError {
//   return new LoginError(error_type, error_code)
// }

/** Make an error-throwing function into a error-returning function */
export function tryCatch<T>(fn: () => T): T | LoginError {
    try {
        return fn()
    } catch (e: any) {
        return e
    }
}

/** If you need to convert your error values back into throw/catch land */
export function assertOk<T>(x: T | LoginError): T {
    if (isErr(x)) {
        throw x
    }
    return x
}
