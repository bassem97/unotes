declare module 'socket' {
    interface Message {
        author?: string
        action?: string
        message: string
        data?: any
    }
}
