declare module 'next-auth' {
    import { DefaultSession } from 'next-auth'

    /**
     * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
     */
    interface Session {
        user: User & DefaultSession['user']
    }

    /**
     * The shape of the user object returned in the OAuth providers' `profile` callback,
     * or the second parameter of the `session` callback, when using a database.
     */
    interface User {
        id: string
        name?: string
        firstName?: string
        lastName?: string
        username?: string
        email: string
        image: string
    }

    interface Token {
        id: string
    }

    /** Returned by the `jwt` callback and `getToken`, when using JWT sessions */
    interface CallbackJwt {
        token: Token
        user: User
    }

    /** Returned by the `session` callback, when using JWT sessions */
    interface CallbackSession {
        session: Session
        token: Token
    }

    interface NextAuthOptions {
        adapter: any
        providers: any
        session: any
        secret: string
        callbacks: {
            jwt: (jwt: CallbackJwt) => Promise<Token>
            session: (session: CallbackSession) => Promise<Session | null>
        }
        pages: {
            signIn: string
            signUp: string
        }
    }
}
