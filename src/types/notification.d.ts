declare module 'notification' {
    interface Notification {
        id: string
        userId: string
        body: string
        seen: boolean
        link: string | null
        createdAt: string
    }
}
