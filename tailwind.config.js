/** @type {import('tailwindcss').Config} */

module.exports = {
    darkMode: 'class',
    content: ['./src/**/*.{js,jsx,ts,tsx}'],
    theme: {
        screens: {
            '2xl': { max: '1535px' },
            // => @media (max-width: 1535px) { ... }

            xl: { max: '1279px' },
            // => @media (max-width: 1279px) { ... }

            lg: { max: '1023px' },
            // => @media (max-width: 1023px) { ... }

            md: { max: '767px' },
            // => @media (max-width: 767px) { ... }

            sm: { max: '639px' },
            // => @media (max-width: 639px) { ... }
            xs: { max: '475px' },
            // => @media (max-width: 475px) { ... }
            tall: { raw: '(max-height: 800px)' },
        },
        extend: {
            colors: {
                primary: '#38B6FF',
                primarysec: '#2c9bdc',
                sec: '#774fac',
                bg: '#ffffff',
                bg2: '#e1dddd',
                font: '#000000',
                font2: '#464646',
                font3: '#adaaaa',
                darkfont: '#131212',
                darkbg: '#181a1b',
            },
            fontFamily: {
                sans: ['Roboto', 'sans-serif'],
                serif: ['Display', 'serif'],
                poppins: ['Poppins', 'serif'],
            },
        },
    },
    plugins: [],
}
